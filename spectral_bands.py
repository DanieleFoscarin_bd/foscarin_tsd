# %%
from matplotlib.pyplot import show
import numpy as np
import bd_rd as bd
import bd_rd.signal as bdsig
from bd_rd.processing.blocks._common import Generic_Processor

class Spectral_Bands(Generic_Processor):

    accepted_input_types = [bdsig.Freq_Frame, bdsig.Buffer_Frames]
    output_type = bdsig.Feature_Frame

    is_invertible = True

    default_config = {
        "n_fft": bd.MAX_FREQ,
        "sr": bd.DEFAULT_SR,
        "band_type": "bark",
        "n_bands": 48,
        "keep_ends": False,
        "norm_bands": True,
        "low_lim": 0,
        "band_freqs": None,
    }

    def __init__(
        self,
        sr=None,
        n_fft=None,
        band_type=None,
        n_bands=None,
        keep_ends=None,
        norm_bands=None,
        low_lim=None,
        band_freqs=None,
        config=None,
    ):

        self.assign_values(
            config,
            sr=sr,
            n_fft=n_fft,
            band_type=band_type,
            n_bands=n_bands,
            keep_ends=keep_ends,
            norm_bands=norm_bands,
            low_lim=low_lim,
            band_freqs=band_freqs,
        )

        super().__init__(config)
        self.band_matrix = None
        self.band_matrix_inv = None

        self.__prepare_band_matrix()
        self.invert_np = self.invert

    def hz_to_erb(self, f):
        return 11.17268 * np.log(1 + (46.06538 * f) / (f + 14678.49))

    def erb_to_hz(self, f):
        return (676170.4) / (47.06538 - np.exp(0.08950404 * f)) - 14678.49

    def hz_to_bark(self, f):
        return ((26.81 * f) / (1960 + f)) - 0.53

    def bark_to_hz(self, f):
        return (1960 * f + 1038.8) / (26.28 - f)

    def hz_to_mel(self, f):
        return 1127 * np.log(1 + f / 700)

    def mel_to_hz(self, f):
        return np.exp(f / 1127) * 700 - 1

    def __prepare_band_matrix(self):

        if self.band_freqs is not None:
            if self.low_lim == 0:
                self.low_lim = 1
            self.low_lim_bins = int(np.floor(self.n_fft * self.low_lim / (self.sr / 2)))
            assert self.low_lim_bins < self.n_fft, "Error, reduce low_lim"
            self.n_fft_up = self.n_fft - self.low_lim_bins
            self.n_bands = self.band_freqs.shape[0]
            self.band_matrix = np.zeros((self.n_bands, self.n_fft_up))

            self.band_bins = self.band_freqs / (self.sr / 2) * self.n_fft_up
            self.band_bins = self.band_bins.astype(np.int32)
            assert (
                self.band_bins.shape[0] == np.unique(self.band_bins).shape[0]
            ), f"Unable to compute {self.band_freqs.shape[0]} bands with {self.n_fft_up} spectral bins. Try to reduce n_bands, increase n_fft or reduce low_lim."

            for i in range(self.n_bands - 1):
                band_size = self.band_bins[i + 1] - self.band_bins[i]
                frac = np.arange(band_size) / band_size
                idxs = np.arange(self.band_bins[i], self.band_bins[i] + band_size)
                idxs_upto = idxs < self.n_fft_up

                if self.norm_bands:
                    # TODO fix not triangular filters because of normalization
                    self.band_matrix[i, idxs[idxs_upto]] = (
                        1 - frac[idxs_upto]
                    ) / band_size
                    self.band_matrix[i + 1, idxs[idxs_upto]] = (
                        frac[idxs_upto] / band_size
                    )
                    # self.band_matrix[i, idxs[idxs_upto]] = frac[idxs_upto] / band_size
                    # self.band_matrix[i + 1, idxs[idxs_upto]] = (1 - frac[idxs_upto]) / band_size
                else:
                    self.band_matrix[i, idxs[idxs_upto]] = 1 - frac[idxs_upto]
                    self.band_matrix[i + 1, idxs[idxs_upto]] = frac[idxs_upto]

            # if not self.keep_ends:
            #     self.band_matrix = self.band_matrix[1:-1]
            self.band_matrix_inv = self.band_matrix.copy()

            # if self.keep_ends:
            # maybe just don't duplicate here?
            self.band_matrix[0, :] = self.band_matrix[0, :]
            self.band_matrix[-1, :] = self.band_matrix[-1, :]
            self.band_matrix = self.band_matrix.T
            # if not self.keep_ends:
            #     self.n_bands = self.n_bands - 2

        else:

            if self.band_type == "mel":
                band_to_hz = self.mel_to_hz
                hz_to_band = self.hz_to_mel
            elif self.band_type == "bark":
                band_to_hz = self.bark_to_hz
                hz_to_band = self.hz_to_bark
            elif self.band_type == "erb":
                band_to_hz = self.erb_to_hz
                hz_to_band = self.hz_to_erb
            elif self.band_type == "linear":
                band_to_hz = lambda x: x
                hz_to_band = lambda x: x

            if self.low_lim == 0:
                self.low_lim = 1
            self.low_lim_bins = int(np.floor(self.n_fft * self.low_lim / (self.sr / 2)))
            assert self.low_lim_bins < self.n_fft, "Error, reduce low_lim"
            self.n_fft_up = self.n_fft - self.low_lim_bins

            if not self.keep_ends:
                self.n_bands = self.n_bands + 2
            self.band_matrix = np.zeros((self.n_bands, self.n_fft_up))

            self.band_freqs = band_to_hz(
                np.linspace(
                    hz_to_band(self.low_lim),
                    hz_to_band(self.sr / 2),
                    self.n_bands,
                    endpoint=False,
                )
            )
            self.band_bins = self.band_freqs / (self.sr / 2) * self.n_fft_up
            self.band_bins = self.band_bins.astype(np.int32)
            assert (
                self.band_bins.shape[0] == np.unique(self.band_bins).shape[0]
            ), f"Unable to compute {self.n_bands} bands with {self.n_fft_up} spectral bins. Try to reduce n_bands, increase n_fft or reduce low_lim."

            for i in range(self.n_bands - 1):
                band_size = self.band_bins[i + 1] - self.band_bins[i]
                frac = np.arange(band_size) / band_size
                idxs = np.arange(self.band_bins[i], self.band_bins[i] + band_size)
                idxs_upto = idxs < self.n_fft_up

                if self.norm_bands:
                    self.band_matrix[i, idxs[idxs_upto]] = (
                        1 - frac[idxs_upto]
                    ) / band_size
                    self.band_matrix[i + 1, idxs[idxs_upto]] = (
                        frac[idxs_upto] / band_size
                    )
                else:
                    self.band_matrix[i, idxs[idxs_upto]] = 1 - frac[idxs_upto]
                    self.band_matrix[i + 1, idxs[idxs_upto]] = frac[idxs_upto]

            if not self.keep_ends:
                self.band_matrix = self.band_matrix[1:-1]
            self.band_matrix_inv = self.band_matrix.copy()

            if self.keep_ends:
                self.band_matrix[0, :] = self.band_matrix[0, :] * 2
                self.band_matrix[-1, :] = self.band_matrix[-1, :] * 2
            self.band_matrix = self.band_matrix.T
            if not self.keep_ends:
                self.n_bands = self.n_bands - 2

    def compute_frame_np(self, input_data):
        if input_data.ndim == 1:
            input_data = input_data[np.newaxis, :]
        input_data = input_data[:, self.low_lim_bins :]
        out_data = np.dot(input_data, self.band_matrix).flatten()
        return self._Generic_Processor__update(out_data)

    def compute_np(self, input_spec):
        out_spec = np.zeros((input_spec.shape[0], self.n_bands), dtype=np.complex64)
        for frame in range(input_spec.shape[0]):
            out_spec[frame] = self.compute_frame_np(input_spec[frame])
        return out_spec

    def compute_frame(self, input_):
        print("compute_frame", self)
        input_data = input_.get()
        input_type = input_.value_type
        if input_type == bd.T_SPEC_COMPLEX:
            return self.compute_frame_np(np.abs(input_data))
        elif input_type == bd.T_SPEC_MAG:
            return self.compute_frame_np(input_data)
        elif input_type == bd.T_SPEC_MAG2:
            return self.compute_frame_np(np.sqrt(input_data))
        elif input_type == bd.T_SPEC_DB:
            return self.compute_frame_np(np.power(10, input_data / 20))


# %%
if __name__ == "__main__":
    from bd_rd.processing.blocks import STFT
    from matplotlib import pyplot as plt

    import os

    SR = 16000

    config = bd.utils.Class_Dict(
        sr=SR,
        framesize=int(0.01 * SR),
        zero_pad_end=False,
        pre_emphasis=False,
        pre_gain=1,
        window="bd",
        ola_ratio=4,
        max_freq=20000,
        stft_gain=1,
        value_type=bd.T_INT16F,
        dc_removal=True,
    )
    stft_processor = STFT(config=config)

    # root =  'C:\\Users\\fosca\BDSOUND SRL\\[BdResearch] Tesi - Foscarin - Echo Reduction - Foscarin'
    # data_folder = root + '\\data\\generated\\test2'
    # data_folder = '/home/danielefoscarin/LOCAL/SSD/generated/dataset1/train_scenario_10'
    # data_folder = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/rnnoise_data/speech'
    data_folder = "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/GrazUniversity"

    sig, sr = bd.io.read_wav(os.path.join(data_folder, "clnsp1099.wav"), fmt=bd.T_F)

    sig_stft = stft_processor.compute_np(sig)

    plt.figure()
    bd.plots.plot_spectrogram(
        np.log(np.abs(sig_stft + 1e-3)),
        sr=sr,
        framesize=int(sr * 0.01),
        new_figure=False,
        kwargs_imshow={"cmap": "viridis", "interpolation": "nearest"},
        title="Speech signal spectrogram",
        show=False,
    )
    plt.ylim([5000, 0])
    plt.xlim([0, 6])
    plt.savefig("fig_speech_spectrogram.pdf")
    plt.show()

    # n_bands = 48
    # band_processor = Spectral_Bands(sr=sr, n_fft=sig_stft.shape[1], band_type='bark', n_bands=n_bands, keep_ends=False, norm_bands=True, low_lim=150)
    # print(np.round(band_processor.band_freqs, 0))

    # plt.figure()
    # plt.imshow(band_processor.band_matrix, interpolation='nearest', aspect='auto')
    # plt.grid()
    # plt.show()

    # plt.figure()
    # plt.plot(band_processor.band_matrix)
    # # plt.xlim([0,20])
    # plt.grid()
    # plt.show()

    # matrix_sum = np.sum(band_processor.band_matrix, axis=1)
    # plt.figure()
    # plt.plot(matrix_sum)
    # plt.show()

    # sig_erb = band_processor.compute_np(np.square(np.abs(sig_stft)))

    # plt.figure()
    # plt.imshow(np.transpose(np.log(np.abs(sig_erb+1e-3))), interpolation='nearest', aspect='auto', cmap='viridis')
    # plt.title('Bark spectrogram 48 bands')
    # plt.xlabel('frame')
    # plt.ylabel('band')
    # plt.savefig('fig_bark_spec.pdf')
    # plt.show()

    # plt.figure()
    # plt.imshow(np.transpose(np.log(np.abs(sig_stft+1e-3))), interpolation='nearest', aspect='auto', origin='lower', cmap='viridis')
    # plt.title('Bark spectrogram 48 bands')
    # plt.xlabel('frames')
    # plt.ylabel('band')
    # # plt.savefig('fig_bark_spec.pdf')
    # plt.show()

    # print(np.round(band_processor.band_freqs, 0))

    # %%
    # plt.figure()
    # plt.imshow(np.transpose(np.square(np.abs(sig_stft))), interpolation='nearest', aspect='auto', origin='lower', cmap='viridis')
    # plt.title('Bark spectrogram 48 bands')
    # plt.xlabel('frames')
    # plt.ylabel('band')
    # plt.savefig('fig_bark_spec.pdf')
    # plt.show()
    # %%
    custom = np.concatenate(
        (
            np.array([150, 300, 500, 800, 1000]),
            np.linspace(1200, 8000, 43, endpoint=False),
        )
    )
    n_bands = 48
    band_processor = Spectral_Bands(
        sr=sr,
        n_fft=sig_stft.shape[1],
        band_type="bark",
        n_bands=n_bands,
        keep_ends=False,
        norm_bands=True,
        band_freqs=custom,
    )
    print(np.round(band_processor.band_freqs, 0))

    plt.figure()
    plt.imshow(band_processor.band_matrix, interpolation="nearest", aspect="auto")
    plt.grid()
    plt.show()

    plt.figure()
    plt.plot(band_processor.band_matrix)
    # plt.xlim([0,20])
    plt.grid()
    plt.show()

    matrix_sum = np.sum(band_processor.band_matrix, axis=1)
    plt.figure()
    plt.plot(matrix_sum)
    plt.show()

    sig_erb = band_processor.compute_np(np.square(np.abs(sig_stft)))

    plt.figure()
    plt.imshow(np.log(np.abs(sig_erb + 1e-3)), interpolation="nearest", aspect="auto")

    print(np.round(band_processor.band_freqs, 0))

    # %%
    from bd_rd.processing.blocks import STFT
    from matplotlib import pyplot as plt
    import os

    SR = 16000

    config = bd.utils.Class_Dict(
        sr=SR,
        framesize=int(0.01 * SR),
        zero_pad_end=False,
        pre_emphasis=False,
        pre_gain=1,
        window="bd",
        ola_ratio=4,
        max_freq=20000,
        stft_gain=1,
        value_type=bd.T_INT16F,
        dc_removal=True,
    )
    stft_processor = STFT(config=config)

    # root =  'C:\\Users\\fosca\BDSOUND SRL\\[BdResearch] Tesi - Foscarin - Echo Reduction - Foscarin'
    # data_folder = root + '\\data\\generated\\test2'
    data_folder = "/home/danielefoscarin/LOCAL/SSD/generated/dataset1/train_scenario_10"

    sig, sr = bd.io.read_wav(os.path.join(data_folder, "sIn.wav"), fmt=bd.T_F)

    sig_stft = stft_processor.compute_np(sig)

    bd.plots.plot_spectrogram(
        np.log(np.abs(sig_stft + 1e-3)), sr=sr, framesize=int(sr * 0.01)
    )

    bark_freqs = [
        150.0,
        176.0,
        202.0,
        228.0,
        256.0,
        284.0,
        313.0,
        342.0,
        373.0,
        404.0,
        436.0,
        469.0,
        503.0,
        538.0,
        574.0,
        610.0,
        648.0,
        687.0,
        728.0,
        769.0,
        812.0,
        856.0,
        902.0,
        949.0,
        998.0,
        1048.0,
        1100.0,
        1154.0,
        1210.0,
        1268.0,
        1328.0,
        1390.0,
        1455.0,
        1522.0,
        1592.0,
        1665.0,
        1741.0,
        1820.0,
        1903.0,
        1989.0,
        2080.0,
        2174.0,
        2273.0,
        2377.0,
        2486.0,
        2601.0,
        2722.0,
        2849.0,
        2984.0,
        3126.0,
        3277.0,
        3437.0,
        3607.0,
        3788.0,
        3981.0,
        4188.0,
        4410.0,
        4648.0,
        4904.0,
        5182.0,
        5483.0,
        5810.0,
        6167.0,
        6559.0,
        6991.0,
        7469.0,
    ]
    plt.figure()
    plt.plot(np.linspace(150, 8000, 66), bark_freqs)
    plt.show()
    # %%
    t = np.arange(3 * 16000) / 16000
    freq = 440
    x = np.cos(2 * np.pi * t / freq)

    y = (
        np.cos(2 * np.pi * t / 50)
        + 3 * np.cos(2 * np.pi * t / 0.5)
        + 0.5 * np.cos(2 * np.pi * t / 10)
    )

    z = x * y
    Z = np.fft.rfft(z, 48000)

    plt.plot(np.linspace(0, 8000, Z.size), np.log10(np.abs(Z)))
    plt.xlim([-10, 50])

    # %%
    SR = 48000
    config = bd.utils.Class_Dict(
        sr=SR,
        framesize=int(0.01 * SR),
        zero_pad_end=False,
        pre_emphasis=False,
        pre_gain=1,
        window="bd",
        ola_ratio=4,
        max_freq=20000,
        stft_gain=1,
        value_type=bd.T_INT16F,
        dc_removal=True,
    )
    stft_processor = STFT(config=config)
    path = "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/48Kdataset/train/"
    # ignoring sr here
    name_list = os.listdir(path)
    name_list = [
        os.path.join(path, name) for name in name_list if (name.endswith(".wav"))
    ]
    speech_stack = np.zeros((5 * 48000,))
    for name in name_list:
        curr_speech, _ = bd.io.read_wav(os.path.join(path, name), fmt=bd.T_F)
        speech_stack[0 : curr_speech.shape[0]] += curr_speech

    speech_stack /= len(name_list)
    speech_fft = np.fft.fft(speech_stack, n=48000)
    speech_energy = np.abs(speech_fft[: int(speech_fft.shape[0] / 2)])
    # %%

    plt.figure()
    plt.plot(20 * np.log10(speech_energy))
    # plt.plot(speech_energy)
    plt.xscale("log")
    plt.grid()
    plt.show()

    speech_mass = np.cumsum(speech_energy[:max_freq])

    plt.figure()
    plt.plot(speech_mass)
    # plt.xscale('log')
    plt.grid()
    plt.show()

    plt.figure()
    plt.plot(np.diff(speech_mass, n=1))
    # plt.xscale('log')
    plt.grid()
    plt.show()

    # %%

    min_freq = 0
    # max_freq = 24000
    max_freq = 8000
    speech_mass = np.cumsum(speech_energy[:max_freq])
    plt.figure()
    # plt.plot(20*np.log10(speech_freq**2))
    plt.plot(speech_energy[:max_freq])
    # plt.xscale('log')
    plt.grid()
    plt.show()
    plt.figure()
    plt.plot(speech_mass)
    # plt.xscale('log')
    plt.grid()
    plt.show()
    n_band = 49
    band_y = np.linspace(min_freq, speech_mass[-1], n_band)
    # print(band_y)
    band_x = []
    ax = np.linspace(0, speech_mass.shape[0], speech_mass.shape[0])
    for en in band_y:
        # print(en)
        # curr_x = np.where(np.argmax(speech_mass < en))
        temp = ax[speech_mass < en]
        if temp.shape[0] == 0:
            curr_x = 0
        else:
            curr_x = np.argmax(temp)
        # print(curr_x)
        band_x.append(curr_x)

    band_x = np.array(band_x)
    plt.figure()
    plt.plot(band_x)
    # plt.xscale('log')
    plt.grid()
    plt.show()
    print(band_x.shape)
    print(band_x)
    print(np.diff(band_x))

    band_processor = Spectral_Bands(
        sr=sr, n_fft=sig_stft.shape[1], norm_bands=True, band_freqs=band_x
    )
    print(np.round(band_processor.band_freqs, 0))

    plt.figure()
    plt.imshow(band_processor.band_matrix, interpolation="nearest", aspect="auto")
    plt.grid()
    plt.show()

    plt.figure()
    plt.plot(band_processor.band_matrix)
    # plt.xlim([0,20])
    plt.grid()
    plt.show()

    matrix_sum = np.sum(band_processor.band_matrix, axis=1)
    plt.figure()
    plt.plot(matrix_sum)
    plt.show()

    sig_erb = band_processor.compute_np(np.square(np.abs(sig_stft)))

    plt.figure()
    plt.imshow(np.log(np.abs(sig_erb + 1e-3)), interpolation="nearest", aspect="auto")

    print(band_processor.band_bins)
    # joined_string = ",".join(a_list)
    # print(np.round(band_processor.band_freqs, 0))
    print(str(band_processor.band_freqs).replace("  ", ", "))
    # %%
    # entropy bands
    from tqdm import tqdm
    from bd_rd.processing.blocks import STFT
    from scipy.special import entr

    SR = 48000
    config = bd.utils.Class_Dict(
        sr=SR,
        framesize=int(0.01 * SR),
        zero_pad_end=False,
        pre_emphasis=False,
        pre_gain=1,
        window="bd",
        ola_ratio=4,
        max_freq=20000,
        stft_gain=1,
        value_type=bd.T_INT16F,
        dc_removal=True,
    )
    stft_processor = STFT(config=config)

    path = "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/48Kdataset/train/"
    # ignoring sr here
    name_list = os.listdir(path)
    name_list = [
        os.path.join(path, name) for name in name_list if (name.endswith(".wav"))
    ]
    speech_stack = np.zeros((5 * 48000,))
    quant_steps = 100

    def normalize01(x):
        x_min = np.min(x)
        x = (x - x_min) / (np.max(x) - x_min)
        return x

    # spec_entropy = np.zeros()
    spec_entropy = np.zeros((800,))
    for name in tqdm(name_list):
        curr_speech, _ = bd.io.read_wav(os.path.join(path, name), fmt=bd.T_F)
        curr_stft = stft_processor.compute_np(curr_speech)
        # curr_stft = curr_stft[:,:321]
        curr_stft = np.log(np.abs(curr_stft + 1e-5))
        curr_stft = normalize01(curr_stft)
        curr_stft = np.floor(curr_stft * 50).astype(int)
        curr_stft = np.sort(curr_stft)
        # print(curr_stft[1,:], np.max(curr_stft[1,:]))

        curr_spec_entropy = []
        for bin in range(curr_stft.shape[1]):
            dist = np.zeros((50,))
            for val in range(50):
                dist[val] = np.sum(curr_stft[:, bin] == val)
            dist /= 50
            bin_entropy = np.sum(entr(dist))
            curr_spec_entropy.append(bin_entropy)

        curr_spec_entropy = np.array(curr_spec_entropy)
        spec_entropy += curr_spec_entropy

    spec_entropy /= len(name_list)
    spec_inv_entropy = 1 - normalize01(spec_entropy)

    # print(curr_spec_entropy)

    # %%

    plt.figure()
    plt.plot(np.arange(0, 25 * 321, 25), spec_inv_entropy)
    plt.xscale("log")
    plt.show()

    # %%
    min_freq = 0
    # max_freq = 24000
    max_freq = 8000
    speech_mass = np.cumsum(normalize01(spec_entropy[:267]))
    plt.figure()
    # plt.plot(20*np.log10(speech_freq**2))
    plt.plot(spec_entropy[:267])
    # plt.xscale('log')
    plt.xlim([0, 10])
    plt.grid()
    plt.show()

    plt.figure()
    plt.title("entr mass")
    plt.plot(speech_mass)
    # plt.xscale('log')
    plt.grid()
    plt.show()
    n_band = 48
    band_y = np.linspace(min_freq, speech_mass[-1], n_band)
    # print(band_y)
    band_x = []
    ax = np.linspace(0, speech_mass.shape[0], speech_mass.shape[0])
    for en in band_y:
        # print(en)
        # curr_x = np.where(np.argmax(speech_mass < en))
        temp = ax[speech_mass < en]
        if temp.shape[0] == 0:
            curr_x = 0
        else:
            curr_x = np.argmax(temp)
        # print(curr_x)
        band_x.append(curr_x)

    band_x = np.array(band_x)
    plt.figure()
    plt.plot(band_x)
    # plt.xscale('log')
    plt.grid()
    plt.show()
    print(band_x.shape)
    print(band_x)
    print(np.diff(band_x))
    print("copy this -----------------")
    print(",".join([str(x) for x in band_x]))

# %%
    # A WEIGHTED BANDS
