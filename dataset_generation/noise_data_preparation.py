# %%
import bd_rd as bd
import numpy as np
import os
import sys
import subprocess
from tqdm import tqdm

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)


def slice_dataset(input_path, output_path, slice_len):
    file_list = os.listdir(input_path)
    file_list = [os.path.join(input_path, x) for x in file_list]
    for filename in tqdm(file_list):
        x, sr = bd.io.read_wav(filename, fmt=bd.T_INT16F)
        file_len = x.shape[0]/sr
        trim_start = 0
        while trim_start < file_len - 2:
            out_name = os.path.join(
                output_path,
                os.path.basename(filename[:-4]) + "_" + str(trim_start) + ".wav",
            )
            
            process = subprocess.check_call(
                ["sox", filename, out_name, "trim", str(trim_start), str(slice_len)],
            )
            trim_start += slice_len
            # print(["sox", filename, out_name, "trim", str(trim_start), str(slice_len)])
            # print(trim_start, file_len)


# %%
slice_dataset(
    "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/noise/CAR-NOISE/ART_noise",
    "/home/disks/sdb/danielefoscarin/local_datasets/car_ART_sliced",
    slice_len=8,
)
# %%
