# %%
# Libraries and paths
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from pf_utils import tb_map_subprocess, clean_probe_dir
import bd_rd as bd
from bd_rd.io.probes import read_probe_file
from bd_rd.signal import align
from bd_rd.processing.blocks import STFT
from scipy.signal import lfilter
from scipy.signal.filter_design import bilinear

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/disks/ssdata/danielefoscarin/TSD_thesis/bd_tf2/bin/python; version: 3.8.10

import numpy as np
import matplotlib.pyplot as plt

SAVE_RESIDUAL = False


class Target_Generator:
    def __init__(
        self,
        bin_folder,
        temp_folder,
        sr=16000,
        echo_name="echo.wav",
        clean_name="ne_reverb_clean.wav",
    ):
        """
        Parameters
        ----------
        data_folder : string
            Path of the folder containing scenario files.
        bin_folder : string
            Path of the folder containing the tbmap executable and .conf .
        temp_folder : string
            Path of the folder to write tbmap output and probes.
        sr : integer
            Sample rate of input files.

        """

        self.bin_folder = bin_folder
        self.sr = sr
        self.echo_name = echo_name
        self.clean_name = clean_name
        self.temp_folder = temp_folder

        config = bd.utils.Class_Dict(
            sr=self.sr,
            framesize=int(0.01 * self.sr),
            zero_pad_end=False,
            pre_emphasis=True,
            pre_gain=1,
            window="bd",
            ola_ratio=4,
            max_freq=20000,
            stft_gain=1,
            value_type=bd.T_INT16F,
            dc_removal=True,
        )
        self.stft_processor = STFT(config=config)
        self.stft_processor.b_wola.out_idx = 1
        # prepare A-weighting
        self.a_coeffs = self._a_weighting_coeffs(self.sr)

    def _a_weighting_coeffs(self, sr):
        """Computes the coefficients for A-Weighting
        based on https://github.com/SiggiGue/pyfilterbank/blob/master/pyfilterbank/splweighting.py

        Parameters
        ----------
        sr : int
            Sample rate.

        Returns
        -------
        tuple (num, den)
            Numerator and denominator bilinear filter coefficients
        """
        f1 = 20.598997
        f2 = 107.65265
        f3 = 737.86223
        f4 = 12194.217
        A1000 = 1.9997
        numerators = [
            (2 * np.pi * f4) ** 2 * (10 ** (A1000 / 20.0)),
            0.0,
            0.0,
            0.0,
            0.0,
        ]
        denominators = np.convolve(
            [1.0, +4 * np.pi * f4, (2 * np.pi * f4) ** 2],
            [1.0, +4 * np.pi * f1, (2 * np.pi * f1) ** 2],
        )
        denominators = np.convolve(
            np.convolve(denominators, [1.0, 2 * np.pi * f3]), [1.0, 2 * np.pi * f2]
        )
        num, den = bilinear(numerators, denominators, sr)
        return (num, den)

    def _a_weighting(self, x):
        return lfilter(self.a_coeffs[0], self.a_coeffs[1], x)

    def get_target_talk_state(self, data_folder):
        """Compute and save on a raw file the target data and the spectrograms 
        processed by the tb_map, ready to be loaded by the tensorflow preprocessor.
        This will save 3 files on disk:
        scenario_spectra.raw: 

        Parameters
        ----------
        data_folder : str
            Path to the scenario folder

        Returns
        -------
        tupla (target_talk_state, tbmap_ts)
            Target talk state, talk state predicted by the baseline algorithm.
        """
        # BIG WARNING this is written on a little endian machine!!
        # check with "sys.byteorder"
        assert (
            sys.byteorder == "little"
        ), "WARNING, this machine is big endian, go to preprocessing.py and change to little_endian=False inside _load_scenario_raw()"

        # LOAD DATA -----------------------------------------------------------------
        # load echo signal from scenario folder
        echo, _ = bd.io.read_wav(
            os.path.join(data_folder, self.echo_name), fmt=bd.T_INT16F
        )
        # load ne reverberated noiseless signal from scenario folder
        ne_reverb_clean, _ = bd.io.read_wav(
            os.path.join(data_folder, self.clean_name), fmt=bd.T_INT16F
        )

        # Produce probes for the input pipeline
        tb_map_subprocess(
            self.bin_folder,
            data_folder,
            os.path.join(self.bin_folder, "tb_map_21_07"),
            os.path.join(data_folder, "sIn.wav"),
            os.path.join(data_folder, "rIn.wav"),
            os.path.join(self.temp_folder, "temp_sOut.wav"),
            os.path.join(self.bin_folder, "nrai20_aec_only.conf"),
            verbose=False,
            path_mode="full",
        )

        # move sOut in the scenario folder for debugging
        os.rename(
            os.path.join(self.temp_folder, "temp_sOut.wav"),
            os.path.join(data_folder, "temp_sOut.wav"),
        )

        # load the talk state predicted by the tbmap
        tbmap_ts = read_probe_file(
            os.path.join(self.temp_folder, "probe_POST_FILTER_talk_state.dat")
        ).data

        # load echo replica computed by the linear AEC
        replica = read_probe_file(
            os.path.join(
                self.temp_folder, "probe_ECHO_CANCELLER_constrained_echo_replica0.dat"
            )
        ).data

        # load the output of the linear AEC
        aec_out = read_probe_file(
            os.path.join(self.temp_folder, "probe_ECHO_CANCELLER_stft_s_out0.dat")
        ).data

        # TARGET GENERATION ----------------------------------------------------------
        # use NRAI VAD for the near end, in the ideal case with no noise and no echo,
        # then compute residual echo for the far end

        # make a silent file
        silence = np.zeros(echo.shape)
        silence_name = "temp_silence.wav"
        bd.io.write_wav(
            os.path.join(self.temp_folder, silence_name),
            silence,
            self.sr,
            fmt=bd.T_INT16F,
        )
        temp_clean_name = "temp_near_end_denoised.wav"

        # Near-end VAD on ideal case
        tb_map_subprocess(
            self.bin_folder,
            data_folder,
            os.path.join(self.bin_folder, "tb_map_21_07"),
            os.path.join(data_folder, self.clean_name),
            os.path.join(self.temp_folder, silence_name),
            os.path.join(self.temp_folder, temp_clean_name),
            os.path.join(self.bin_folder, "nrai20_with_NR.conf"),
            verbose=False,
            path_mode="full",
        )
        ne_VA_prob = read_probe_file(
            os.path.join(self.temp_folder, "probe_AI_SPP_vad_prob.dat")
        ).data

        # transform echo replica in audio and align the echo audio
        replica_audio = self.stft_processor.invert_np(replica)
        self.stft_processor.reset()

        # need to align 1 frame delay introduced by tbmap
        echo_aligned = np.zeros(echo.shape)
        echo_aligned[1 * int(self.sr * 0.01) :] = echo[: -1 * int(self.sr * 0.01)]

        # obtain residual echo estimantion
        echo_residual = echo_aligned - replica_audio

        # save files for debugging
        if SAVE_RESIDUAL:
            bd.io.write_wav(
                os.path.join(data_folder, "echo_replica.wav"),
                replica_audio,
                self.sr,
                fmt=bd.T_INT16F,
            )
            bd.io.write_wav(
                os.path.join(data_folder, "echo_residual.wav"),
                echo_residual,
                self.sr,
                fmt=bd.T_INT16F,
            )


        # using A-weighting on residual echo
        echo_w = self._a_weighting(echo_aligned)
        ne_reverb_clean_w = self._a_weighting(ne_reverb_clean)

        # threshold is defined as the max of (ne speech - 40dB, max level - 60dB)
        # TODO improve threshold ("100") management
        # TODO is it better to use a little less than -40dB? to avoid clash with AEC weights

        # consider speech level
        speech_level = np.std(ne_reverb_clean_w)
        # if speech level is less then -20, consider -20dB
        if speech_level / 100 < 2 ** 15 / 1000:
            speech_level = 2 ** 15 / 10

        # compute the residual level for every frame, rescaled for the threshold
        # in this way the threshold level sits at 1
        # independenlty to the presence of ne speech
        echo_residual_frame = np.zeros((ne_VA_prob.shape))
        for i in range(echo_residual_frame.shape[0]):
            echo_residual_frame[i] = np.std(
                100
                * echo_w[
                    int(i * self.sr * 0.01) : int((i + 1) * self.sr * 0.01)
                ]
                / speech_level
            )

        # quantize the residual level into a binary AD
        # do not apply smoothing
        # NB the NN will not see this AD!
        residual_activity = self._quantize_VA(
            echo_residual_frame,
            th_VA=1,
            smooth_rate=None,
            # smooth_rate=1,
        )

        # clean generated wav files from probe folder
        os.remove(os.path.join(self.temp_folder, temp_clean_name))
        os.remove(os.path.join(self.temp_folder, silence_name))
        # clean probes from probe folder
        clean_probe_dir(self.temp_folder, verbose=False)

        # quantize VA
        ne_VA = self._quantize_VA(ne_VA_prob, smooth_rate=1)
        residual_activity = self._quantize_VA(residual_activity, smooth_rate=1)

        # talk state from VA
        target_talk_state = ne_VA + 2 * residual_activity

        # WRITE RAW OUTPUTS -----------------------------------------------------------
        out_spec_raw = np.concatenate(
            [
                aec_out,
                replica,
            ],
            axis=-1,
        )
        out_spec_raw.astype("complex64").tofile(
            os.path.join(data_folder, "scenario_spectra.raw")
        )
        out_vad_raw = np.concatenate(
            [
                np.expand_dims(echo_residual_frame, axis=-1),
                np.expand_dims(ne_VA, axis=-1),
            ],
            axis=-1,
        )
        out_vad_raw.astype("float").tofile(
            os.path.join(data_folder, "scenario_target_data.raw")
        )

        return (
            target_talk_state,
            tbmap_ts,
        )

    def _quantize_VA(self, x, th_VA=0.5, smooth_rate=1):
        if smooth_rate is not None:
            y = bd.utils.SMOOTH(x, bd.utils.SMOOTH_RATE(smooth_rate), axis=-1)
        else:
            # FIXED very bad bug here
            y = x.copy()
        y[y >= th_VA] = 1
        y[y < th_VA] = 0
        return y


# %%
if __name__ == "__main__":
    sr = 16000
    bin_folder = "/home/danielefoscarin/LOCAL/SSD/bin"
    sox_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_sox"
    slicing_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_slice"
    tbmap_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_tbmap"

    config = bd.utils.Class_Dict(
        sr=sr,
        framesize=int(0.01 * sr),
        zero_pad_end=False,
        pre_emphasis=True,
        pre_gain=1,
        window="bd",
        ola_ratio=4,
        max_freq=20000,
        stft_gain=1,
        value_type=bd.T_INT16F,
        dc_removal=True,
    )
    stft_processor = STFT(config=config)
    stft_processor.b_wola.out_idx = 1

    # echo_replica_stft = read_probe_file('/home/disks/ssdata/danielefoscarin/generated/dataset_2110/train_scenario_0/probe_ECHO_CANCELLER_constrained_echo_replica0.dat').data

    target_generator = Target_Generator(bin_folder, temp_folder=tbmap_temp_folder)
    talk_state, _ = target_generator.get_target_talk_state(
        "/home/disks/ssdata/danielefoscarin/generated/dataset_prova/train_scenario_1"
    )

    plt.figure()
    plt.plot(talk_state)
    plt.show()
    
    # plt.figure()
    # plt.plot(echo_residual_frame)
    # plt.show()


# %%
    p = "/home/disks/ssdata/danielefoscarin/generated/dataset_prova/train_scenario_1/scenario_target_data.raw"
    # p = "/home/disks/ssdata/danielefoscarin/generated/dataset_1212/val_scenario_99/scenario_target_data.raw" 
    asd = np.fromfile(p,
    dtype="float",
    )
    asd = np.reshape(asd, (2000,2))
    print(asd.shape)
    plt.figure()
    plt.plot(asd[:,0])
    plt.yscale("log")
    # plt.plot(asd[:,1])
    plt.show()
# %%
