# %%
# libraries
import bd_rd as bd
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from dataset_generation.scenario_generator import *
from dataset_generation.target_generator import Target_Generator
from bd_rd.signal import align

# from spectral_bands import Spectral_Bands

import numpy as np
from random import shuffle

# import scipy
from matplotlib import pyplot as plt
import os
from icecream import ic
from tqdm import tqdm
import pyroomacoustics as pra
import json

ic.disable()
# %%
# slice tracks and make scenarios


root_path = "/home/disks/sdb/danielefoscarin/nonlinear_device_test"
sox_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_sox"
dataset_root_folder = "/home/disks/ssdata/danielefoscarin/test_sets/devices_test_sets"
echo_conf = None
sequence_len = 20
sr = 16000
speaker_levels = ["0dB", "-6dB", "-12dB"]
noise_levels = [-6, -20]
echo_speech_ratio = 10 ** (12 / 20)
EXIST_OK = True


def slice_in_memory(sig, sequence_len, sr):
    seq_l = []
    start = 0
    end = start + sequence_len * sr
    while end <= sig.shape[0]:
        seq_l.append(sig[start:end])
        start = end
        end = start + sequence_len * sr
    return seq_l


def rs_n_read(filename, sox_temp_folder=sox_temp_folder, sr=sr):
    filename_rs_path, _ = resample_on_filename(filename, sox_temp_folder, sr)
    sig, _ = bd.io.read_wav(filename_rs_path, fmt=bd.T_INT16F)
    os.remove(filename_rs_path)
    return sig


def set_levels(
    speech, noise, echo, echo_speech_ratio, speech_noise_ratio, ref_level=1500
):
    speech_std = np.std(speech)
    noise_std = np.std(noise)
    echo_std = np.std(echo)

    # set echo to reference level
    echo /= echo_std
    echo *= ref_level
    echo_std = np.std(echo)

    # set speech level relative to echo
    speech /= speech_std
    speech *= ref_level / echo_speech_ratio
    speech_std = np.std(speech)

    # set noise level relative to speech
    noise /= noise_std
    noise *= speech_std / speech_noise_ratio
    noise_std = np.std(noise)

    return speech, noise, echo


class linear_room:
    def __init__(self):
        self.material_choice = "panel_fabric_covered_6pcf"
        self.room_dim = [6, 4, 2.7]
        self.mic_pos = [3, 2, 1]
        self.fe_pos = [3, 2.2, 1]
        self.ne_pos = [3, 2.5, 1.5]
        self.room_max_order = 5

    def echo_reverberation(self, echo):
        room = pra.ShoeBox(
            self.room_dim,
            # TODO understand fs
            fs=44100,
            max_order=self.room_max_order,
            materials=pra.Material(energy_absorption=self.material_choice),
        )
        room.add_source(self.fe_pos, signal=echo)
        room.add_microphone(self.mic_pos)
        room.simulate()
        out = room.mic_array.signals[0, :][0 : echo.shape[0]]
        return out

    def speech_reverberation(self, speech):
        room = pra.ShoeBox(
            self.room_dim,
            fs=44100,
            max_order=self.room_max_order,
            materials=pra.Material(energy_absorption=self.material_choice),
        )
        room.add_source(self.ne_pos, signal=speech)
        room.add_microphone(self.mic_pos)
        room.simulate()
        out = room.mic_array.signals[0, :][0 : speech.shape[0]]
        return out


# def prepare_device_test_set(root_path):
devices_source_path = os.path.join(root_path, "devices")
tracks_path = os.path.join(root_path, "original_tracks")
devices_list = [name for name in os.listdir(devices_source_path)]

# load rin
rin = rs_n_read(os.path.join(tracks_path, "rIn.wav"))

# load ne noise
ne_noise_rs_path, _ = resample_on_filename(
    os.path.join(tracks_path, "ne_noise.wav"), sox_temp_folder, sr
)
ne_noise, _ = bd.io.read_wav(ne_noise_rs_path, fmt=bd.T_INT16F)
os.remove(ne_noise_rs_path)

# load ne speech
ne_speech_rs_path, _ = resample_on_filename(
    os.path.join(tracks_path, "ne_speech.wav"), sox_temp_folder, sr
)
ne_speech, _ = bd.io.read_wav(ne_speech_rs_path, fmt=bd.T_INT16F)
os.remove(ne_speech_rs_path)

ne_speech_std = np.std(ne_speech)
ne_noise_std = np.std(ne_noise)
print(ne_speech_std, ne_noise_std)


for device in tqdm(devices_list):

    # load and slice device tracks
    device_path = os.path.join(dataset_root_folder, device)
    os.makedirs(device_path, exist_ok=EXIST_OK)
    echo_l = []
    echo_std_l = []
    for level in speaker_levels:
        echo_l.append(
            rs_n_read(os.path.join(devices_source_path, device, level + ".wav"))
        )

    for level_i, level in enumerate(speaker_levels):
        ic(level, " speaker")
        level_path = os.path.join(dataset_root_folder, device, level + "_speaker")
        os.makedirs(level_path, exist_ok=EXIST_OK)
        room = linear_room()
        echo_reverb = room.echo_reverberation(echo_l[level_i])
        ne_speech_reverb = room.speech_reverberation(ne_speech)

        for noise_level in noise_levels:
            ic(noise_level)
            noise_level_path = os.path.join(level_path, str(noise_level) + "dB_noise")
            os.makedirs(noise_level_path, exist_ok=EXIST_OK)

            # set signal levels
            curr_speech, curr_noise, curr_echo = set_levels(
                ne_speech_reverb,
                ne_noise,
                echo_reverb,
                echo_speech_ratio,
                speech_noise_ratio=noise_level,
            )

            # TODO more efficine incorporate slicing inside writing cycle
            speech_seq_l = slice_in_memory(curr_speech, sequence_len, sr)
            noise_seq_l = slice_in_memory(curr_noise, sequence_len, sr)
            rin_seq_l = slice_in_memory(rin, sequence_len, sr)
            echo_seq_l = slice_in_memory(curr_echo, sequence_len, sr)

            for i in tqdm(range(len(rin_seq_l))):
                scenario_path = os.path.join(
                    noise_level_path, "test_scenario_" + str(i)
                )
                os.makedirs(scenario_path, exist_ok=EXIST_OK)
                bd.io.write_wav(
                    os.path.join(scenario_path, "rIn.wav"),
                    rin_seq_l[i],
                    sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(scenario_path, "ne_reverb_clean.wav"),
                    speech_seq_l[i],
                    sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(scenario_path, "sIn.wav"),
                    speech_seq_l[i] + noise_seq_l[i] + echo_seq_l[i],
                    sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(scenario_path, "echo.wav"),
                    echo_seq_l[i],
                    sr,
                    fmt=bd.T_INT16F,
                )


# %%
# target generation
print("\n TARGET GENERATION \n\n")
bin_folder = "/home/danielefoscarin/LOCAL/SSD/bin"
tbmap_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_tbmap"
target_generator = Target_Generator(
    bin_folder=bin_folder, 
    temp_folder=tbmap_temp_folder
)

for device in devices_list:
    device_path = os.path.join(dataset_root_folder, device)
    speaker_levels = os.listdir(device_path)
    print(device_path)
    for speaker_level in speaker_levels:
        speaker_level_path = os.path.join(device_path, speaker_level)
        noise_levels = os.listdir(os.path.join(speaker_level_path))
        print(speaker_level_path)
        for noise_level in noise_levels:
            noise_level_path = os.path.join(speaker_level_path, noise_level)
            print(noise_level_path)
            test_scenario_list = [
                name for name in os.listdir(noise_level_path) if name.startswith("test")
            ]
            test_scenario_list.sort()
            for scenario in tqdm(test_scenario_list):
                curr_target, curr_tbmap_ts = target_generator.get_target_talk_state(
                    os.path.join(noise_level_path, scenario)
                )
                np.save(os.path.join(noise_level_path, scenario, "talk_state.npy"), curr_target)
                np.save(os.path.join(noise_level_path, scenario, "tbmap_ts.npy"), curr_tbmap_ts)
# %%
# TODO script to add another device and target only that one