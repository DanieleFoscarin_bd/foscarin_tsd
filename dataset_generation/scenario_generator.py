# %%
import os
import sys
currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)
from bd_rd.signal import align
from pf_utils import *

# from TS_oracle import *
import bd_rd as bd
from numba import jit

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/disks/ssdata/danielefoscarin/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
sys.path.append("../")
import numpy as np
import pyroomacoustics as pra
from scipy.signal import butter, lfilter
import json

SR = 16000
FORMAT_MIN = -2**15
FORMAT_MAX = 2**15 - 1


@jit
def fast_sigmoid(x):
    y = np.where(x >= 0, 1 / (1 + np.exp(-x)), np.exp(x) / (1 + np.exp(x)))
    return y


@jit
def fast_gru(x, y_past, wf, wh, uf, uh, bf):
    f = fast_sigmoid(wf * x + uf * y_past + bf)
    y = f * y_past + (1 - f) * np.tanh(wh * x + uh * f * y_past)
    return y


@jit
def fast_gru_cycle(sig, wf, wh, uf, uh, bf):
    y0 = 0
    y = np.zeros_like(sig)
    for n in range(len(sig)):
        y[n] = fast_gru(sig[n], y0, wf, wh, uf, uh, bf)
        y0 = y[n]
    return y


class Scenario_Generator:
    def __init__(self, sr, sox_temp_folder):
        """Class that manages the generation of a hands-free telecom scenario

        Parameters
        ----------
        sr : int
            Sample rate.
        sox_temp_folder : string
            Path to the temporary write sox folder
        """
        self.sr = sr
        self.sox_temp_folder = sox_temp_folder
        self.speed_of_sound = 343

    def _tanned_distortion(
        self, in_sig, l, relative_gain_db=0, preserve_level=True, mix=1
    ):
        gain = 10 ** (relative_gain_db / 20)
        sig = in_sig.copy()

        in_std = np.std(sig)
        sig /= in_std
        sig *= gain

        sig = (1 - l) * np.tanh(sig) + l * sig
        sig = np.clip(sig, -1, 1)
        sig = sig / gain
        if preserve_level:
            sig /= np.std(sig)
            sig *= in_std
        return mix * sig + (1 - mix) * in_sig

    def _gru_distortion(
        self, in_sig, wh, wf, uf, uh, bf, relative_gain_db=0, preserve_level=True, mix=1
    ):
        gain = 10 ** (relative_gain_db / 20)
        sig = in_sig.copy()
        in_std = np.std(sig)
        # set std to 1: same behaviour with different input levels
        sig /= in_std
        # add relative gain
        sig *= gain
        # correct range -12 db since gru is too harsh
        sig *= 0.20
        # call jitted fast function
        y = fast_gru_cycle(sig, wf, wh, uf, uh, bf)
        y = y / gain
        if preserve_level:
            y /= np.std(y)
            y *= in_std
        return mix * y + (1 - mix) * in_sig

    def _sinful_distortion(
        self, in_sig, l, relative_gain_db=0, preserve_level=True, mix=1
    ):
        gain = 10 ** (relative_gain_db / 20)
        sig = in_sig.copy()

        in_std = np.std(sig)
        sig = sig / in_std
        sig = gain * sig

        sig = np.clip(sig, -np.pi / 2, np.pi / 2)
        sig = np.sin(sig) * (1 + l)
        sig = np.clip(sig, -1, 1)
        sig = sig / gain
        if preserve_level:
            sig = sig * in_std
        return mix * sig + (1 - mix) * in_sig

    def _butter_bandpass(self, lowcut, highcut, order=2):
        nyq = 0.5 * self.sr
        low = lowcut / nyq
        high = highcut / nyq
        b, a = butter(order, [low, high], btype="band")
        return b, a

    def _butter_bandpass_filter(self, data, lowcut, highcut, order=2):
        b, a = self._butter_bandpass(lowcut, highcut, order=order)
        y = lfilter(b, a, data)
        return y

    def _fix_source_position_bad(self, mic_pos, source_dist, room_dim):
        source_pos = np.zeros((3,))
        for i in range(3):
            if mic_pos[i] <= room_dim[i] / 2:
                source_pos[i] = mic_pos[i] + source_dist[i]
            else:
                source_pos[i] = mic_pos[i] - source_dist[i]

        source_pos = np.round(source_pos, decimals=1)
        return source_pos

    def _LEM_generator(self, ne, rin, ne_noise):
        """Generate a loudspeaker-room-microphone scenario.
        Using random chosen speaker distortion, room size, absorption,
        source and mic positions.

        Parameters
        ----------
        ne : np array
            Near-end speech sequence.
        rin : np array
            Far-end sequence
        ne_noise : np array
            Near-end noise sequence.

        Returns
        -------
        dict(str, ndarray)
            ne_reverb, echo, ne_noise_reverb
        """

        # RANDOM VARIABLES ----------------------------------------
        # every random variable here

        # 1) prefilter
        pre_filter_on = bool(np.random.choice([True, False], p=[0.5, 0.5]))
        pre_low_cut = np.random.randint(50, 1000)
        pre_high_cut = np.random.randint(4000, 7500)

        # 2) distortion
        dist_type = np.random.choice(["tan", "gru", "none"], p=[0.2, 0.75, 0.05])
        dist_l = np.random.rand()  # used only with tanned distortion
        dist_p = {
            "wh": np.random.uniform(0.5, 3),
            "wf": np.random.uniform(0, 4),
            "uf": np.random.uniform(0, 3),
            "uh": np.random.uniform(0, 1),
            "bf": np.random.uniform(-1, 0.5),
        }
        dist_gain = np.random.uniform(-6, 6)
        dist_mix = np.random.uniform(0, 1)

        # 3) room reverb
        fe_reverb_on = bool(np.random.choice([True, False], p=[0.8, 0.2]))
        ne_reverb_on = bool(np.random.choice([True, False], p=[0.7, 0.3]))
        material_choice = np.random.randint(0, 10)
        room_dim = [
            np.random.uniform(3, 8),
            np.random.uniform(3, 8),
            np.random.uniform(2.5, 5),
        ]
        mic_pos = [
            np.random.uniform(1.1, room_dim[0] - 1.1),
            np.random.uniform(1.1, room_dim[1] - 1.1),
            np.random.uniform(1.1, room_dim[2] - 1.1),
        ]
        ne_dist = np.random.uniform(0.2, 1, size=(3,))
        ne_direction = np.random.choice([1, -1], (3,))
        ne_pos = ne_dist * ne_direction
        fe_dist = np.random.uniform(0.2, 1, size=(3,))
        fe_direction = np.random.choice([1, -1], (3,))
        fe_pos = fe_dist * fe_direction
        room_max_order = np.random.randint(1, 5)
        m_list = [
            "concrete_floor",
            "brick_wall_rough",
            "hard_surface",
            "wood_1.6cm",
            "carpet_thin",
            "blinds_open",
            "curtains_velvet",
            "felt_5mm",
            "panel_fabric_covered_6pcf",
            "audience_wooden_chairs_2_m2",
        ]
        rand_material = m_list[material_choice]

        # 4) level control
        sig_echo_ratio_db = np.random.uniform(-18, 0)
        if np.random.rand() < 0.1:
            # clipping microphone 10% of scenario
            echo_max_db = np.random.uniform(0, 3)
        elif np.random.rand() < 0.5:
            # high volume scenarios
            echo_max_db = np.random.uniform(-6, 0)
        else:
            # low volume scenarios
            echo_max_db = np.random.uniform(-15, -6)

        # 5) mic filter
        mic_filter_on = bool(np.random.choice([True, False], p=[0.5, 0.5]))
        mic_low_cut = np.random.randint(50, 500)
        mic_high_cut = np.random.randint(5000, 7550)
        # END OF RANDOM ASSIGN------------------------
        # Pass a json for reproducibility ------------
        log_dict = {
            "pre_filter": {
                "pre_filter_on": pre_filter_on,
                "pre_low_cut": pre_low_cut,
                "pre_high_cut": pre_high_cut,
            },
            "distorsion": {
                "dist_type": dist_type,
                "dist_l": dist_l,
                "dist_p": dist_p,
                "dist_gain": dist_gain,
                "dist_mix": dist_mix,
            },
            "room_reverb": {
                "fe_reverb_on": fe_reverb_on,
                "ne_reverb_on": ne_reverb_on,
                "material_choice": material_choice,
                "rand_material": rand_material,
                "room_dim": room_dim,
                "mic_pos": mic_pos,
                "ne_dist": ne_dist.tolist(),
                "ne_direction": ne_direction.tolist(),
                "ne_pos": ne_pos.tolist(),
                "fe_dist": fe_dist.tolist(),
                "fe_direction": fe_direction.tolist(),
                "room_max_order": room_max_order,
            },
            "level_control": {
                "sig_echo_ratio_db": sig_echo_ratio_db,
                "echo_max_db": echo_max_db,     
            },
            "mic_filter": {
                "mic_filter_on": mic_filter_on,
                "mic_low_cut": mic_low_cut,
                "mic_high_cut": mic_high_cut,
            },
        }

        # PREFILTER ----------------------------------
        # bandpass before distortion to simulate low quality far end
        if pre_filter_on:
            filt_order = 2
            rin = self._butter_bandpass_filter(
                rin, pre_low_cut, pre_high_cut, filt_order
            )

        # DISTORTION ---------------------------------
        # nonlinear distortion
        if dist_type == "tan":
            rin_d = self._tanned_distortion(
                rin,
                dist_l,
                relative_gain_db=dist_gain,
                mix=dist_mix,
            )
        elif dist_type == "gru":
            rin_d = self._gru_distortion(
                rin,
                dist_p["wh"],
                dist_p["wf"],
                dist_p["uf"],
                dist_p["uh"],
                dist_p["bf"],
                relative_gain_db=dist_gain,
                mix=dist_mix,
            )
        elif dist_type == "none":
            # no distortion is applied
            rin_d = rin.copy()

        # ROOM REVERB --------------------------------
        # far end
        if fe_reverb_on < 0.8:
            room = pra.ShoeBox(
                room_dim,
                fs=44100,
                max_order=room_max_order,
                materials=pra.Material(energy_absorption=rand_material),
            )

            room.add_source(fe_pos, signal=rin_d)
            room.add_microphone(mic_pos)
            room.simulate()
            # return microphone signal
            echo = room.mic_array.signals[0, :]
            echo = echo[0 : rin.shape[0]]
        else:
            # if not reverb, still delay the rin_d by the same time of reverb
            dist = np.linalg.norm(fe_pos - mic_pos)
            delay = int((dist / self.speed_of_sound) * self.sr) + 160
            echo = np.pad(rin_d, (delay, 0))
            echo = echo[0 : rin_d.shape[0]]

        # near end
        if ne_reverb_on < 0.7:
            room = pra.ShoeBox(
                room_dim,
                fs=44100,
                max_order=room_max_order,
                materials=pra.Material(energy_absorption=rand_material),
            )

            room.add_source(ne_pos, signal=ne)
            room.add_microphone(mic_pos)
            room.simulate()
            ne_reverb = room.mic_array.signals[0, :]
            ne_reverb = ne_reverb[0 : ne.shape[0]]

            room = pra.ShoeBox(
                room_dim,
                fs=44100,
                max_order=room_max_order,
                materials=pra.Material(energy_absorption=rand_material),
            )

            room.add_source(ne_pos, signal=ne_noise)
            room.add_microphone(mic_pos)
            room.simulate()
            ne_noise_reverb = room.mic_array.signals[0, :]
            ne_noise_reverb = ne_noise_reverb[0 : ne_noise.shape[0]]

        else:
            # do not apply reverb
            ne_reverb = ne
            ne_noise_reverb = ne_noise

        # NEAR-FAR LEVEL ----------------------------
        # need a high range of levels that includes same cases with clipping
        # using max and not std for echo here since we are focusing on clipping
        echo_max = 10 ** (echo_max_db /20) * 2 ** 15 - 1
        sig_std = np.std(ne_reverb + ne_noise_reverb)
        sig_echo_ratio = 10 ** (sig_echo_ratio_db / 20)
        # find current echo max level in range [-32768, 32767]
        echo_max_init = np.max(np.abs(echo))
        # rescale to set current echo max to 1
        echo /= echo_max_init
        # rescale to set current echo max to the target value
        echo *= echo_max
        echo_std = np.std(echo)
        # from this point we focus on std to set the relative sig-echo level
        # rescale to set the ne std to 1
        ne_reverb /= sig_std
        ne_noise_reverb /= sig_std
        # rescale to set the ne std to the target level
        ne_reverb *= echo_std * sig_echo_ratio
        ne_noise_reverb *= echo_std * sig_echo_ratio
        # hard clip at the maximum level
        # assuming only echo can clip
        echo = np.clip(echo, FORMAT_MIN+100, FORMAT_MAX-100)

        # MIC FILTER ----------------------------------
        # apply a bandpass
        if mic_filter_on:
            mic_filt_order = 2
            echo = self._butter_bandpass_filter(
                echo, mic_low_cut, mic_high_cut, mic_filt_order
            )
            ne_reverb = self._butter_bandpass_filter(
                ne_reverb, mic_low_cut, mic_high_cut, mic_filt_order
            )
            ne_noise_reverb = self._butter_bandpass_filter(
                ne_noise_reverb, mic_low_cut, mic_high_cut, mic_filt_order
            )

        # OUTPUT ----------------------------------------
        # return microphone signal and log
        out = {
            "ne_reverb": ne_reverb,
            "ne_noise_reverb": ne_noise_reverb,
            "echo": echo,
            "log_dict": log_dict
        }
        return out

    def _place_in_time(self, sig_list, final_len):
        """Place a list of signals in a length with random
        non-overlapping positions

        Parameters
        ----------
        sig_list : array
            List of audio signals.
        final_len : int
            Length of the final sequence, in samples.

        Returns
        -------
        numpy array
            Output sequence with shape (final_len,)
        """
        sig_silence_len = final_len - sum([len(x) for x in sig_list])
        silence_partition = np.random.rand(len(sig_list) + 1)
        silence_partition = silence_partition / sum(silence_partition)
        silence_partition = silence_partition * sig_silence_len
        t_axes = np.zeros((final_len,))
        t_pos = 0
        for i in range(len(sig_list)):
            t_pos += np.int(silence_partition[i])
            t_axes[t_pos : t_pos + len(sig_list[i])] = sig_list[i]
            t_pos += len(sig_list[i])
        return t_axes

    def scenario_generator_on_names(
        self, f_e_pick, n_e_pick, f_e_noise, n_e_noise, final_len, noise_gain=None
    ):
        """Generate scenario from lists of filenames for speech and noise.

        Parameters
        ----------
        f_e_pick : list
            List of paths to far end speech files.
        n_e_pick : list
            List of paths to near end speech fiiles.
        f_e_noise : list
            List of paths to far end noise files
        n_e_noise : list
            List of path to near end noise files
        final_len : int
            Length of the sequences in samples
        noise_gain : int, optional
            Level of noise in dB w.r.t the speech files, by default None

        Returns
        -------
        dict(str, ndarray)
            rin, sin, rin_clean, ne_clean, echo, ne_reverb
        """

        # RESAMPLE AND LOAD FILES ----------------------------------------
        # noise files should be already resampled and with right length
        # TODO move here handling and resampling of noise files
        assert (
            n_e_noise.shape[0] == f_e_noise.shape[0] == final_len
        ), "ERROR, noise length must be equal to the sequence length"

        f_e_pick_rs = []
        for filename in f_e_pick:
            filename_rs, safe_prefix = resample_on_filename(
                filename, self.sox_temp_folder, self.sr
            )
            f_e_pick_rs.append(filename_rs)

        rin_list = []
        rin_total_len = 0
        for filename in f_e_pick_rs:
            curr_rin, check_sr = bd.io.read_wav(filename, fmt=bd.T_INT16F)
            rin_total_len += curr_rin.shape[0]
            rin_list.append(curr_rin)

        n_e_pick_rs = []
        for filename in n_e_pick:
            filename_rs, safe_prefix = resample_on_filename(
                filename, self.sox_temp_folder, self.sr
            )
            n_e_pick_rs.append(filename_rs)

        clean_list = []
        clean_total_len = 0
        for filename in n_e_pick_rs:
            curr_clean, check = bd.io.read_wav(filename, fmt=bd.T_INT16F)
            clean_total_len += curr_clean.shape[0]
            clean_list.append(curr_clean)

        # REMOVE resampling temp files
        for filename in os.listdir(self.sox_temp_folder):
            if filename.startswith(safe_prefix):
                os.remove(os.path.join(self.sox_temp_folder, filename))

        # create far end sequence ---------------------------------
        rin_clean = self._place_in_time(rin_list, final_len)

        # FE NOISE LEVEL -----------------------------------------
        # add environmental noise to rin:
        if noise_gain is None:
            # random dB reduction
            # gain_f_e = np.random.uniform(-18, 0)
            gain_f_e = np.random.uniform(-24, 6)
        else:
            gain_f_e = noise_gain
        # set f_e_noise to far end speech std
        f_e_noise = np.std(rin_clean) * f_e_noise / np.std(f_e_noise)
        f_e_noise *= 10 ** (gain_f_e / 20)
        # add noise to rin clean
        rin = rin_clean + f_e_noise

        # create near end sequence -----------------------------
        ne_clean = self._place_in_time(clean_list, final_len)

        # NE NOISE LEVEL ------------------------------------------
        # add environmental noise to near end:
        if noise_gain is None:
            # random dB reduction
            gain_n_e = np.random.uniform(-18, 0)
        else:
            gain_n_e = noise_gain
        # set f_e_noise to near end speech std
        n_e_noise = np.std(ne_clean) * n_e_noise / np.std(n_e_noise)
        n_e_noise *= 10 ** (gain_n_e / 20)

        # compute LEM ------------------------------------------
        lem_dict = self._LEM_generator(ne_clean, rin, n_e_noise)
        sin = lem_dict["ne_reverb"] + lem_dict["ne_noise_reverb"] + lem_dict["echo"]

        # OBSOLETE
        # anti clipping check ----------------------------------
        # if np.max(sin) >= 2 ** 15:
        #     lem_dict["ne_noise_reverb"] /= 2
        #     lem_dict["ne_reverb"] /= 2
        #     lem_dict["echo"] /= 2
        #     sin /= 2

        # OUTPUT -----------------------------------------------
        out = {
            "rin": rin,
            "sin": sin,
            "echo": lem_dict["echo"],
            "ne_reverb": lem_dict["ne_reverb"],
            "ne_noise_reverb": lem_dict["ne_noise_reverb"],
            "lem_conf": lem_dict["log_dict"],
        }

        return out


# %%
if __name__ == "__main__":
    sox_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_sox"
    x, _ = bd.io.read_wav(
        "/home/disks/ssdata/danielefoscarin/generated/dataset_2110/train_scenario_0/clean_near_end.wav"
    )
    processor = Scenario_Generator(
        16000,
        sox_temp_folder,
    )
    y = processor._gru_distortion(x, 1, 2, 1.5, 0.5, 1, relative_gain_db=6, mix=1)
    bd.io.write_wav("original.wav", x, 16000, fmt=bd.T_INT16F)
    bd.io.write_wav("gru_dist.wav", y, 16000, fmt=bd.T_INT16F)

    plt.figure()
    plt.plot(y, alpha=0.5)
    plt.plot(x, alpha=0.5)
    plt.show()
    print(np.std(x), np.std(y))
# %%
