# %%
# libraries
import bd_rd as bd
import os
import sys
import argparse

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir)

from dataset_generation.scenario_generator import *
from dataset_generation.target_generator import Target_Generator
from bd_rd.signal import align

# from spectral_bands import Spectral_Bands

import numpy as np
from random import shuffle

# import scipy
from matplotlib import pyplot as plt
import os
from tqdm import tqdm


class Dataset_Generator:
    def __init__(
        self,
        slicing_temp_folder,
        sox_temp_folder,
        sequence_len=20,
        segment_len=4,
        n_segments=4,
        sr=16000,
        mode="train",
    ):
        """[summary]

        Parameters
        ----------
        slicing_temp_folder : string
            Path to the write folder for temporary slicing files
        sox_temp_folder : string
            Path to the sox write folder
        sequence_len : int, optional
            Length of the sequence in seconds, by default 20
        segment_len : int, optional
            Maximum length of the speech segments in seconds, by default 4
        n_segments : int, optional
            Number of speech segments in a sequence, by default 4
        sr : int, optional
            Sample rate, by default 16000
        mode : str, optional
            'train' or 'test'. When 'train' it keep part of the files
            as validation set. By default 'train'
        """
        self.slicing_temp_folder = slicing_temp_folder
        self.sox_temp_folder = sox_temp_folder
        self.sr = sr
        self.mode = mode
        self.min_residual_segment = 1  # in seconds
        if self.mode == "train":
            self.val_ratio = 0.2
        elif self.mode == "test":
            self.val_ratio = 0
        self.sequence_len = sequence_len  # in seconds
        self.segment_len = segment_len
        self.n_segments = n_segments

        self.train_single_pool = []
        self.val_single_pool = []
        self.train_LR_pool = []
        self.train_LR_pool = []
        self.train_noise_pool = []
        self.val_noise_pool = []

        self.ugo_pool = []

        self.scenario_generator = Scenario_Generator(self.sr, self.sox_temp_folder)

    def add_source_single_clean(self, path):
        """Add the path to a clean single speech dataset.

        Parameters
        ----------
        path : string
            Path to the dataset folder.
        """
        name_list = os.listdir(path)
        name_list = [
            os.path.join(path, name) for name in name_list if (name.endswith(".wav"))
        ]
        val_list = name_list[: int(np.floor(len(name_list) * self.val_ratio))]
        train_list = name_list[int(np.floor(len(name_list) * self.val_ratio)) :]
        self.train_single_pool += train_list
        self.val_single_pool += val_list

    def add_UGO_scenarios(self, path):
        self.ugo_pool.append(path)

    def add_source_noise(self, path):
        """Add the path to a noise dataset

        Parameters
        ----------
        path : string
            Path to the dataset folder.
        """
        name_list = os.listdir(path)
        name_list = [
            os.path.join(path, name) for name in name_list if (name.endswith(".wav"))
        ]
        val_list = name_list[: int(np.floor(len(name_list) * self.val_ratio))]
        train_list = name_list[int(np.floor(len(name_list) * self.val_ratio)) :]
        self.train_noise_pool += train_list
        self.val_noise_pool += val_list
        pass

    def _make_noise_sequence(self,):
        seq_len = 0
        while seq_len < self.sequence_len * self.sr:
            # load a noise for the near end, resampled if necessary
            noise_rs_name, safe_prefix = resample_on_filename(
                self.noise_pool[self.noise_iter], self.sox_temp_folder, self.sr
            )
            noise_rs, _ = bd.io.read_wav(
                noise_rs_name,
                fmt=bd.T_INT16F,
            )
            # remove temp file for resampling temp folder
            os.remove(noise_rs_name)

            # trim leading or trailing zeros
            noise_rs = np.trim_zeros(noise_rs)

            # if the loaded noise is longer than the sequence
            if (
                noise_rs.shape[0]
                >= self.sequence_len * self.sr - seq_len
            ):
                if seq_len == 0:
                    # if it's the first noise in the sequence
                    noise_seq = noise_rs[
                        : self.sequence_len * self.sr - seq_len
                    ]
                else:
                    noise_rs = noise_rs[
                        : self.sequence_len * self.sr - seq_len
                    ]
                    noise_seq = np.concatenate(
                        (noise_seq, noise_rs), axis=0
                    )

            else:
                # the loaded noise is shorter than the sequence
                if seq_len == 0:
                    # if it's the first noise in the sequence
                    noise_seq = noise_rs[
                        : self.sequence_len * self.sr - seq_len
                    ]
                else:
                    noise_rs = noise_rs[
                        : self.sequence_len * self.sr - seq_len
                    ]
                    noise_seq = np.concatenate(
                        (noise_seq, noise_rs), axis=0
                    )

            # update seq_len
            seq_len = noise_seq.shape[0]
            # iterate through the noise pool
            if self.noise_iter < len(self.noise_pool) - 1:
                self.noise_iter += 1
            else:
                self.noise_iter = 0
        return noise_seq

    def generate_dataset(
        self,
        mode,
        out_folder,
        noise,
        n_repeat_pool=1,
    ):
        """Generate a dataset of hands-free telecom scenarios

        Parameters
        ----------
        mode : 'train','val','test'
            Type of dataset
        out_folder : string
            Path to the folder where the dataset is written
        noise : 'none','rand_SNR' or int,
            When 'none' no noise is added to the sequences.
            When 'rand_SNR' noise added according to scenario_generator_on_name + denoise half sequence
            When int noise is added at int SNR
        n_repeat_pool : int, optional
            Number of time the speech pool is created and consumed, by default 1
        """
        if mode == "train":
            self.single_pool = self.train_single_pool
            self.noise_pool = self.train_noise_pool
            out_name = "train_scenario_"
        elif mode == "val":
            self.single_pool = self.val_single_pool
            self.noise_pool = self.val_noise_pool
            out_name = "val_scenario_"
        elif mode == "test":
            self.single_pool = self.train_single_pool
            self.noise_pool = self.train_noise_pool
            out_name = "test" + str(noise) + "dB_"

        assert (
            mode == "train" or mode == "val" or mode == "test"
        ), "Error, mode must be 'train' or 'val' "

        assert len(self.single_pool) > 0, "ERROR, no validation data has been defined"

        for repeat in range(n_repeat_pool):
            print(f"generating dataset with pool repetition {repeat}")
            # shuffle the input data order
            shuffle(self.single_pool)
            shuffle(self.noise_pool)

            print("slicing clean speech...")
            for i, filename in tqdm(enumerate(self.single_pool)):
                signal, in_sr = bd.io.read_wav(filename, fmt=bd.T_INT16F)
                if signal.shape[0] <= in_sr * self.segment_len:
                    # if signal shorter than segment_len, use the entire file
                    bd.io.write_wav(
                        os.path.join(
                            self.slicing_temp_folder, str(i) + "_temp_whole.wav"
                        ),
                        signal,
                        in_sr,
                        fmt=bd.T_INT16F,
                    )
                else:
                    # else if signal is longer than segment_len, slice it and save it
                    for j in range(
                        int(np.floor(signal.shape[0] / (in_sr * self.segment_len)))
                    ):

                        if (
                            j
                            == np.floor(signal.shape[0] / (in_sr * self.segment_len))
                            - 1
                        ):
                            # write the last residual segment even if shorter than 4 seconds but longer than 1 seconds
                            seg_start = (j + 1) * in_sr * self.segment_len
                            if (
                                signal.shape[0] - seg_start
                                >= self.min_residual_segment * in_sr
                            ):
                                bd.io.write_wav(
                                    os.path.join(
                                        self.slicing_temp_folder,
                                        str(i) + "_" + str(j) + "_temp_sliced_last.wav",
                                    ),
                                    signal[seg_start:],
                                    in_sr,
                                    fmt=bd.T_INT16F,
                                )
                        else:
                            # write the sliced segments with length segment_len
                            seg_start = j * in_sr * self.segment_len
                            seg_end = (j + 1) * in_sr * self.segment_len
                            bd.io.write_wav(
                                os.path.join(
                                    self.slicing_temp_folder,
                                    str(i) + "_" + str(j) + "_temp_sliced.wav",
                                ),
                                signal[seg_start:seg_end],
                                in_sr,
                                fmt=bd.T_INT16F,
                            )

            sliced_name_list = os.listdir(self.slicing_temp_folder)
            sliced_name_list = [
                os.path.join(self.slicing_temp_folder, name)
                for name in sliced_name_list
                if (name.endswith(".wav"))
            ]
            shuffle(sliced_name_list)
            n_scenarios = int(np.floor(len(sliced_name_list) / (2 * self.n_segments)))

            c = 0
            self.noise_iter = 0
            shuffle(self.noise_pool)
            print("building scenarios...")
            for i in tqdm(range(n_scenarios)):

                curr_scenario_folder = os.path.join(
                    out_folder, out_name + str(i + repeat * n_scenarios)
                )
                if not os.path.exists(curr_scenario_folder):
                    os.mkdir(curr_scenario_folder)
                far_end_pick = sliced_name_list[c : c + self.n_segments]
                near_end_pick = sliced_name_list[
                    c + self.n_segments : c + 2 * self.n_segments
                ]
                c += 2 * self.n_segments

                if noise != "none":

                    assert (
                        len(self.noise_pool) > 0
                    ), "ERROR, no noise files have been defined, try using add_source_noise(...)"

                    # pick the start of the denoised sequence, length half of the sequence, ignored if defined SNR
                    denoise_start = int(
                        np.random.uniform(0, self.sequence_len * self.sr / 2 - 1)
                    )
                    # near end noise file
                    noise_seq_ne = self._make_noise_sequence()
                    # far end noise file
                    noise_seq_fe = self._make_noise_sequence()

                    if noise == "rand_SNR":
                        # near end denoise section -20dB
                        noise_seq_ne[
                            denoise_start : int(
                                denoise_start + self.sequence_len * self.sr / 2
                            )
                        ] *= 0.1
                        # far end denoise section -40dB
                        noise_seq_fe[
                            denoise_start : int(
                                denoise_start + self.sequence_len * self.sr / 2
                            )
                        ] *= 0.01  

                else:
                    # if noise is none pass silence signal to scenario generator
                    noise_seq_ne = np.zeros((self.sequence_len * self.sr,))
                    noise_seq_fe = np.zeros((self.sequence_len * self.sr,))

                if noise == "rand_SNR" or noise == "none":
                    scen_dict = self.scenario_generator.scenario_generator_on_names(
                        far_end_pick,
                        near_end_pick,
                        noise_seq_ne,
                        noise_seq_fe,
                        self.sequence_len * self.sr,
                    )
                else:
                    scen_dict = self.scenario_generator.scenario_generator_on_names(
                        far_end_pick,
                        near_end_pick,
                        noise_seq_ne,
                        noise_seq_fe,
                        self.sequence_len * self.sr,
                        noise_gain=-noise,
                    )

                for temp_file in far_end_pick:
                    os.remove(temp_file)
                for temp_file in near_end_pick:
                    os.remove(temp_file)

                bd.io.write_wav(
                    os.path.join(curr_scenario_folder, "rIn.wav"),
                    scen_dict["rin"],
                    self.sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(curr_scenario_folder, "ne_reverb_clean.wav"),
                    scen_dict["ne_reverb"],
                    self.sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(curr_scenario_folder, "echo.wav"),
                    scen_dict["echo"],
                    self.sr,
                    fmt=bd.T_INT16F,
                )
                bd.io.write_wav(
                    os.path.join(curr_scenario_folder, "sIn.wav"),
                    scen_dict["sin"],
                    self.sr,
                    fmt=bd.T_INT16F,
                )

                with open(os.path.join(curr_scenario_folder, "lem_conf.json"), "w") as fp:
                    json.dump(scen_dict["lem_conf"], fp, indent=4)



# %%
if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dataset", dest="dataset", type=str, help="Dataset folder name"
    )
    parser.add_argument(
        "--generate_data",
        dest="generate_data",
        action="store_true",
        help="Generate dataset",
    )
    parser.add_argument(
        "--generate_target",
        dest="generate_target",
        action="store_true",
        help="Generate target",
    )

    parser.add_argument(
        "--repeat_pool",
        dest="repeat_pool",
        type=int,
        help="Number of time the entire training speech pool is repeated",
        default=1,
    )
    args = vars(parser.parse_args())

    print("######################################")
    print("\nDATASET GENERATOR\n")
    print("Dataset Name: ", args["dataset"])
    print("Generate Data: ", args["generate_data"])
    print("Generate Target: ", args["generate_target"])
    print("Repeat Pool: ", args["repeat_pool"])
    print()
    print("######################################")

    SR = 16000

    # dataset folder
    root_folder = "/home/danielefoscarin/LOCAL/SSD/generated"
    out_dataset_folder = os.path.join(root_folder, args["dataset"])

    bin_folder = "/home/danielefoscarin/LOCAL/SSD/bin"

    # temp folders
    sox_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_sox"
    slicing_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_slice"
    tbmap_temp_folder = "/home/danielefoscarin/LOCAL/SSD/temp_tbmap"

    TRAIN_RATIO = 0.8
    SAMPLE_LEN_SEC = 20
    SAMPLE_LEN = SAMPLE_LEN_SEC * SR
    N_SEGMENTS = 5

    # %%
    # TRAINING SET GENERATION

    if args["generate_data"]:
        dataset_generator = Dataset_Generator(
            slicing_temp_folder, sox_temp_folder, n_segments=N_SEGMENTS
        )

        # NOISE SOURCES ---------------------------------------------
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/carinterior_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/cabin_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/suburb_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/railroad_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/fan_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/car_ART_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/soundsnap_background_sliced"
        )
        dataset_generator.add_source_noise(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/noise/UrbanSound8K/train_16bit"
        )
        dataset_generator.add_source_noise(
            "/home/disks/sdb/danielefoscarin/local_datasets/ESC-50/train"
        )
        dataset_generator.add_source_noise(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/rnnoise_data/noise_10s/train"
        )
        dataset_generator.add_source_noise(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/noise/soundsnap/office/sporadic_16bit"
        )

        # SPEECH sources ------------------------------------------------
        dataset_generator.add_source_single_clean(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/multilingual_podcast/German"
        )
        dataset_generator.add_source_single_clean(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/48Kdataset/train"
        )
        dataset_generator.add_source_single_clean(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/Yoruba/yo_ng_female"
        )
        dataset_generator.add_source_single_clean(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/Yoruba/yo_ng_male"
        )
        dataset_generator.add_source_single_clean(
            "/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/source/speech/valentini/clean_trainset_28spk_wav/clean_trainset_28spk_wav"
        )

        print(
            "\ntrain noise pool size: ", len(dataset_generator.train_noise_pool)
        )
        print(
            "train single speech pool size: ", len(dataset_generator.train_single_pool)
        )

        print("\n\ngenerating training set...")
        dataset_generator.generate_dataset(
            "train", out_dataset_folder, noise="rand_SNR", n_repeat_pool=args["repeat_pool"]
        )
        print("\n\ngenerating validation set...")
        dataset_generator.generate_dataset(
            "val", out_dataset_folder, noise="rand_SNR"
        )

    # TRAINING SET generate target

    if args["generate_target"]:
        print("\n\ngenerating target...")
        train_ts_target_list = []
        val_ts_target_list = []
        train_scenarios_list = [
            name for name in os.listdir(out_dataset_folder) if name.startswith("train")
        ]
        train_scenarios_list.sort()
        val_scenarios_list = [
            name for name in os.listdir(out_dataset_folder) if name.startswith("val")
        ]
        val_scenarios_list.sort()
        target_generator = Target_Generator(bin_folder, temp_folder=tbmap_temp_folder)

        for scenario in tqdm(train_scenarios_list):
            curr_target, curr_tbmap_ts = target_generator.get_target_talk_state(
                os.path.join(out_dataset_folder, scenario)
            )
            np.save(
                os.path.join(out_dataset_folder, scenario, "tbmap_ts"), curr_tbmap_ts
            )

        for scenario in tqdm(val_scenarios_list):
            curr_target, curr_tbmap_ts = target_generator.get_target_talk_state(
                os.path.join(out_dataset_folder, scenario)
            )
            np.save(
                os.path.join(out_dataset_folder, scenario, "tbmap_ts"), curr_tbmap_ts
            )

# %%
