# %% imports
import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10

import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
import warnings

# %%
# CASUAL PADDING CONVOLUTIONAL LAYER
# ----------------------------------
class CausalConv2D(layers.Layer):
    def __init__(
        self,
        filters,
        kernel_size,
        strides=1,
        dilation_rate=1,
        padding="same",
        activation="relu",
        relu_max_value=None,
        with_batch_norm=True,
        c_name=None
    ):

        super(CausalConv2D, self).__init__()
        self.filters = filters
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.activation = activation
        self.relu_max_value = relu_max_value
        self.with_batch_norm = with_batch_norm
        if c_name is not None:
            self._name=c_name

        assert self.padding in [
            "same",
            "valid",
            "causal_valid",
        ], "ERROR, supported padding types are 'same', 'valid', 'causal_valid'."

        # o = [i + 2*p - k - (k-1)*(d-1)]/s + 1
        # P = ((S-1)*W-S+F)/2, with F = filter size, S = stride
        # p = (out - in - stride + kernel)/2
        # psame = (kernel - stride)/2
        if self.padding == "same":
            padding = np.zeros((2, 2), dtype="int32")
            padding[0, 0] = int(self.kernel_size[0] * self.dilation_rate - self.strides)
            # padding[1,0] = padding[1,1] = int((self.kernel_size[1]*self.dilation_rate[1] - self.strides[1]) // 2)
            padding[1, 0] = padding[1, 1] = int(
                (self.kernel_size[1] * self.dilation_rate - self.strides) // 2
            )

            self.causal_padding = layers.ZeroPadding2D(
                padding=((padding[0, 0], padding[0, 1]), (padding[1, 0], padding[1, 1]))
            )
        elif self.padding == "valid":
            self.causal_padding = layers.ZeroPadding2D(padding=(0, 0))

        elif self.padding == "causal_valid":
            padding = np.zeros((2, 2), dtype="int32")
            padding[0, 0] = int(self.kernel_size[0] * self.dilation_rate - self.strides)
            # padding[1,0] = padding[1,1] = int((self.kernel_size[1]*self.dilation_rate[1] - self.strides[1]) // 2)
            self.causal_padding = layers.ZeroPadding2D(
                padding=((padding[0, 0], padding[0, 1]), (padding[1, 0], padding[1, 1]))
            )

        self.depthwise = layers.DepthwiseConv2D(
            kernel_size=self.kernel_size, strides=self.strides, padding="valid"
        )

        self.pointwise = layers.Conv1D(
            filters=self.filters,
            kernel_size=1,
            strides=self.strides,
        )

        self.batch_norm1 = layers.BatchNormalization()
        self.batch_norm2 = layers.BatchNormalization()
        self.act_layer1 = layers.ReLU(self.relu_max_value)

        assert self.activation in [
            "relu",
            "softmax",
        ], "ERROR: supported activation are 'relu', 'softmax'."
        if self.activation == "relu":
            self.act_layer2 = layers.ReLU(self.relu_max_value)
        elif self.activation == "softmax":
            self.act_layer2 = layers.Softmax()

    def call(self, input):
        out = self.causal_padding(input)
        out = self.depthwise(out)
        out = self.batch_norm1(out)
        out = self.act_layer1(out)

        out = self.pointwise(out)
        out = self.batch_norm2(out)
        out = self.act_layer2(out)
        return out

    def get_config(self):
        cfg = super().get_config()
        return cfg

    def get_sub_layers(self):
        return [
            [self.depthwise, self.batch_norm1, self.act_layer1],
            [self.pointwise, self.batch_norm2, self.act_layer2],
        ]


class CausalBottleneck(layers.Layer):
    def __init__(
        self,
        expansion,
        projection,
        kernel_size,
        strides=(1, 1),
        dilation_rate=(1, 1),
        padding="same",
        activation="relu",
        relu_max_value=None,
        end_activation=False,  # False according to MobileNet v2 paper
        residual_connection=False,
        c_name=None
    ):
        super(CausalBottleneck, self).__init__()
        self.expansion = expansion
        self.projection = projection
        self.kernel_size = kernel_size
        self.strides = strides
        self.dilation_rate = dilation_rate
        self.padding = padding
        self.activation = activation
        self.relu_max_value = relu_max_value
        self.end_activation = end_activation
        self.residual_connection = residual_connection
        # experimental
        if c_name is not None:
            self._name=c_name

        assert self.padding in [
            "same",
            "valid",
            "causal_valid",
        ], "ERROR, supported padding types are 'same', 'valid', 'causal_valid'."

        self.exp_layer = layers.Conv1D(filters=self.expansion, kernel_size=1)

        # projection pointwise conv
        self.proj_layer = layers.Conv1D(filters=self.projection, kernel_size=1)

        # depthwise conv2D, need special padding before
        self.depth_conv = layers.DepthwiseConv2D(
            kernel_size=self.kernel_size,
            strides=self.strides,
            padding="valid",
            dilation_rate=self.dilation_rate,
        )

        if self.padding == "same":
            padding = np.zeros((2, 2), dtype="int32")
            padding[0, 0] = int(
                self.kernel_size[0] * self.dilation_rate[0] - self.strides[0]
            )
            padding[1, 0] = padding[1, 1] = int(
                (self.kernel_size[1] * self.dilation_rate[1] - self.strides[1]) // 2
            )
            self.causal_padding = layers.ZeroPadding2D(
                padding=((padding[0, 0], padding[0, 1]), (padding[1, 0], padding[1, 1]))
            )
        elif self.padding == "valid":
            self.causal_padding = layers.ZeroPadding2D(padding=(0, 0))

        elif self.padding == "causal_valid":
            padding = np.zeros((2, 2), dtype="int32")
            padding[0, 0] = int(
                self.kernel_size[0] * self.dilation_rate[0] - self.strides[0]
            )
            # padding[1,0] = padding[1,1] = int((self.kernel_size[1]*self.dilation_rate[1] - self.strides[1]) // 2)
            self.causal_padding = layers.ZeroPadding2D(
                padding=((padding[0, 0], padding[0, 1]), (padding[1, 0], padding[1, 1]))
            )

        self.batch_norm1 = layers.BatchNormalization()
        self.batch_norm2 = layers.BatchNormalization()
        self.batch_norm3 = layers.BatchNormalization()

        self.act_layer1 = layers.ReLU(self.relu_max_value)
        self.act_layer2 = layers.ReLU(self.relu_max_value)

        assert self.activation in [
            "relu",
            "softmax",
        ], "ERROR: supported activation are 'relu', 'softmax'."
        if self.activation == "relu":
            self.act_layer3 = layers.ReLU(self.relu_max_value)
        elif self.activation == "softmax":
            self.act_layer3 = layers.Softmax()

        self.res_add = layers.Add()

    def call(self, input):

        # expansion
        out = self.exp_layer(input)
        out = self.batch_norm1(out)
        out = self.act_layer1(out)

        # depth conv
        out = self.causal_padding(out)
        out = self.depth_conv(out)
        out = self.batch_norm2(out)
        out = self.act_layer2(out)

        # projection
        out = self.proj_layer(out)
        out = self.batch_norm3(out)
        if self.end_activation:
            out = self.act_layer3(out)

        if self.residual_connection:
            out = self.res_add([out, input])

        return out

    # debug call
    def debug_call(self, input):
        # expansion
        out = self.exp_layer(input)
        out = self.batch_norm1(out)
        out = self.act_layer1(out)
        out_exp = out
        tf.io.write_file

        # depth conv
        out = self.causal_padding(out)
        out = self.depth_conv(out)
        out = self.batch_norm2(out)
        out = self.act_layer2(out)
        out_depth = out
        np.save('out_depth.npy', out_depth)

        # projection
        out = self.proj_layer(out)
        out = self.batch_norm3(out)
        out_proj = out
        np.save('out_proj.npy', out_proj)

        if self.end_activation:
            out = self.act_layer3(out)

        if self.residual_connection:
            out = self.res_add([out, input])
        
        return out

    def get_config(self):
        cfg = super().get_config()
        return cfg

    def get_sub_layers(self):
        return [
            [self.exp_layer, self.batch_norm1, self.act_layer1],
            [self.depth_conv, self.batch_norm2, self.act_layer2],
            [self.proj_layer, self.batch_norm3, None],
        ]


# %%
if __name__ == "__main__":
    print("Custom NN layers")
# %%
