# %% 
# libraries
import bd_rd as bd
from tensorflow.python.keras.optimizer_v2.rmsprop import RMSProp
bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks import STFT
from bd_rd.processing.common import get_bd_window

from scenario_generator import *  
# from dataset_generator import Dataset_Generator
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import keras.backend as bk


import numpy as np
from matplotlib import pyplot as plt
import os
from icecream import ic
import keras_flops
from tqdm import tqdm
import io
import joblib
import datetime

import seaborn as sn
import pandas as pd
from sklearn.metrics import confusion_matrix

import tensorflow as tf
from tensorflow.keras import layers

import optuna
# from tensorflow import keras

SR = 16000

bin_folder = '/home/danielefoscarin/LOCAL/SSD/bin'

# out_dataset_folder = '/home/danielefoscarin/LOCAL/SSD/generated/dataset_0809'
out_dataset_folder = '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_24_SNR'

sox_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_sox'
slicing_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_slice'
tbmap_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_tbmap'

SAMPLE_LEN_SEC = 20
SAMPLE_LEN = SAMPLE_LEN_SEC * SR

tf.random.set_seed(1)


train_ts_target_list = []
val_ts_target_list = []
train_scenarios_list = [name for name in os.listdir(out_dataset_folder) if name.startswith('train')]
train_scenarios_list.sort()
val_scenarios_list = [name for name in os.listdir(out_dataset_folder) if name.startswith('val')]
val_scenarios_list.sort()



train_list = [os.path.join(out_dataset_folder, name) for name in train_scenarios_list]
val_list = [os.path.join(out_dataset_folder, name) for name in val_scenarios_list]

# %%
def debug_feat():
    # CHECK FEATURES
    auto = tf.data.AUTOTUNE
    # val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='full_linear', n_bands=72, augmentation=True)
    test_scenarios_list = [name for name in os.listdir(out_dataset_folder) if name.startswith('test')]
    train_list = [os.path.join(out_dataset_folder, name) for name in train_scenarios_list]
    val_list = [os.path.join(out_dataset_folder, name) for name in test_scenarios_list]

    train_dataset = tf.data.Dataset.from_tensor_slices(train_list)
    val_dataset = tf.data.Dataset.from_tensor_slices(val_list)

    linear_bands = np.linspace(150, 8000, 48, endpoint=False)
    train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=linear_bands,  augmentation=True)
    val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=linear_bands,  augmentation=False)

    # try prefetch and interleave
    # train_dataset = train_dataset.prefetch(auto)
    # train_dataset = train_dataset.map(train_preprocessor.tf_get_features_from_bd,
    #                             num_parallel_calls=auto)

    val_dataset = val_dataset.prefetch(auto)
    val_dataset = val_dataset.map(val_preprocessor.tf_get_features_from_bd,
                                num_parallel_calls=auto)

    batch_size = 1
    train_dataset = train_dataset.batch(batch_size)
    val_dataset = val_dataset.batch(batch_size)


    val_dataset = val_dataset.shuffle(3)
    feat, target = next(iter(val_dataset))
    # feat, target = next(iter(val_dataset))
    feat = feat.numpy()[0]
    target = target.numpy()
    ic(target.shape)
    print(feat.shape)
    
    plt.figure(figsize=(12,4))
    plt.subplot(1,3,1)
    # bd.plots.plot_spectrogram(feat[:,:,0], new_figure=False, show=False)
    plt.imshow(feat[:,:,0].T, aspect='auto', interpolation='nearest')
    plt.title('aec out')
    plt.colorbar()
    plt.xlabel('Frame')
    plt.ylabel('Band')
    plt.subplot(1,3,2,)
    # bd.plots.plot_spectrogram(feat[:,:,1], new_figure=False, show=False)
    plt.imshow(feat[:,:,1].T, aspect='auto', interpolation='nearest')
    plt.title('echo replica')
    plt.colorbar()
    plt.xlabel('Frame')
    plt.ylabel('Band')
    plt.subplot(1,3,3,)
    # bd.plots.plot_spectrogram(feat[:,:,2], new_figure=False, show=False)
    plt.imshow(feat[:,:,2].T, aspect='auto', interpolation='nearest')
    plt.title('log_incoherence')
    plt.colorbar()
    plt.xlabel('Frame')
    plt.ylabel('Band')
    plt.tight_layout()
    plt.savefig('features.pdf')
    plt.show()

    plt.figure()
    plt.plot(target[0])
    plt.xlabel('Frames')
    plt.ylabel('TS')
    plt.yticks([0,1,2,3])
    plt.title('Talk State target')
    plt.grid(linestyle=":")
    plt.savefig('target.pdf')
    plt.show()

debug_feat()

# %%
# check pipeline and timeit

auto = tf.data.AUTOTUNE
speech_pdf_bands = np.linspace(150, 8000, 48, endpoint=False)

train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=speech_pdf_bands, target_type='dv_ts', augmentation=True)
val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=speech_pdf_bands, target_type='dv_ts', augmentation=False)

train_dataset = tf.data.Dataset.from_tensor_slices(train_list)
val_dataset = tf.data.Dataset.from_tensor_slices(val_list)

# try prefetch and interleave
train_dataset = train_dataset.prefetch(auto)
train_dataset = train_dataset.map(train_preprocessor.tf_get_features_from_bd,
                             num_parallel_calls=auto)

val_dataset = val_dataset.prefetch(auto)
val_dataset = val_dataset.map(val_preprocessor.tf_get_features_from_bd,
                             num_parallel_calls=auto)

import timeit

td = tf.convert_to_tensor(train_list[0])
num = 10
t = timeit.timeit(lambda: train_preprocessor.tf_get_features_from_bd(td), number=num)
print(f'Avg execution time {t/num} s in {num} iterations')


# %% 
# COMPARISON WITH TBMAP TALK STATE DETECTOR
#------------------------------------------
def to_onehot(x, n_class=4):
    onehot = np.zeros((n_class,1))
    onehot[int(x)] = 1
    return onehot 

tbmap_ts_list = []
target_list = []
correct_c = 0
frame_c = 0
target_count_class = []
tbmap_count_class = []
for i, scenario in enumerate(val_scenarios_list):
    curr_tbmap_ts = np.load(os.path.join(out_dataset_folder, scenario, 'tbmap_ts.npy'))
    tbmap_ts_list.append(curr_tbmap_ts)
    # print(np.mean(curr_tbmap_ts))
    #remember to ignore the first value of talk_state_npy since its the delay value
    curr_target = np.load(os.path.join(out_dataset_folder, scenario, 'talk_state.npy'))[1:]
    target_list.append(curr_target)
    frame_c += curr_target.shape[0]
    for i in range(curr_target.shape[0]):
        target_count_class.append(to_onehot(curr_target[i]))
        tbmap_count_class.append(to_onehot(curr_tbmap_ts[i]))
        if curr_tbmap_ts[i] == curr_target[i]:
            correct_c += 1  



target_count_class = np.array(target_count_class)
target_count_class = np.sum(target_count_class, axis=0)/frame_c


tbmap_count_class = np.array(tbmap_count_class)
tbmap_count_class = np.sum(tbmap_count_class, axis=0)/frame_c

print(f'Accuracy of the POST_FILTER_talk_state: {correct_c/frame_c}')

# figure
plt.figure(figsize=(12,8))
plt.subplot(2,1,1)
plt.title('Target')
plt.plot(target_list[22])
plt.grid(linestyle=':')

plt.subplot(2,1,2)
plt.title('S2C prediction')
plt.plot(tbmap_ts_list[22])
plt.grid(linestyle=':')
plt.show()

plt.figure()
plt.subplot(2,1,2)
plt.title('Tbmap classes distribution')
plt.bar(range(4), tbmap_count_class[:,0])
plt.grid(linestyle=':')
plt.xticks(range(0,4))

plt.subplot(2,1,1)
plt.title('Target classes distribution')
plt.bar(range(4), target_count_class[:,0])
plt.xticks(range(0,4))
# plt.plot(target_count_class[:,0])
plt.grid(linestyle=':')
plt.show()




# %%



# %%
# TS space NET
# ----------------------------------------------
class Misclassification():

    def __init__(self):
        self.correct = 0
        self.leak = 1
        self.hd_loss = 1
        self.fd_loss = 0.2
        self.over = 0.01
        self.noise = 0.01
        self.ok = 0.01
        self.num_classes = 4
        self.cat_loss = tf.keras.losses.CategoricalCrossentropy()
        self.w_loss_matrix = np.array([[self.correct, self.hd_loss, self.ok, self.fd_loss],
                                    [self.noise, self.correct, self.leak, self.leak],
                                    [self.ok, self.hd_loss, self.correct, self.fd_loss],
                                    [self.over, self.over, self.leak, self.correct]])

        self.w_loss_matrix = tf.convert_to_tensor(self.w_loss_matrix.T, dtype=tf.float32)

    def weighted_categorical_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)        
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted categorical cross entropy
        loss = -tf.math.reduce_sum( (1-w_loss) * target * tf.math.log(pred+1e-9))/(target.shape[1])
        return loss

    def weighted_binary_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)        
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted binary cross entropy
        loss = -tf.math.reduce_sum( (1-w_loss) * target * tf.math.log(pred+1e-9) + w_loss * (1-target) * tf.math.log(1-pred+1e-9))/(2*target.shape[1])
        return loss

def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer='Adam'):

    misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands/8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2**(-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input


    x = CausalBottleneck(int(expansion*in_f), int(in_f), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)
    # x = CausalConv2D(filters=int(4*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX)(x)
    x = CausalBottleneck(int(expansion*in_f), np.max([int(4*wm), in_f]), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(x)
    # x = CausalBottleneck(int(expansion*in_f), int(4*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(x)

    x = layers.MaxPool2D((1,2))(x)

    x = CausalBottleneck(int(expansion*4*wm), np.max([int(4*wm), in_f]), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)
    # x = CausalBottleneck(int(expansion*4*wm), int(4*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)

    # x = CausalConv2D(filters=int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX)(x)
    x = CausalBottleneck(int(expansion*in_f), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(x)
    x = layers.MaxPool2D((1,2))(x)

    x = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)
    # x = CausalBottleneck(32, 8, kernel_size=(eye_depth,3), relu_max_value=MAX, residual_connection=True)(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    # ne = CausalConv2D(filters=int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX)(ne)
    ne = CausalBottleneck(int(expansion*8*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(ne)
    ne = layers.MaxPool2D((1,2))(ne)


    # ne = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    # ne = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    ne = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    ne = CausalConv2D(filters=int(32*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid')(ne)
    # ne = CausalBottleneck(int(expansion*16*wm), int(32*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid', relu_max_value=MAX, residual_connection=False)(ne)
    ne = tf.squeeze(ne, axis=2)
    
    ne = layers.Conv1D(int(32*wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(1, 1, activation='sigmoid',)(ne)
    ne = layers.Layer(name='NE')(ne)

    # FAR END branch
    fe = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(fe)
    # fe = CausalConv2D(filters=int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX)(fe)
    fe = CausalBottleneck(int(expansion*8*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(fe)
    fe = layers.MaxPool2D((1,2))(fe)


    fe = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(fe)
    fe = CausalConv2D(filters=int(16*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid')(fe)
    # fe = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid', relu_max_value=MAX, residual_connection=False)(fe)
    fe = tf.squeeze(fe, axis=2)
    
    fe = layers.Conv1D(int(16*wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation='sigmoid')(fe)
    fe = layers.Layer(name='FE')(fe)


    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    # ts = tf.concat([ne_feat, ne, tf.square(ne),fe_feat, fe, tf.square(fe)], axis=2, name='ts concat')
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(4, 1,)(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name='TS')(ts)


    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    if optimizer == 'Adam':
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), 
        loss = {"NE": tf.keras.losses.binary_crossentropy,
                    "FE": tf.keras.losses.binary_crossentropy,
                    # "TS": tf.keras.losses.sparse_categorical_crossentropy,
                    "TS": misclass.weighted_binary_cross_entropy
                    }, 
        metrics=['accuracy'])
    elif optimizer == 'RMSProp':
        model.compile(optimizer=tf.keras.optimizers.RMSProp(learning_rate=0.001), 
        loss = {"NE": tf.keras.losses.binary_crossentropy,
                    "FE": tf.keras.losses.binary_crossentropy,
                    # "TS": tf.keras.losses.sparse_categorical_crossentropy,
                    "TS": misclass.weighted_binary_cross_entropy
                    }, 
        metrics=['accuracy'])


    return model

model =  make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
model.summary()
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)/2000))
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)))




# %%
# create dataset 


# speech_pdf_bands = np.concatenate((np.array([150, 300, 500, 800, 1000]), np.linspace(1200, 8000, 43, endpoint=False)))
speech_pdf_bands = np.linspace(150, 8000, 48, endpoint=False)


auto = tf.data.AUTOTUNE
# augmentation on train_preprocessor not on val
# train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, target_type='dv_ts', augmentation=True)
# val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, target_type='dv_ts', augmentation=False)

# train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='full_linear', n_bands=48, target_type='dv_ts', augmentation=True)
# val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='full_linear', n_bands=48, target_type='dv_ts', augmentation=False)

train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=speech_pdf_bands, target_type='dv_ts', augmentation=True)
val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=speech_pdf_bands, target_type='dv_ts', augmentation=False)

train_list = [os.path.join(out_dataset_folder, name) for name in train_scenarios_list]
val_list = [os.path.join(out_dataset_folder, name) for name in val_scenarios_list]

train_dataset = tf.data.Dataset.from_tensor_slices(train_list)
val_dataset = tf.data.Dataset.from_tensor_slices(val_list)

# try prefetch and interleave
train_dataset = train_dataset.prefetch(auto)
train_dataset = train_dataset.map(train_preprocessor.tf_get_features_from_bd,
                             num_parallel_calls=auto)

val_dataset = val_dataset.prefetch(auto)
val_dataset = val_dataset.map(val_preprocessor.tf_get_features_from_bd,
                             num_parallel_calls=auto)

batch_size = 1
train_dataset = train_dataset.batch(batch_size)
val_dataset = val_dataset.batch(batch_size)

# %%
# ts space net TRAINING
# -------------------------------------
# /home/danielefoscarin/LOCAL/HDD/log_tensorboard/ts_space/
current_time = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
log_dir = '/home/danielefoscarin/LOCAL/HDD/log_tensorboard/ts_space/dataset_20_08/'+current_time+'/'
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
checkpoint_filepath = '/home/danielefoscarin/LOCAL/HDD/model_checkpoint/ts_space/best_48lin_23_08/'
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath+'best_model',
    save_weights_only=True,
    monitor='val_TS_loss',
    mode='min',
    save_best_only=True)

reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_TS_loss', factor=0.05,
                              patience=1, min_lr=0.0001)

# model = make_tsMNet()
model = make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
model.summary()
flops = keras_flops.get_flops(model, batch_size=1)
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)/2000))

# %%

epochs = 15
history = model.fit(train_dataset, 
    batch_size=batch_size,
    epochs=epochs, 
    validation_data=val_dataset,
    callbacks=[tensorboard_callback, 
        model_checkpoint_callback,
        reduce_lr],
    )
# %%
# Load pretrained model
checkpoint_filepath = '/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/lin_20210908-181650/'
model.load_weights(checkpoint_filepath+'best_model')

# %%
smooth_frame = 5
pred_l = []
vad_ne_l = []
vad_fe_l = []
pred_l_smoothed = []
ts_vad_l = []
target_l = []
target_vad_ne_l = []
target_vad_fe_l = []
for feat, target in tqdm(val_dataset): 
    pred = model(feat)
    vad_ne = np.squeeze(pred[0].numpy())
    vad_ne[vad_ne >= 0.4] = 1
    vad_ne[vad_ne < 0.4] = 0
    vad_ne_l.append(vad_ne)

    vad_fe = np.squeeze(pred[1].numpy())
    vad_fe[vad_fe >= 0.4] = 1
    vad_fe[vad_fe < 0.4] = 0
    vad_fe_l.append(vad_fe)

    ts_vad = vad_ne + 2*vad_fe
    ts_vad_l.append(ts_vad)

    pred = pred[2].numpy()
    pred = np.squeeze(pred, 0)
    pred_sel = np.zeros((pred.shape[0], 1))
    pred_sel = np.argmax(pred, axis=1)
    pred_sel = np.expand_dims(pred_sel, axis=-1)
    # ic(pred_sel.shape)

    ne = np.zeros(pred_sel.shape)
    fe = np.zeros(pred_sel.shape)
    ne[pred_sel%2==1] = 1
    fe[pred_sel>1] = 1

    ne_smoothed = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(smooth_frame))
    ne_smoothed = np.round(ne_smoothed, 0)
    fe_smoothed = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(smooth_frame))
    fe_smoothed = np.round(fe_smoothed, 0)

    pred_sel_smoothed = ne_smoothed + 2*fe_smoothed
    # pred_sel_smoothed = np.round(pred_sel_smoothed, 0)

    pred_l.append(pred_sel)
    pred_l_smoothed.append(pred_sel_smoothed)
    target_vad_ne = np.squeeze(target['NE'].numpy())
    target_vad_fe = np.squeeze(target['FE'].numpy())
    target = target['TS'].numpy()
    target = np.squeeze(target, (0,-1))
    target_l.append(target)
    target_vad_ne_l.append(target_vad_ne)
    target_vad_fe_l.append(target_vad_fe)
# %%
#remove this, use analysis.plot_confusion_matrix
def plot_confusion_matrix(target, pred, title):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = confusion_matrix(target, pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm)/4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r')
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

# %%
plt.figure(figsize=(10,12))
plt.subplot(3,1,1)
plt.plot(pred_l[0])
plt.title('predicted')
plt.subplot(3,1,2)
plt.plot(pred_l_smoothed[0])
plt.title('predicted smoothed')
plt.subplot(3,1,3)
plt.plot(target_l[0])
plt.title('target')
plt.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(ts_vad_l), "TS from 2 VADs")
fig.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l), "TS_space_net")
fig.show()

# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l_smoothed), "SMOOTHED TS_space_net")
# fig.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(tbmap_ts_list), "Baseline algorithm")
fig.show()






# %% ------------------------------------------------------------------------
# run TS_optuna_search 
# ----------------------------------------------------------------------------


# %% OPTUNA ANLAYSIS
best_params =  {'bands': 48, 'wm': 0.65, 'expansion': 2, 'kernel_depth': 7}
model = make_tsMNet(input_bands=48, wm=0.65, expansion=2, kernel_depth=7)
model.summary()

model.load_weights('/home/danielefoscarin/LOCAL/HDD/model_checkpoint/optuna/40/best_model')
# # %%
flops = keras_flops.get_flops(model, batch_size=1)
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)/2000))



# %%
study = joblib.load('/home/danielefoscarin/LOCAL/HDD/optuna_study/study_11_08.pkl')
print(study.best_trial)


# %%
fig = optuna.visualization.plot_optimization_history(study)
fig.show()

fig = optuna.visualization.plot_param_importances(study)
fig.show()

fig = optuna.visualization.plot_slice(study)
fig.show()

fig = optuna.visualization.plot_contour(study, params=['wm', 'bands'])
fig.show()

fig = optuna.visualization.plot_parallel_coordinate(study)
fig.show()
# %%
# study_summary = optuna.study.StudySummary(study)
print(study.trials[33])

# %%
auto = tf.data.AUTOTUNE
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
train_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, target_type='dv_ts', augmentation=True)
val_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, target_type='dv_ts', augmentation=False)

train_list = [os.path.join(out_dataset_folder, name) for name in train_scenarios_list]
val_list = [os.path.join(out_dataset_folder, name) for name in val_scenarios_list]

train_dataset = tf.data.Dataset.from_tensor_slices(train_list)
val_dataset = tf.data.Dataset.from_tensor_slices(val_list)

train_dataset = train_dataset.prefetch(auto)
train_dataset = train_dataset.map(train_preprocessor.tf_get_features_from_bd,
                            num_parallel_calls=auto)

val_dataset = val_dataset.prefetch(auto)
val_dataset = val_dataset.map(val_preprocessor.tf_get_features_from_bd,
                            num_parallel_calls=auto)

batch_size = 1
train_dataset = train_dataset.batch(batch_size)
val_dataset = val_dataset.batch(batch_size)

# %%
smooth_frame = 5
pred_l = []
pred_l_smoothed = []
target_l = []
for feat, target in tqdm(val_dataset): 
    pred = model(feat)
    pred = pred[2].numpy()
    pred = np.squeeze(pred, 0)
    pred_sel = np.zeros((pred.shape[0], 1))
    pred_sel = np.argmax(pred, axis=1)
    pred_sel = np.expand_dims(pred_sel, axis=-1)
    # ic(pred_sel.shape)

    ne = np.zeros(pred_sel.shape)
    fe = np.zeros(pred_sel.shape)
    ne[pred_sel%2==1] = 1
    fe[pred_sel>1] = 1

    ne_smoothed = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(smooth_frame))
    ne_smoothed = np.round(ne_smoothed, 0)
    fe_smoothed = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(smooth_frame))
    fe_smoothed = np.round(fe_smoothed, 0)

    pred_sel_smoothed = ne_smoothed + 2*fe_smoothed
    # pred_sel_smoothed = np.round(pred_sel_smoothed, 0)

    pred_l.append(pred_sel)
    pred_l_smoothed.append(pred_sel_smoothed)
    target = target['TS'].numpy()
    target = np.squeeze(target, (0,-1))
    target_l.append(target)
# %%
#remove this, use analysis.plot_confusion_matrix
def plot_confusion_matrix(target, pred, title):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = confusion_matrix(target, pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm)/4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r')
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

# %%
p = 10
plt.figure(figsize=(10,12))
plt.subplot(3,1,1)
plt.plot(pred_l[p])
plt.title('predicted')
plt.subplot(3,1,2)
plt.plot(pred_l_smoothed[p])
plt.title('predicted smoothed')
plt.subplot(3,1,3)
plt.plot(target_l[p])
plt.title('target')
plt.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l), "TS_space_net")
fig.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l_smoothed), "SMOOTHED TS_space_net")
fig.show()

fig2 = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(tbmap_ts_list), "Santilli")
fig2.show()
# %%



plt.figure()
plt.plot(np.linspace(0.0001, 1, 1000), 5*np.log(np.linspace(0.0001,1,1000)/0.4))
# plt.plot(np.linspace(0.0001, 1, 1000), np.diff(np.log(np.linspace(0.0001,1,1000)/0.4), append=0))
print(np.log(0.2/0.4)*1e06/3e05)
# plt.ylim([-5,2])
# plt.xlim([0,0.1])
plt.grid()
plt.show()


# %% TEST INFERENCE
# load models
model_lin = make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
model_lin.load_weights('/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/lin_20210831-171132/'+'best_model')
model_bark = make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
model_bark.load_weights('/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/bark_20210831-171325/'+'best_model')

# %%
# functions

config = bd.utils.Class_Dict(sr=SR, framesize=int(0.01*SR), zero_pad_end=False,
                    pre_emphasis=True,
                    pre_gain=1, window="bd", ola_ratio=4, max_freq=20000,
                    stft_gain=1, value_type=bd.T_INT16F, dc_removal=True)
stft_processor = STFT(config=config)
stft_processor.b_wola.out_idx = 1

def apply_ts(aec_out, ts):
    aec_out[ts==0.0] *= 0.01
    aec_out[ts==1.0] *= 1
    aec_out[ts==2.0] *= 0.01
    aec_out[ts==3.0] *= 1
    return aec_out

def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i,:] *= ts_red[i]
    return aec_out

def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    # ne[pred_sel%2==1] = 1
    # fe[pred_sel>1] = 1
    ne[ts%2==1] = 1
    fe[ts>1] = 1
    return ne, fe

def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2*fe
    return ts_out

def todB(x):
    return 20*np.log10(x)

def tolin(x):
    return 10**(x/20)

def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []
        
        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)
        
        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)
        
    return ts_reduction


# asd = np.array([0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,3,3,3,3,2,2,2,2,0,0,0,0,0,2,2,2,2,2,2,2,0,0,0,0,0,1,1,1,1,3,3,3,3,3,3,3,2,2,2,2,1,1,1,1,1,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0])

# %%
# preprocessing for inference

sin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/sin.wav'
rin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/rin.wav'
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
inference_preprocessor_lin = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, augmentation=False)
inference_preprocessor_bark = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, augmentation=False)

print('a')
feat_lin, aec_out = inference_preprocessor_lin.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder, mul=tolin(20))
feat_lin = tf.expand_dims(feat_lin, axis=0)
print('b')
feat_bark, aec_out = inference_preprocessor_bark.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder, mul=tolin(20))
feat_bark = tf.expand_dims(feat_bark, axis=0)
print('end')

# print(feat.shape)
# %%
def model_inference(model, feat):
    output = model(feat)
    ne_vad = np.squeeze(output[0].numpy())
    fe_vad = np.squeeze(output[1].numpy())
    ts_class = np.squeeze(output[2].numpy())
    ts = np.zeros((ts_class.shape[0], ))
    for i in range(ts.shape[0]):
        ts[i] = np.argmax(ts_class[i])
    return ne_vad, fe_vad, ts
# ic(ne_vad.shape, fe_vad.shape, ts.shape)


# %%
ne_lin, fe_lin, ts_lin = model_inference(model_lin, feat_lin)
times=np.arange(ts_lin.shape[0])/100

cols=np.concatenate([times[:,None],ne_lin[:,None],fe_lin[:,None],ts_lin[:,None]], axis=1)
np.savetxt('lin.csv', cols, fmt='%.2f', delimiter=',')

zoom = [0,times[-1]]
plt.figure()
plt.plot(np.arange(ts_lin.shape[0])/100, ne_lin)
plt.xlim(zoom)
# plt.xlim([50,70])
plt.grid()
plt.show()

plt.figure()
plt.plot(np.arange(ts_lin.shape[0])/100, fe_lin)
plt.xlim(zoom)
plt.grid()
plt.show()

plt.figure()
plt.plot(np.arange(ts_lin.shape[0])/100, ts_lin)
plt.xlim(zoom)
plt.grid()
plt.show()

# %%
ne_bark, fe_bark, ts_bark = model_inference(model_bark, feat_bark)

zoom = [200,250]
plt.figure()
plt.plot(np.arange(ts_bark.shape[0])/100, ne_bark)
plt.xlim(zoom)
plt.grid()
plt.show()

plt.figure()
plt.plot(np.arange(ts_bark.shape[0])/100, fe_bark)
plt.xlim(zoom)
plt.grid()
plt.show()

plt.figure()
plt.plot(np.arange(ts_bark.shape[0])/100, ts_bark)
plt.xlim(zoom)
plt.grid()
plt.show()

# %%
# post_aec_stft = apply_ts(aec_out.numpy(), ts_lin)
post_aec_stft = apply_ts_reduction(aec_out.numpy(), ts_conditional_smoothing(ts_lin, slow_len=6))

post_aec = stft_processor.invert_np(post_aec_stft)
aec_out_audio = stft_processor.invert_np(aec_out)
bd.io.write_wav("post_aec2.wav", post_aec, SR) 
bd.io.write_wav("aec_out.wav", aec_out_audio, SR) 

# %%
temp_folder = '/home/disks/ssdata/danielefoscarin/TSD_thesis/thesis_foscarin/temp'
data_folder = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386'
tb_map_subprocess(bin_folder, data_folder, os.path.join(bin_folder,'tb_map_21_07'),
                        os.path.join(data_folder, 'sin.wav'),
                        # os.path.join(data_folder, rin_name),
                        os.path.join(data_folder, 'rin.wav'),    
                        os.path.join(temp_folder, 'sOut.wav'),
                        os.path.join(bin_folder, 'ugo.conf'),
                        verbose=False, path_mode='full')





# %%
# ne_hd = bd.utils.HARD_SIGMOID(ne_vad, 0.01, 0.05)
# fe_hd = bd.utils.HARD_SIGMOID(fe_vad, 0.01, 0.05)

# plt.figure()
# plt.plot(ne_hd)
# plt.show()

# plt.figure()
# plt.plot(fe_hd)
# plt.show()

# plt.figure()
# plt.plot(np.arange(ts.shape[0])/160, np.ceil(ne_hd + 2*fe_hd))
# # plt.xlim([150,170])
# plt.grid
# plt.show()
