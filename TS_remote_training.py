# %%
# libraries
import bd_rd as bd
from tensorflow.python.keras.optimizer_v2.rmsprop import RMSProp

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/disks/ssdata/danielefoscarin/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks import STFT
from bd_rd.processing.common import get_bd_window

from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import keras.backend as bk


import numpy as np
import os
import sys
import argparse
from tqdm import tqdm
from tqdm.keras import TqdmCallback
import io
import joblib
import datetime
import json

import tensorflow as tf
from tensorflow.keras import layers
# TODO make a json on the model folder that records all the used configurations

SR = 16000
SAMPLE_LEN_SEC = 20
SAMPLE_LEN = SAMPLE_LEN_SEC * SR

tf.random.set_seed(1)
np.random.seed(1)

# MANAGE input parameters
parser = argparse.ArgumentParser()
parser.add_argument("--dataset", dest="dataset", type=str, help="Dataset folder name")
parser.add_argument(
    "--bands",
    dest="bands",
    type=str,
    choices=["linear", "bark"],
    help="Bands type, default linear",
)
parser.add_argument(
    "--epochs",
    dest="epochs",
    type=int,
    help="Number of epochs",
)
args = vars(parser.parse_args())

if args.get("epochs") is None:
    args["epochs"] = 50

if args.get("bands") is None:
    args["bands"] = "linear"

assert args["bands"] in [
    "linear",
    "bark",
], "Unsupported argument, must be 'linear' or 'bark'."
if args["bands"] == "bark":
    BARK_BANDS = True
else:
    BARK_BANDS = False

root_folder = "/home/danielefoscarin/LOCAL/SSD/generated"
out_dataset_folder = os.path.join(root_folder, args["dataset"])
print("\nTraining on ", out_dataset_folder, "...\n\n")


train_ts_target_list = []
val_ts_target_list = []
train_scenarios_list = [
    name for name in os.listdir(out_dataset_folder) if name.startswith("train")
]
train_scenarios_list.sort()
val_scenarios_list = [
    name for name in os.listdir(out_dataset_folder) if name.startswith("val")
]
val_scenarios_list.sort()


train_list = [os.path.join(out_dataset_folder, name) for name in train_scenarios_list]
val_list = [os.path.join(out_dataset_folder, name) for name in val_scenarios_list]


# %%
# TS space NET
# ----------------------------------------------
class Misclassification:
    def __init__(self):
        self.correct = 0
        self.leak = 1
        self.hd_loss = 1
        self.dt_leak = 0.2
        self.fd_loss = 0.2
        self.over = 0.01
        self.noise = 0.01
        self.ok = 0.01
        
        # experimental
        self.correct = 0
        self.leak = 1
        self.hd_loss = 0.5
        self.dt_leak = 0.1
        self.fd_loss = 0.1
        self.over = 0.001
        self.noise = 0.05
        self.ok = 0.001
        # ----------------
        self.num_classes = 4
        self.cat_loss = tf.keras.losses.CategoricalCrossentropy()
        self.w_loss_matrix = np.array(
            [
                [self.correct, self.hd_loss, self.ok, self.fd_loss],
                [self.noise, self.correct, self.leak, self.dt_leak],
                [self.ok, self.hd_loss, self.correct, self.fd_loss],
                [self.over, self.over, self.leak, self.correct],
            ]
        )

        self.w_loss_matrix = tf.convert_to_tensor(
            self.w_loss_matrix.T, dtype=tf.float32
        )

    def weighted_categorical_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted categorical cross entropy
        loss = -tf.math.reduce_sum((1 - w_loss) * target * tf.math.log(pred + 1e-9)) / (
            target.shape[1]
        )
        return loss

    def weighted_binary_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted binary cross entropy
        loss = -tf.math.reduce_sum(
            (1 - w_loss) * target * tf.math.log(pred + 1e-9)
            + w_loss * (1 - target) * tf.math.log(1 - pred + 1e-9)
        ) / (2 * target.shape[1])
        return loss


def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer="Adam"):

    misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands / 8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2 ** (-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input

    x = CausalBottleneck(
        int(expansion * in_f),
        int(in_f),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)
    x = CausalBottleneck(
        int(expansion * in_f),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)

    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 4 * wm),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    x = CausalBottleneck(
        int(expansion * in_f),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)
    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(ne)
    ne = layers.MaxPool2D((1, 2))(ne)

    ne = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalConv2D(
        filters=int(32 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(ne)
    ne = tf.squeeze(ne, axis=2)

    ne = layers.Conv1D(int(32 * wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
    )(ne)
    ne = layers.Layer(name="NE")(ne)

    # FAR END branch
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(fe)
    fe = layers.MaxPool2D((1, 2))(fe)

    fe = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalConv2D(
        filters=int(16 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(fe)
    fe = tf.squeeze(fe, axis=2)

    fe = layers.Conv1D(int(16 * wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation="sigmoid")(fe)
    fe = layers.Layer(name="FE")(fe)

    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(
        4,
        1,
    )(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name="TS")(ts)

    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    # if optimizer == "Adam":
    #     model.compile(
    #         optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    #         loss={
    #             "NE": tf.keras.losses.binary_crossentropy,
    #             "FE": tf.keras.losses.binary_crossentropy,
    #             "TS": misclass.weighted_binary_cross_entropy,
    #         },
    #         metrics=["accuracy"],
    #     )
    NE_LOSS_W = 1
    FE_LOSS_W = 1
    TS_LOSS_W = 1
    # Experimental different weight loss
    if optimizer == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
            loss={
                "NE": tf.keras.losses.binary_crossentropy,
                "FE": tf.keras.losses.binary_crossentropy,
                "TS": misclass.weighted_binary_cross_entropy,
            },
            metrics=["accuracy"],
            loss_weights = {
                "NE": NE_LOSS_W,
                "FE": FE_LOSS_W,
                "TS": TS_LOSS_W,
            }
        )


    # OBSOLETE
    # elif optimizer == "RMSProp":
    #     model.compile(
    #         optimizer=tf.keras.optimizers.RMSProp(learning_rate=0.001),
    #         loss={
    #             "NE": tf.keras.losses.binary_crossentropy,
    #             "FE": tf.keras.losses.binary_crossentropy,
    #             "TS": misclass.weighted_binary_cross_entropy,
    #         },
    #         metrics=["accuracy"],
    #     )

    return model


print("...model definition done")
# %%
# create dataset
auto = tf.data.AUTOTUNE
auto = 4

if BARK_BANDS:
    train_preprocessor = Pipeline_Preprocessor(
        SR,
        SAMPLE_LEN_SEC,
        band_type="bark",
        n_bands=48,
        target_type="dv_ts",
        augmentation=True,
    )
    val_preprocessor = Pipeline_Preprocessor(
        SR,
        SAMPLE_LEN_SEC,
        band_type="bark",
        n_bands=48,
        target_type="dv_ts",
        augmentation=False,
    )
else:
    linear_bands = np.linspace(150, 8000, 48, endpoint=False)
    train_preprocessor = Pipeline_Preprocessor(
        SR,
        SAMPLE_LEN_SEC,
        custom_bands=linear_bands,
        target_type="dv_ts",
        augmentation=True,
    )
    val_preprocessor = Pipeline_Preprocessor(
        SR,
        SAMPLE_LEN_SEC,
        custom_bands=linear_bands,
        target_type="dv_ts",
        augmentation=False,
    )

train_dataset = tf.data.Dataset.from_tensor_slices(train_list)
val_dataset = tf.data.Dataset.from_tensor_slices(val_list)

train_dataset = train_dataset.map(
    train_preprocessor.tf_get_features_from_bd, num_parallel_calls=auto
)


val_dataset = val_dataset.map(
    val_preprocessor.tf_get_features_from_bd, num_parallel_calls=auto
)

batch_size = 1
train_dataset = train_dataset.batch(batch_size)
val_dataset = val_dataset.batch(batch_size)


train_dataset = train_dataset.prefetch(auto)
val_dataset = val_dataset.prefetch(auto)

print("...dataset preparation done")
# %%
# training preparation
model_ID = datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
log_to = "/home/disks/sdb/danielefoscarin/log_tensorboard/tsd/"
checkpoint_to = "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd"
if BARK_BANDS:
    log_dir = os.path.join(log_to, "bark_" + model_ID + "/")
    checkpoint_filepath = os.path.join(checkpoint_to, "bark_" + model_ID + "/")
else:
    log_dir = os.path.join(log_to, "lin_" + model_ID + "/")
    checkpoint_filepath = os.path.join(checkpoint_to, "lin_" + model_ID + "/")
tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)
# saves after every epoch
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath + "best_model",
    save_weights_only=True,
    monitor="val_TS_loss",
    mode="min",
    save_best_only=True,
)

reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(
    monitor="val_TS_loss", factor=0.05, patience=1, min_lr=0.0001
)
model = make_tsMNet(input_bands=48, wm=0.65, expansion=2, kernel_depth=7)
model.summary()
# %%
# fit
print("...starting fit")
print("\n\n#### MODEL ID ####\n##### ", model_ID, " ######\n\n" )

epochs = args["epochs"]
history = model.fit(
    train_dataset,
    batch_size=batch_size,
    epochs=epochs,
    validation_data=val_dataset,
    callbacks=[
        tensorboard_callback,
        model_checkpoint_callback,
        reduce_lr,
        # TqdmCallback(verbose=1),
    ],
    # verbose=0,
)

print("\n#############################\nTRAINING FINISHED")
