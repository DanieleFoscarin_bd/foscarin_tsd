# %%

import bd_rd as bd
bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
import tensorflow as tf
from tensorflow.keras import layers
import numpy as np
from custom_NN_utils import *

# TS space NET
# ----------------------------------------------
class Misclassification:
    def __init__(self):
        self.correct = 0
        self.leak = 1
        self.hd_loss = 1
        self.dt_leak = 0.2
        self.fd_loss = 0.2
        self.over = 0.01
        self.noise = 0.01
        self.ok = 0.01
        self.num_classes = 4
        self.cat_loss = tf.keras.losses.CategoricalCrossentropy()
        self.w_loss_matrix = np.array(
            [
                [self.correct, self.hd_loss, self.ok, self.fd_loss],
                [self.noise, self.correct, self.leak, self.dt_leak],
                [self.ok, self.hd_loss, self.correct, self.fd_loss],
                [self.over, self.over, self.leak, self.correct],
            ]
        )

        self.w_loss_matrix = tf.convert_to_tensor(
            self.w_loss_matrix.T, dtype=tf.float32
        )

    def weighted_categorical_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted categorical cross entropy
        loss = -tf.math.reduce_sum((1 - w_loss) * target * tf.math.log(pred + 1e-9)) / (
            target.shape[1]
        )
        return loss

    def weighted_binary_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted binary cross entropy
        loss = -tf.math.reduce_sum(
            (1 - w_loss) * target * tf.math.log(pred + 1e-9)
            + w_loss * (1 - target) * tf.math.log(1 - pred + 1e-9)
        ) / (2 * target.shape[1])
        return loss


def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer="Adam"):

    misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands / 8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2 ** (-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input

    x = CausalBottleneck(
        int(expansion * in_f),
        int(in_f),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)
    x = CausalBottleneck(
        int(expansion * in_f),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)

    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 4 * wm),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    x = CausalBottleneck(
        int(expansion * in_f),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)
    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(ne)
    ne = layers.MaxPool2D((1, 2))(ne)

    ne = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalConv2D(
        filters=int(32 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(ne)
    ne = tf.squeeze(ne, axis=2)

    ne = layers.Conv1D(int(32 * wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
    )(ne)
    ne = layers.Layer(name="NE")(ne)

    # FAR END branch
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(fe)
    fe = layers.MaxPool2D((1, 2))(fe)

    fe = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalConv2D(
        filters=int(16 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(fe)
    fe = tf.squeeze(fe, axis=2)

    fe = layers.Conv1D(int(16 * wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation="sigmoid")(fe)
    fe = layers.Layer(name="FE")(fe)

    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(
        4,
        1,
    )(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name="TS")(ts)

    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    if optimizer == "Adam":
        model.compile(
            optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
            loss={
                "NE": tf.keras.losses.binary_crossentropy,
                "FE": tf.keras.losses.binary_crossentropy,
                "TS": misclass.weighted_binary_cross_entropy,
            },
            metrics=["accuracy"],
        )
    elif optimizer == "RMSProp":
        model.compile(
            optimizer=tf.keras.optimizers.RMSProp(learning_rate=0.001),
            loss={
                "NE": tf.keras.losses.binary_crossentropy,
                "FE": tf.keras.losses.binary_crossentropy,
                "TS": misclass.weighted_binary_cross_entropy,
            },
            metrics=["accuracy"],
        )

    return model


def make_NeedleHead_4inf(input_bands=48, wm=0.65, expansion=2, kernel_depth=7):

    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands / 8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2 ** (-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input

    x = CausalBottleneck(
        int(expansion * in_f),
        int(in_f),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)
    x = CausalBottleneck(
        int(expansion * in_f),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)

    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 4 * wm),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    x = CausalBottleneck(
        int(expansion * in_f),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)
    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(ne)
    ne = layers.MaxPool2D((1, 2))(ne)

    ne = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalConv2D(
        filters=int(32 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(ne)
    ne = tf.squeeze(ne, axis=2)

    ne = layers.Conv1D(int(32 * wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
    )(ne)
    ne = layers.Layer(name="NE")(ne)

    # FAR END branch
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(fe)
    fe = layers.MaxPool2D((1, 2))(fe)

    fe = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalConv2D(
        filters=int(16 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(fe)
    fe = tf.squeeze(fe, axis=2)

    fe = layers.Conv1D(int(16 * wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation="sigmoid")(fe)
    fe = layers.Layer(name="FE")(fe)

    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(
        4,
        1,
    )(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name="TS")(ts)

    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    return model