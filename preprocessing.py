# %%
# libraries
from bd_rd.processing.blocks import STFT
import bd_rd as bd
from bd_rd.io.probes import read_probe_file
import numpy as np
import os
from pf_utils import clean_probe_dir, resample_on_filename, tb_map_subprocess

from spectral_bands import Spectral_Bands
from scipy.signal import freqz

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10


import tensorflow as tf

# %%
class Pipeline_Preprocessor:
    def __init__(
        self,
        sr,
        sequence_len_sec,
        band_type=None,
        n_bands=None,
        custom_bands=None,
        target_type="talk_state",
        augmentation=True,
    ):
        """Class that controls the pipeline for a tensorflow training input

        Parameters
        ----------
        sr : int
            Sample rate.
        sequence_len_sec : int
            Lenght of the sequence in seconds.
        band_type : str, optional
            Spectral band type, by default None
        n_bands : int, optional
            Number of spectral bands, by default None
        custom_bands : array, optional
            Array with the border frequencies of custom bands, by default None
        target_type : "dv_ts", "talk_state", optional
            Format of the input target, by default "talk_state". This argument is
            obsolete and it is ignored.
        augmentation : bool, optional
            Turn on data augmentation, by default True
        """
        self.sr = sr
        self.sequence_len = sequence_len_sec * 100  # WARNING implies 10ms window

        self.n_bands = n_bands
        self.band_type = band_type
        if self.n_bands == None:
            self.n_bands = 48

        assert target_type in [
            "talk_state",
            "double_vad",
            "dv_ts",
        ], "Error: unrecognized target_type."

        self.target_type = target_type
        self.augmentation = augmentation
        self.custom_bands = custom_bands
        if self.custom_bands is not None:
            self.n_bands = self.custom_bands.shape[0]
            self.bands_processor = Spectral_Bands(
                sr=self.sr, n_fft=321, norm_bands=True, band_freqs=self.custom_bands
            )
        elif self.band_type == "full_linear":
            # this object will not be used in full_linear mode
            # it is instatiated for easier compatibility
            self.bands_processor = Spectral_Bands(
                sr=self.sr,
                n_fft=321,
                band_type="mel",
                n_bands=self.n_bands,
                keep_ends=False,
                norm_bands=True,
                low_lim=150,
            )
        else:
            self.bands_processor = Spectral_Bands(
                sr=self.sr,
                n_fft=321,
                band_type=self.band_type,
                n_bands=self.n_bands,
                keep_ends=False,
                norm_bands=True,
                low_lim=150,
            )

    def _bd_bands(self, sig):
        """
        Compute spectral bands from stft input
        """
        sig = sig.numpy()
        sig_bands = self.bands_processor.compute_np(sig)
        sig_bands = tf.convert_to_tensor(sig_bands, dtype=tf.complex64)
        sig_bands.set_shape([self.sequence_len, self.n_bands])
        return sig_bands

    def _bd_smooth(self, x, frames, type_):
        """
        Smoothing the spectrogram using bd.utils.SMOOTH()
        """
        frames = frames.numpy()
        x = x.numpy()
        smooth_ = bd.utils.SMOOTH_RATE(frames)
        x = bd.utils.SMOOTH(x, smooth_, axis=0)
        if type_ == "real":
            x = tf.convert_to_tensor(x, dtype=tf.float32)
        elif type_ == "complex":
            x = tf.convert_to_tensor(x, dtype=tf.complex64)
        return x

    def _compute_coherence(self, x_stft, x_psd, y_stft, y_psd):
        """
        Compute square magnitude coherence using stft and PSD of two signals
        """
        coh = tf.math.conj(x_stft) * y_stft
        coh = tf.py_function(
            func=self._bd_smooth,
            inp=[coh, 3, "complex"],
            Tout=tf.complex64,
        )
        coh = tf.math.conj(coh) * coh
        coh = tf.cast(coh, dtype=tf.float32)
        # stabilize coh adding epsilon in denumerator
        coh = (coh) / (1e-3 + x_psd * y_psd)
        return coh

    def _load_scenario_raw(self, curr_folder):
        # BIG WARNING this is written on a liitle endian machine!!
        # check with "sys.byteorder"

        n_bin = 321
        spectra_raw = tf.io.read_file(curr_folder + "/scenario_spectra.raw")
        target_data_raw = tf.io.read_file(curr_folder + "/scenario_target_data.raw")
        spectra = tf.io.decode_raw(
            spectra_raw, out_type=tf.complex64, little_endian=True
        )
        spectra = tf.reshape(
            spectra,
            [
                self.sequence_len,
                n_bin * 2,
            ],
        )
        aec_stft = spectra[:, :n_bin]
        replica_stft = spectra[:, n_bin:]
        aec_stft.set_shape([self.sequence_len, n_bin])
        replica_stft.set_shape([self.sequence_len, n_bin])

        target_data = tf.io.decode_raw(
            target_data_raw, out_type=tf.float64, little_endian=True
        )
        target_data = tf.reshape(
            target_data,
            [
                self.sequence_len,
                2,
            ],
        )
        # target_data = tf.cast(target_data, dtype=tf.float32)
        residual_level = tf.expand_dims(target_data[:, 0], -1)
        residual_level.set_shape([self.sequence_len, 1])
        residual_level = tf.cast(residual_level, dtype=tf.float32)

        ne_VAD = tf.expand_dims(target_data[:, 1], -1)
        ne_VAD.set_shape([self.sequence_len, 1])
        ne_VAD = tf.cast(ne_VAD, dtype=tf.int32)

        return aec_stft, replica_stft, ne_VAD, residual_level

    def _ts_after_augmentation(self, residual_level, ne_VAD):
        fe_SD = tf.where(residual_level >= 1, 1.0, residual_level)
        fe_SD = tf.where(residual_level < 1, 0.0, fe_SD)
        fe_SD = tf.cast(fe_SD, dtype=tf.int32)
        ne_VAD = tf.cast(ne_VAD, dtype=tf.int32)
        ts = ne_VAD + 2 * fe_SD
        return {"NE": ne_VAD, "FE": fe_SD, "TS": ts}

    def _compute_biquad_freqz(self):
        coeff = tf.random.uniform((4,), -3 / 8, 3 / 8)
        coeff = coeff.numpy()
        w, h = freqz([coeff[0], coeff[1]], [coeff[2], coeff[3]], worN=321, fs=self.sr)
        h = np.expand_dims(h, axis=0)
        hh = np.repeat(h, self.sequence_len, axis=0)
        hh = tf.convert_to_tensor(hh, dtype=tf.complex64)
        hh.set_shape([self.sequence_len, 321])
        return hh

    def _stft_to_input_features(self, aec_stft, replica_stft, sin_stft):
        # from stft to squared magnitude (_sm), it is real.
        replica_sm = tf.math.conj(replica_stft) * replica_stft
        replica_sm = tf.cast(replica_sm, dtype=tf.float32)
        aec_sm = tf.math.conj(aec_stft) * aec_stft
        aec_sm = tf.cast(aec_sm, dtype=tf.float32)
        sin_sm = tf.math.conj(sin_stft) * sin_stft
        sin_sm = tf.cast(sin_sm, dtype=tf.float32)

        # from squared magnitude to PSD using bd smoothing filter
        replica_psd = tf.py_function(
            func=self._bd_smooth,
            inp=[replica_sm, 3, "real"],
            Tout=tf.float32,
        )
        aec_psd = tf.py_function(
            func=self._bd_smooth,
            inp=[aec_sm, 3, "real"],
            Tout=tf.float32,
        )

        sin_psd = tf.py_function(
            func=self._bd_smooth,
            inp=[sin_sm, 3, "real"],
            Tout=tf.float32,
        )

        # computing coherence -------------
        # TODO test other coherence combinations
        coh = self._compute_coherence(aec_stft, aec_psd, sin_stft, sin_psd)
        coh = 1 - coh

        if self.band_type != "full_linear":
            sin_sm = tf.py_function(
                func=self._bd_bands, inp=[sin_sm], Tout=tf.complex64
            )

            replica_sm = tf.py_function(
                func=self._bd_bands, inp=[replica_sm], Tout=tf.complex64
            )

            aec_sm = tf.py_function(
                func=self._bd_bands, inp=[aec_sm], Tout=tf.complex64
            )

            coh = tf.py_function(func=self._bd_bands, inp=[coh], Tout=tf.complex64)

        # log of the real spectrograms
        sin_sm = tf.math.log(tf.cast(sin_sm, dtype=tf.float32) + 1e-3)
        replica_sm = tf.math.log(tf.cast(replica_sm, dtype=tf.float32) + 1e-3)
        aec_sm = tf.math.log(tf.cast(aec_sm, dtype=tf.float32) + 1e-3)
        coh = tf.cast(coh, dtype=tf.float32)
        coh = tf.math.log(tf.cast(coh, dtype=tf.float32) + 1e-3)

        # adjust range for stability
        # scale_up = np.log(1e-3)
        scale_up = 6.91
        replica_sm += scale_up
        replica_sm /= 10
        aec_sm += scale_up
        aec_sm /= 10
        sin_sm += scale_up
        sin_sm /= 10
        coh += scale_up
        coh /= 6

        features = tf.stack([aec_sm, replica_sm, coh], axis=-1)

        return features

    def tf_get_features_from_bd(self, curr_folder):
        """Preprocessing pipeline. To be used mapped to a tf.data.Dataset.

        Parameters
        ----------
        curr_folder : tf.tensor
            Path from a tf.data.Dataset iteration

        Returns
        -------
        tuple (tf.tensor, tf.tensor)
            Features and target
        """

        # LOAD DATA -------------------------------------------------
        aec_stft, replica_stft, ne_VAD, residual_level = self._load_scenario_raw(
            curr_folder
        )

        # AUGMENTATION ----------------------------------------------
        if self.augmentation:
            # TODO think better about augmentiation gains
            # first augmentation, random biquad filter
            hh = tf.py_function(
                func=self._compute_biquad_freqz,
                inp=[],
                Tout=tf.complex64,
            )
            hh.set_shape([self.sequence_len, 321])

            aec_stft = tf.math.multiply(aec_stft, hh)
            replica_stft = tf.math.multiply(replica_stft, hh)

            # second augmentation, random gain +- 6 dB
            rand_gain_db1 = tf.random.uniform(shape=[], minval=-6, maxval=6)
            rand_gain1 = 10 ** (rand_gain_db1 / 20)
            rand_gain1 = tf.cast(rand_gain1, dtype=tf.complex64)
            rand_gain_db2 = tf.random.uniform(shape=[], minval=-6, maxval=6)
            rand_gain2 = 10 ** (rand_gain_db2 / 20)
            rand_gain2 = tf.cast(rand_gain2, dtype=tf.complex64)
            rand_gain_db_total = tf.random.uniform(shape=[], minval=-6, maxval=6)
            rand_gain_total = 10 ** (rand_gain_db_total / 20)
            rand_gain_total = tf.cast(rand_gain_total, dtype=tf.complex64)

            aec_stft *= rand_gain1 * rand_gain_total

            replica_stft *= rand_gain2 * rand_gain_total

            residual_level *= 10 ** (rand_gain_db2 / 20) * 10 ** (
                rand_gain_db_total / 20
            )

        # get sin after augmentation
        sin_stft = aec_stft + replica_stft

        # compute features
        features = self._stft_to_input_features(aec_stft, replica_stft, sin_stft)

        # compute target
        target = self._ts_after_augmentation(residual_level, ne_VAD)

        return features, target

    def inference_preprocessing(
        self,
        sin_path,
        rin_path,
        bin_folder,
        temp_tbmap_folder,
        temp_sox_folder,
        tbmap_path=None,
        conf_path=None,
        clean_probes_after=True,
        pre_gain=1,
        verbose=False,
    ):
        """Prepare the input for inference, without a target.
            This is not made to be used with tf dataset objects,
            call this function before feeding the NN

        Parameters
        ----------
        sin_path : str
            Filename of sIn signal.
        rin_path : str
            Filename of rIn signal.
        bin_folder : str
            Path to the bin folder
        temp_tbmap_folder : str
            Path to the temp folder for tbmap files
        temp_sox_folder : str
            Path to the temp folder for sox files
        clean_probes_after : bool, optional
            Remove probes for the temp_tbmap_folder after processing, by default True
        pre_gain : int, optional
            Linear gain applied to the input probes, by default 1
        verbose : bool, optional
            Display the tbmap text output, by default False

        Returns
        -------
        dictionary
            'features', 'aec_stft', 'replica_stft'
        """
        # Compatibility default fix
        # TODO fix this issue where it is used
        if tbmap_path is None:
            tbmap_path = os.path.join(bin_folder, "tb_map_21_07")
        if conf_path is None:
            conf_path = os.path.join(bin_folder, "nrai20_aec_only.conf")
        # --------------------------------------------------

        sin_path_rs, sin_safe_prefix = resample_on_filename(
            sin_path, temp_sox_folder, self.sr
        )
        rin_path_rs, rin_safe_prefix = resample_on_filename(
            rin_path, temp_sox_folder, self.sr
        )
        data_folder = os.path.basename(sin_path_rs)  # only needed for compatibility

        tb_map_subprocess(
            bin_folder,
            data_folder,
            tbmap_path,
            sin_path_rs,
            rin_path_rs,
            os.path.join(temp_tbmap_folder, "temp_sOut.wav"),
            conf_path,
            verbose=verbose,
            path_mode="full",
        )

        os.remove(sin_path_rs)
        os.remove(rin_path_rs)

        aec_stft = read_probe_file(
            os.path.join(temp_tbmap_folder, "probe_ECHO_CANCELLER_stft_s_out0.dat")
        ).data
        aec_stft *= pre_gain
        aec_stft = tf.convert_to_tensor(aec_stft, dtype=tf.complex64)

        replica_stft = read_probe_file(
            os.path.join(
                temp_tbmap_folder, "probe_ECHO_CANCELLER_constrained_echo_replica0.dat"
            )
        ).data
        replica_stft *= pre_gain
        replica_stft = tf.convert_to_tensor(replica_stft, dtype=tf.complex64)

        sin_stft = aec_stft + replica_stft

        self.sequence_len = aec_stft.shape[0]
        self.sequence_len = tf.cast(self.sequence_len, dtype=tf.int32)

        if clean_probes_after:
            clean_probe_dir(temp_tbmap_folder, verbose=False)

        # process the features as in the validation dataset
        features = self._stft_to_input_features(aec_stft, replica_stft, sin_stft)

        # output dictionary
        out = {
            "features": features,
            "aec_stft": aec_stft,
            "replica_stft": replica_stft,
        }

        return out


# %%
# import matplotlib.pyplot as plt
# linear_bands = np.linspace(150, 8000, 48, endpoint=False)
# preprocessor = Pipeline_Preprocessor(16000, 20, custom_bands=linear_bands, target_type="dv_ts")
# feat, target = preprocessor.tf_get_features_from_bd("/home/danielefoscarin/LOCAL/SSD/generated/dataset_1206/train_scenario_0")
# plt.figure()
# plt.plot(target["TS"])
# plt.plot(target["NE"])
# plt.plot(target["FE"])
# plt.show()
# %%
