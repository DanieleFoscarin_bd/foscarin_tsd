# %%
import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(rootrepodir)

import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt
from tensorflow.keras import layers
import keras_flops
from custom_NN_utils import CausalBottleneck, CausalConv2D


from layers import (
    Layer,
    Dense,
    Conv2D,
    VerticalPointwise,
    DepthwiseConv2D,
    VerticalMaxPooling,
)
from keras_to_np import keras_to_np
from multi_layers import divide_CBConv, divide_CSConv, CBConv, CSConv

# %%
# TS space NET
# ----------------------------------------------


def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer="Adam"):

    # misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands / 8))

    # input_shape = (2000, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2 ** (-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input

    x1 = CausalBottleneck(
        int(expansion * in_f),
        int(in_f),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="init_cbconv0",
    )(x)
    x2 = CausalBottleneck(
        int(expansion * in_f),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
        c_name="init_cbconv1",
    )(x1)

    x3 = layers.MaxPool2D((1, 2), name="init_maxpool0")(x2)

    x4 = CausalBottleneck(
        int(expansion * 4 * wm),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="init_cbconv2",
    )(x3)

    x5 = CausalBottleneck(
        int(expansion * in_f),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
        c_name="init_cbconv3",
    )(x4)
    x6 = layers.MaxPool2D((1, 2), name="init_maxpool1")(x5)

    x7 = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="init_cbconv4",
    )(x6)

    ne = x7
    fe = x7

    # NEAR END branch
    ne1 = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="ne_cbconv0",
    )(ne)
    ne2 = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
        c_name="ne_cbconv1",
    )(ne1)
    ne3 = layers.MaxPool2D((1, 2), name="ne_maxpool0")(ne2)

    ne4 = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="ne_cbconv2",
    )(ne3)
    ne5 = CausalConv2D(
        filters=int(32 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
        c_name="ne_csconv0",
    )(ne4)
    ne6 = tf.squeeze(ne5, axis=2)

    ne7 = layers.Conv1D(int(32 * wm), 1, name="ne_conv1d0")(ne6)
    ne8 = layers.BatchNormalization(name="ne_bn")(ne7)
    ne9 = layers.ReLU(MAX, name="ne_relu")(ne8)
    ne10 = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
        name="ne_conv1d_sigmoid",
    )(ne9)
    ne11 = layers.Layer(name="NE")(ne10)

    # FAR END branch
    fe1 = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="fe_cbconv0",
    )(fe)
    fe2 = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
        c_name="fe_cbconv1",
    )(fe1)
    fe3 = layers.MaxPool2D((1, 2), name="fe_maxpool0")(fe2)

    fe4 = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
        c_name="fe_cbconv2",
    )(fe3)
    fe5 = CausalConv2D(
        filters=int(16 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
        c_name="fe_csconv0",
    )(fe4)
    fe6 = tf.squeeze(fe5, axis=2)

    fe7 = layers.Conv1D(int(16 * wm), 1, name="fe_conv1d0")(fe6)
    fe8 = layers.BatchNormalization(name="fe_bn")(fe7)
    fe9 = layers.ReLU(MAX, name="fe_relu")(fe8)
    fe10 = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
        name="fe_conv1d_sigmoid",
    )(fe9)
    fe11 = layers.Layer(name="FE")(fe10)

    # TALK STATE space
    ts = tf.concat(
        [ne11, tf.square(ne11), fe11, tf.square(fe11)],
        axis=2,
    )
    ts1 = layers.Conv1D(32, 1, name="ts_conv1d0")(ts)
    ts2 = layers.BatchNormalization(name="ts_bn0")(ts1)
    ts3 = layers.ReLU(MAX, name="ts_relu0")(ts2)
    ts4 = layers.Conv1D(16, 1, name="ts_conv1d1")(ts3)
    ts5 = layers.BatchNormalization(name="ts_bn1")(ts4)
    ts6 = layers.ReLU(MAX, name="ts_relu1")(ts5)
    ts7 = layers.Conv1D(4, 1, name="ts_conv1d2")(ts6)
    ts8 = layers.Softmax(name="ts_softmax")(ts7)
    ts9 = layers.Layer(name="TS")(ts8)

    # model = tf.keras.Model(inputs=input, outputs=[ne11, fe11, ts9])
    model = tf.keras.Model(inputs=input, outputs=[ne11, fe11, ts9])

    return model


# %%
print("load model weigths...")
model_trained = make_tsMNet(
    input_bands=48,
    wm=0.65,
    expansion=2,
    kernel_depth=7,
)
trained_filepath = (
    # "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/lin_20211115-123811" #this one is the best
    "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/lin_20211116-141429"
)
print(
    "Flops per frame: ", "{:e}".format(keras_flops.get_flops(model_trained, 1) / 2000)
)
# %%
model_trained.load_weights(os.path.join(trained_filepath, "best_model"))

# %%
l_names = [layer.name for layer in model_trained.layers]

# %%
x = np.random.random((1, 1, 48, 3)) * 6
x = np.ones((1, 1, 48, 3)) * 0
x_tf = tf.convert_to_tensor(x)


cbconv1 = model_trained.get_layer(l_names[1])
# exp, depth, proj, residual = divide_CBConv(cbconv1)
sub_layers = divide_CBConv(cbconv1)
np_cbconv = CBConv(sub_layers)

early1 = tf.keras.Model(
    inputs=model_trained.input, outputs=model_trained.get_layer(l_names[1]).output
)


out_tf = early1(x_tf)
out_np = np_cbconv.compute_frame_np(x[0, 0])
print(out_np[1])
print(out_tf[0, -1, 1])
print(np.max(np.abs(out_np - out_tf[0, -1])))


# %%



class NpNeedleHead:
    # NB this is specific for the current needle head architecture,
    # could it be generalized?
    # Maybe after a OO refactoring of the multi-layers

    def __init__(self, model):
        self.model = model
        self.early_out = []

        # build every layer
        self.layers = {
            "init_cbconv0": CBConv(divide_CBConv(self.model.get_layer(l_names[1]))),
            "init_cbconv1": CBConv(divide_CBConv(self.model.get_layer(l_names[2]))),
            "init_maxpool0": VerticalMaxPooling(
                keras_to_np(self.model.get_layer(l_names[3]))
            ),
            "init_cbconv2": CBConv(divide_CBConv(self.model.get_layer(l_names[4]))),
            "init_cbconv3": CBConv(divide_CBConv(self.model.get_layer(l_names[5]))),
            "init_maxpool1": VerticalMaxPooling(
                keras_to_np(self.model.get_layer(l_names[6]))
            ),
            "init_cbconv4": CBConv(divide_CBConv(self.model.get_layer(l_names[7]))),
            # end of init branch
            #  NE FE branches
            # order is as returned from summary()
            "ne_cbconv0": CBConv(divide_CBConv(self.model.get_layer(l_names[8]))),
            "fe_cbconv0": CBConv(divide_CBConv(self.model.get_layer(l_names[9]))),
            "ne_cbconv1": CBConv(divide_CBConv(self.model.get_layer(l_names[10]))),
            "fe_cbconv1": CBConv(divide_CBConv(self.model.get_layer(l_names[11]))),
            "ne_maxpool0": VerticalMaxPooling(
                keras_to_np(self.model.get_layer(l_names[12]))
            ),
            "fe_maxpool0": VerticalMaxPooling(
                keras_to_np(self.model.get_layer(l_names[13]))
            ),
            "ne_cbconv2": CBConv(divide_CBConv(self.model.get_layer(l_names[14]))),
            "fe_cbconv2": CBConv(divide_CBConv(self.model.get_layer(l_names[15]))),
            "ne_csconv0": CSConv(divide_CSConv(self.model.get_layer(l_names[16]))),
            "fe_csconv0": CSConv(divide_CSConv(self.model.get_layer(l_names[17]))),
            # squeeze
            # squeeze
            "ne_conv1d0": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[20]),
                    batch_norm=self.model.get_layer(l_names[22]),
                    activation=self.model.get_layer(l_names[24]),
                )
            ),
            "fe_conv1d0": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[21]),
                    batch_norm=self.model.get_layer(l_names[23]),
                    activation=self.model.get_layer(l_names[25]),
                )
            ),
            "ne_conv1d_sigmoid": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[26]),
                    activation=tf.keras.layers.Activation("sigmoid"),
                )
            ),
            "fe_conv1d_sigmoid": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[27]),
                    activation=tf.keras.layers.Activation("sigmoid"),
                )
            ),
            # TS branch
            # square
            # square
            # concat
            "ts_conv1d0": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[33]),
                    batch_norm=self.model.get_layer(l_names[34]),
                    activation=self.model.get_layer(l_names[35]),
                )
            ),
            "ts_conv1d1": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[36]),
                    batch_norm=self.model.get_layer(l_names[37]),
                    activation=self.model.get_layer(l_names[38]),
                )
            ),
            "ts_conv1d2": VerticalPointwise(
                keras_to_np(
                    base=self.model.get_layer(l_names[39]),
                    activation=self.model.get_layer(l_names[40]),
                )
            ),
        }

    def nn_reset(self):
        for name, layer in self.layers.items():
            layer.reset()

    def intermediate_check(self):
        for layer in l_names:
            early = tf.keras.Model(
                input=self.model.input,
                outputs=self.model.get_layer(l_names[layer]).output,
            )
            self.early_out.append(early)
        # TODO finish check and assert

    def compute_frame_np(self, x_in):
        assert (
            len(x_in.shape) == 2
        ), "Expected frame with 2 dimensions: frequency, channel."

        # initial branch
        x = x_in.copy()
        x = self.layers["init_cbconv0"].compute_frame_np(x)
        x = self.layers["init_cbconv1"].compute_frame_np(x)
        x = self.layers["init_maxpool0"].compute_frame_np(x)
        x = self.layers["init_cbconv2"].compute_frame_np(x)
        x = self.layers["init_cbconv3"].compute_frame_np(x)
        x = self.layers["init_maxpool1"].compute_frame_np(x)
        x = self.layers["init_cbconv4"].compute_frame_np(x)

        # ne branch
        ne = x.copy()
        ne = self.layers["ne_cbconv0"].compute_frame_np(ne)
        ne = self.layers["ne_cbconv1"].compute_frame_np(ne)
        ne = self.layers["ne_maxpool0"].compute_frame_np(ne)
        ne = self.layers["ne_cbconv2"].compute_frame_np(ne)
        ne = self.layers["ne_csconv0"].compute_frame_np(ne)
        ne = self.layers["ne_conv1d0"].compute_frame_np(ne)
        ne = self.layers["ne_conv1d_sigmoid"].compute_frame_np(ne)
        
        # fe branch
        fe = x.copy()
        fe = self.layers["fe_cbconv0"].compute_frame_np(fe)
        fe = self.layers["fe_cbconv1"].compute_frame_np(fe)
        fe = self.layers["fe_maxpool0"].compute_frame_np(fe)
        fe = self.layers["fe_cbconv2"].compute_frame_np(fe)
        fe = self.layers["fe_csconv0"].compute_frame_np(fe)
        fe = self.layers["fe_conv1d0"].compute_frame_np(fe)
        fe = self.layers["fe_conv1d_sigmoid"].compute_frame_np(fe)

        # ts branch
        ts = np.concatenate((ne, np.square(ne), fe, np.square(fe))).T
        ts = self.layers["ts_conv1d0"].compute_frame_np(ts)
        ts = self.layers["ts_conv1d1"].compute_frame_np(ts)
        ts = self.layers["ts_conv1d2"].compute_frame_np(ts)

        return ne, fe, ts

    def compute_np(self, x_in, reset_before=True):
        assert len(x_in.shape) == 3, "Expected 3-dimensional input: (frames, frequency, channels)."
        if reset_before:
            self.nn_reset()
        ne_seq = np.zeros((x_in.shape[0], 1))
        fe_seq = np.zeros((x_in.shape[0], 1))
        ts_seq = np.zeros((x_in.shape[0], 4))
        for frame in range(x_in.shape[0]):
            ne_seq[frame], fe_seq[frame], ts_seq[frame] = self.compute_frame_np(x_in[frame])
        return ne_seq, fe_seq, ts_seq


# %%
np_model = NpNeedleHead(model_trained)
np_model.nn_reset()

x = np.random.random((1, 1, 48, 3)) * 10
# x = np.ones((1, 1, 48, 3)) * 0
x_tf = tf.convert_to_tensor(x)

out_np = np_model.compute_frame_np(x[0,0])
out_tf = model_trained(x_tf)

print(out_np[2])
print(out_tf[2])
print(f"Maximum numerical error {np.max(np.abs(out_np[2]-out_tf[2]))}")
print(out_np[2].shape)
print(out_tf[2].shape)
# %%
x = np.random.random((1, 2000, 48, 3)) * 10
# x = np.ones((1, 1, 48, 3)) * 0
x_tf = tf.convert_to_tensor(x)
out_np = np_model.compute_np(x[0])
out_tf = model_trained(x_tf)
print(out_np[2].shape)
print(out_tf[2].shape)
print(f"Maximum numerical error {np.max(np.abs(out_np[2]-out_tf[2]))}")


# %%
plt.figure()
plt.plot(out_tf[0][0])
plt.plot(out_np[0])
# %%
