# %%
import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
import os
import sys

import numpy as np
import tensorflow as tf
from tensorflow import keras

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(rootrepodir)
from layers import (
    Layer,
    Dense,
    Conv2D,
    VerticalPointwise,
    DepthwiseConv2D,
    VerticalMaxPooling,
)
from keras_to_np import keras_to_np
from multi_layers import divide_CBConv, divide_CSConv, CBConv
from custom_NN_utils import CausalBottleneck, CausalConv2D

np.random.seed(seed=42)
tf.random.set_seed(42)


def test_Dense_bn_relu():
    x = np.random.random((2, 8, 1)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)

    dense_tf = keras.layers.Dense(16)
    bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.ReLU()

    out_tf = dense_tf(x_tf)
    out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)

    dense_np = Dense(keras_to_np(base=dense_tf, batch_norm=bn_tf, activation=act))
    out_np = dense_np.compute_frame_np(x)
    assert np.allclose(out_np, out_tf, rtol=1e-5)
    assert out_np.shape == out_tf.shape


def test_Dense_bn_relu2():
    x = np.random.random((2, 8, 1)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)

    dense_tf = keras.layers.Dense(16)
    # bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.ReLU()

    out_tf = dense_tf(x_tf)
    # out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)

    dense_np = Dense(keras_to_np(base=dense_tf, activation=act))
    out_np = dense_np.compute_frame_np(x)
    assert np.allclose(out_np, out_tf, rtol=1e-5)
    assert out_np.shape == out_tf.shape


def test_Dense_bn_softmax():
    x = np.random.random((24, 8, 3)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)

    dense_tf = keras.layers.Dense(32)
    # bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.Softmax()

    out_tf = dense_tf(x_tf)
    # out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)

    dense_np = Dense(keras_to_np(base=dense_tf, activation=act))
    out_np = dense_np.compute_frame_np(x)
    assert np.allclose(out_np, out_tf, rtol=1e-5)
    assert out_np.shape == out_tf.shape


def test_VerticalPointwise1():
    x = np.random.random((1, 1, 8, 7)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)
    conv_tf = keras.layers.Conv1D(8, 1, padding="valid")
    out_tf = conv_tf(x_tf)
    conv_np = VerticalPointwise(keras_to_np(conv_tf))
    out_np = conv_np.compute_frame_np(x[0, 0, :, :])
    assert np.allclose(out_np, np.squeeze(out_tf), rtol=1e-5)
    # assert out_np.shape == out_tf[0, 0].shape


# def test_DepthwiseConv2D():
#     x = np.random.random((1, 1, 8, 6)) * 10 - 5
#     pad = np.zeros((1, 5, 8, 4))
#     x_pad = np.concatenate((pad, x), axis=1)
#     x_tf = tf.convert_to_tensor(x_pad)
#     dw_tf = keras.layers.DepthwiseConv2D((6, 3))
#     bn_tf = keras.layers.BatchNormalization()
#     act = keras.layers.ReLU(1)

#     out_tf = dw_tf(x_tf)
#     out_tf = bn_tf(out_tf)
#     out_tf = act(out_tf)
#     conf = keras_to_np(dw_tf, bn_tf, act, padding="causal_valid")
#     dw_np = DepthwiseConv2D(conf)
#     out_np = dw_np.compute_frame_np(x[0, 0, :, :])
#     assert np.allclose(out_np, out_tf[0, -1], rtol=1e-5)


def test_DepthwiseConv2D_causal():
    x = np.random.random((1, 1, 8, 4)) * 10 - 5
    x_pad = np.zeros((1, 6, 10, 4))
    x_pad[:, -1, 1:9, :] = x
    x_tf = tf.convert_to_tensor(x_pad)
    dw_tf = keras.layers.DepthwiseConv2D((6, 3))
    bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.ReLU(1)

    out_tf = dw_tf(x_tf)
    out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)
    conf = keras_to_np(dw_tf, bn_tf, act, padding="causal")
    dw_np = DepthwiseConv2D(conf)
    out_np = dw_np.compute_frame_np(x[0, 0, :, :])
    assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)

def test_DepthwiseConv2D_causal_with_b():
    x = np.random.random((1, 1, 8, 4)) * 10 - 5
    x_pad = np.zeros((1, 6, 10, 4))
    x_pad[:, -1, 1:9, :] = x
    x_tf = tf.convert_to_tensor(x_pad)
    dw_tf = keras.layers.DepthwiseConv2D((6, 3))
    bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.ReLU(1)

    temp_out_tf = dw_tf(x_tf)
    _ = bn_tf(temp_out_tf)
    dw_tf.set_weights([np.ones((1,7,8)), np.ones((8,))])
    bn_tf.set_weights([np.ones((8,))*3, np.ones((8,))*0, np.ones((8,))*3, np.ones((8,))*0])
    # bn_tf.set_weights([np.zeros((8,)), np.zeros((8,)), np.zeros((8,)), np.zeros((8,))])

    out_tf = dw_tf(x_tf)
    out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)
    conf = keras_to_np(dw_tf, bn_tf, act, padding="causal")
    dw_np = DepthwiseConv2D(conf)
    out_np = dw_np.compute_frame_np(x[0, 0, :, :])
    assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)


def test_Conv2D_causal():
    x = np.random.random((1, 1, 8, 4)) * 10 - 2
    x_pad = np.zeros((1, 6, 10, 4))
    x_pad[:, -1, 1:9, :] = x
    x_tf = tf.convert_to_tensor(x_pad)
    conv_tf = keras.layers.Conv2D(5, (6, 3))
    bn_tf = keras.layers.BatchNormalization()
    act = keras.layers.Softmax()

    out_tf = conv_tf(x_tf)
    out_tf = bn_tf(out_tf)
    out_tf = act(out_tf)
    conf = keras_to_np(conv_tf, bn_tf, act, padding="causal")
    conv_np = Conv2D(conf)
    conv_np.reset()
    out_np = conv_np.compute_frame_np(x[0, 0, :, :])
    assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)


def test_VerticalMaxPooling():
    x = np.random.random((1, 1, 14, 5)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)
    mp = keras.layers.MaxPooling2D((1, 2))
    out_tf = mp(x_tf)
    mp_np = VerticalMaxPooling(keras_to_np(mp))
    out_np = mp_np.compute_frame_np(x[0, 0])
    print(out_np)
    print(out_tf[0, 0])
    assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)

def test_cbconv():
    x = np.random.random((1, 1, 8, 4)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)
    cbconv = CausalBottleneck(8, 4, (6,3))
    out_tf = cbconv(x_tf)
    sub_layers = divide_CBConv(cbconv)
    np_cbconv = CBConv(sub_layers)
    out_np = np_cbconv.compute_frame_np(x[0,0])
    assert np.allclose(out_np, out_tf[0, -1], rtol=1e-5)


# %%
if __name__ == "__main__":
    # # x = np.random.random((1, 1, 8, 7)) * 10 - 5
    # x = np.zeros((1, 1, 8, 7))
    # x_tf = tf.convert_to_tensor(x)
    # conv_tf = keras.layers.Conv1D(8, 1, padding="valid")
    # bn_tf = keras.layers.BatchNormalization()
    # temp_out_tf = conv_tf(x_tf)
    # _ = bn_tf(temp_out_tf)
    # conv_tf.set_weights([np.ones((1,7,8)), np.ones((8,))])
    # bn_tf.set_weights([np.ones((8,))*3, np.ones((8,))*0, np.ones((8,))*3, np.ones((8,))*0])
    # # bn_tf.set_weights([np.zeros((8,)), np.zeros((8,)), np.zeros((8,)), np.zeros((8,))])
    # out_tf = conv_tf(x_tf)
    # out_tf = bn_tf(out_tf)


    # conv_np = VerticalPointwise(
    #     keras_to_np(
    #         conv_tf, 
    #         bn_tf,
    #     )
    # )
    # out_np = conv_np.compute_frame_np(x[0, 0, :, :])
    # print(out_np)
    # print(out_tf[0,0])

    # assert np.allclose(out_np, np.squeeze(out_tf), rtol=1e-5)

    x = np.random.random((1, 1, 8, 4)) * 10 - 5
    x = np.zeros((1,1,8,4))
    x_pad = np.zeros((1, 6, 10, 4))
    x_pad[:, -1, 1:9, :] = x
    x_tf = tf.convert_to_tensor(x_pad)
    dw_tf = keras.layers.DepthwiseConv2D((6, 3))
    bn_tf = keras.layers.BatchNormalization()
    # act = keras.layers.ReLU(1)

    temp_out_tf = dw_tf(x_tf)
    _ = bn_tf(temp_out_tf)
    dw_tf.set_weights([np.ones((6,3,4,1)), np.ones((4,))*100])
    bn_tf.set_weights([np.ones((4,)), np.ones((4,)), np.ones((4,)), np.ones((4,))])
    # bn_tf.set_weights([np.zeros((8,)), np.zeros((8,)), np.zeros((8,)), np.zeros((8,))])

    out_tf = dw_tf(x_tf)
    out_tf = bn_tf(out_tf)
    # out_tf = act(out_tf)
    conf = keras_to_np(
        dw_tf, 
        bn_tf, 
        # act, 
        padding="causal"
    )
    dw_np = DepthwiseConv2D(conf)
    out_np = dw_np.compute_frame_np(x[0, 0, :, :])
    # assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)
    print(out_np)
    print(out_tf[0,0])
    print(np.max(np.abs(out_np - out_tf[0,0])))














# %%

