#%%
import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks._common import Generic_Processor
import numpy as np
from keras_to_np import *


def _linear(x):
    return x


def _relu(x, max_value=None):
    return np.maximum(x, 0)


class _relu_max:
    def __init__(self, max_value):
        self.max_value = max_value

    def __call__(self, x):
        return np.clip(x, 0, self.max_value)


def _sigmoid(x):
    y = np.zeros(x.shape)
    y[x >= 0] = 1.0 / (1.0 + np.exp(-1 * x[x >= 0]))
    z = np.exp(x[x < 0])
    y[x < 0] = z / (1.0 + z)
    return y


def _softmax(x):
    # y = np.exp(x) / np.sum(np.exp(x))
    e = np.exp(x - np.max(x, axis=-1, keepdims=True))
    s = np.sum(e, axis=-1, keepdims=True)
    y = e / s
    return y


def _tanh(x):
    y = np.tanh(x)
    return y


class Layer(Generic_Processor):
    """
    Parent of NN layers
    """
    is_invertible = False

    supported_act = {
        None: _linear,
        "linear": _linear,
        "relu": _relu,
        "tanh": _tanh,
        "sigmoid": _sigmoid,
        "softmax": _softmax,
    }

    def __init__(self, config=None):
        self.W = None
        self.b = None
        self.buffer = None

        super().__init__(config)

    def _apply_batch_norm(self, base_weights, bn_weights):
        W = base_weights["W"]
        b = base_weights["b"]
        if bn_weights["gamma"] is None:
            # do not apply batch norm
            pass
        else:
            gamma = bn_weights["gamma"]
            beta = bn_weights["beta"]
            mu = bn_weights["mu"]
            sigma = bn_weights["sigma"]
            for i in range(self.filters):
                den = np.sqrt(1e-3 + sigma[i])
                W[..., i] *= gamma[i] / den
                b[i] = gamma[i] * (b[i] - mu[i]) / den + beta[i]
        return W, b

    def set_weights(self, base_weights, bn_weigths):
        W, b = self._apply_batch_norm(base_weights, bn_weigths)
        self.W = W
        self.b = b

    def get_weights(self):
        # WARNING these are the weigths AFTER batch normalization
        out = {"w": self.W, "b": self.b}
        return out

    def to_C(self):
        out_string = []
        out_string.append(str(self.W))
        out_string.append(str(self.b))
        return out_string

    def compute_frame_np(self):
        pass

    def compute_np(self):
        pass

    def reset(self):
        self.buffer = None


class Dense(Layer):

    default_config = {"layer_conf": None}

    def __init__(self, layer_conf=None, config=None):
        # TODO documentation here

        self.assign_values(config=config, layer_conf=layer_conf)

        super().__init__(config)
        # reset here
        assert layer_conf["name"] == "dense", "Invalid configuration for dense layer"

        self.ID = layer_conf["ID"]
        self.W = None
        self.b = None
        self.bn_weights = layer_conf["bn_weights"]
        self.set_weights(layer_conf["base_weights"], layer_conf["bn_weights"])
        if (
            layer_conf["activation"] == "relu"
            and layer_conf["relu_max_value"] is not None
        ):
            self.act = _relu_max(layer_conf["relu_max_value"])
        else:
            self.act = self.supported_act[layer_conf["activation"]]

    # need to rewrite batch norm only for dense,
    def _apply_batch_norm(self, base_weights, bn_weights):
        """
        Apply batch normalization directly on self.W and self.b
        """
        W = self.layer_conf["base_weights"]["W"]
        b = self.layer_conf["base_weights"]["b"]
        if bn_weights["gamma"] is None:
            # do not apply batch norm
            pass
        else:
            gamma = self.layer_conf["bn_weights"]["gamma"]
            beta = self.layer_conf["bn_weights"]["beta"]
            mu = self.layer_conf["bn_weights"]["mu"]
            sigma = self.layer_conf["bn_weights"]["sigma"]

            den = np.sqrt(1e-3 + sigma)
            W = gamma * W / den
            b = gamma * (b - mu) / den + beta
        return W, b

    def compute_frame_np(self, x):
        out = self.act(np.dot(x, self.W) + self.b)
        return out


class VerticalPointwise(Layer):

    default_config = {"layer_conf": None}

    def __init__(self, layer_conf=None, config=None):
        # TODO documentation here,
        self.assign_values(config=config, layer_conf=layer_conf)
        super().__init__(config)

        assert (
            layer_conf["name"] == "conv1d"
        ), "VerticalPoinwise must be instatiated from a conv1d keras layer"

        self.kernel_size = layer_conf["kernel_size"]
        assert self.kernel_size == (1,), "Unsupported kernel size, must be (1,)"

        self.ID = layer_conf["ID"]

        if (
            layer_conf["activation"] == "relu"
            and layer_conf["relu_max_value"] is not None
        ):
            self.act = _relu_max(layer_conf["relu_max_value"])
        else:
            self.act = self.supported_act[layer_conf["activation"]]
        self.filters = layer_conf["filters"]
        self.dilation_rate = layer_conf["dilation_rate"]
        self.strides = layer_conf["strides"]

        self.W = None
        self.b = None
        self.bn_weights = layer_conf["bn_weights"]
        self.set_weights(layer_conf["base_weights"], layer_conf["bn_weights"])

    def compute_frame_np(self, x):
        assert (
            len(x.shape) == 2
        ), "Expected frame with 2 dimensions: frequency, channel."
        assert self.dilation_rate == (1,), "Unsupported dilation rate for pointwise"
        assert self.strides == (1,), "Unsupported strides for pointwise"
        # No need for a buffer here, it only works on the frequency axis

        out = np.zeros((x.shape[0] - self.kernel_size[0] + 1, self.filters))
        for chan in range(self.filters):
            k = self.W[:, :, chan]
            for freq in range(x.shape[0] - self.kernel_size[0] + 1):
                out[freq, chan] = (
                    np.sum(k * x[freq : freq + self.kernel_size[0], :]) + self.b[chan]
                )
        out = self.act(out)
        return out

    def compute_np(self, x):
        out = np.zeros((x.shape[0], x.shape[1] - self.kernel_size[0] + 1, self.filters))
        for frame in range(x.shape[0]):
            out[frame, :, :] = self.compute_frame_np(x[frame, :, :])
        return out


class DepthwiseConv2D(Layer):
    default_config = {"layer_conf": None}

    def __init__(self, layer_conf=None, config=None):
        self.assign_values(config=config, layer_conf=layer_conf)
        super().__init__(config)

        assert (
            layer_conf["name"] == "depthwiseconv2d"
        ), "Invalid configuration for depthwise layer"

        self.ID = layer_conf["ID"]

        if (
            layer_conf["activation"] == "relu"
            and layer_conf["relu_max_value"] is not None
        ):
            self.act = _relu_max(layer_conf["relu_max_value"])
        else:
            self.act = self.supported_act[layer_conf["activation"]]

        self.filters = layer_conf["filters"]
        self.kernel_size = layer_conf["kernel_size"]
        self.dilation_rate = layer_conf["dilation_rate"]
        self.strides = layer_conf["strides"]
        self.freq_padding = layer_conf["freq_padding"]
        assert self.freq_padding in [
            "causal",
            "causal_valid",
        ], "Unsupported padding, must be 'causal', 'causal_valid'"
        if self.freq_padding == "causal":
            self.low_pad = int((np.ceil(self.kernel_size[1] - 1) / 2))
            self.high_pad = int((np.floor(self.kernel_size[1] - 1) / 2))

        self.W = None
        self.b = None
        self.bn_weights = layer_conf["bn_weights"]
        self.set_weights(layer_conf["base_weights"], layer_conf["bn_weights"])
        self.buffer = None

    def _apply_batch_norm(self, base_weights, bn_weights):
        W = base_weights["W"]
        b = base_weights["b"]
        if bn_weights["gamma"] is None:
            # do not apply batch norm
            pass
        else:
            gamma = bn_weights["gamma"]
            beta = bn_weights["beta"]
            mu = bn_weights["mu"]
            sigma = bn_weights["sigma"]
            # NB different fusion w.r.t conv2D
            for i in range(b.shape[0]):
                den = np.sqrt(1e-3 + sigma[i])
                W[..., i, 0] *= gamma[i] / den
                b[i] = gamma[i] * (b[i] - mu[i]) / den + beta[i]
        return W, b

    def compute_frame_np(self, x):
        assert (
            len(x.shape) == 2
        ), "Expected frame with 2 dimensions: frequency, channel."
        if self.buffer is None:
            if self.freq_padding == "causal_valid":
                # no frequency padding
                self.buffer = np.zeros((self.kernel_size[0], x.shape[0], x.shape[1]))
                self.buffer[-1, :, :] = x
            elif self.freq_padding == "causal":
                # add frequency padding
                self.buffer = np.zeros(
                    (
                        int(self.kernel_size[0]),
                        self.low_pad + x.shape[0] + self.high_pad,
                        x.shape[1],
                    )
                )
                self.buffer[-1, self.low_pad : self.low_pad + x.shape[0], :] = x

        else:
            # shift buffer before loading new frame
            new_buffer = np.zeros(self.buffer.shape)
            new_buffer[:-1] = self.buffer[1:]
            self.buffer = new_buffer
            if self.freq_padding == "causal_valid":
                self.buffer[-1, :, :] = x
            elif self.freq_padding == "causal":
                self.buffer[-1, self.low_pad : self.low_pad + x.shape[0], :] = x

        # TODO add assert compatibility W, b, and self.kernel_size, self.filters
        assert self.dilation_rate == (1, 1), "Unsupported dilation rate (temporary)"
        assert self.strides == (1, 1), "Unsupported strides (temporary)"

        in_depth = x.shape[-1]
        out = np.zeros((self.buffer.shape[1] - self.kernel_size[1] + 1, in_depth))

        for chan in range(self.buffer.shape[2]):
            for freq in range(self.buffer.shape[1] - self.kernel_size[1] + 1):
                out[freq, chan] = (
                    np.sum(
                        self.W[:, :, chan, 0]
                        * self.buffer[:, freq : freq + self.kernel_size[1], chan]
                    )
                    + self.b[chan]
                )

        out = self.act(out)
        return out


class Conv2D(Layer):

    default_config = {"layer_conf": None}

    def __init__(self, layer_conf=None, config=None):
        # TODO documentation here
        self.assign_values(config=config, layer_conf=layer_conf)
        super().__init__(config)

        assert layer_conf["name"] == "conv2d", "Invalid configuration for conv2d layer"

        self.ID = layer_conf["ID"]

        if (
            layer_conf["activation"] == "relu"
            and layer_conf["relu_max_value"] is not None
        ):
            self.act = _relu_max(layer_conf["relu_max_value"])
        else:
            self.act = self.supported_act[layer_conf["activation"]]

        self.filters = layer_conf["filters"]
        self.kernel_size = layer_conf["kernel_size"]
        self.dilation_rate = layer_conf["dilation_rate"]
        self.strides = layer_conf["strides"]
        self.freq_padding = layer_conf["freq_padding"]
        assert self.freq_padding in [
            "causal",
            "causal_valid",
        ], "Unsupported padding, must be 'causal', 'causal_valid'"
        if self.freq_padding == "causal":
            self.low_pad = int((np.ceil(self.kernel_size[1] - 1) / 2))
            self.high_pad = int((np.floor(self.kernel_size[1] - 1) / 2))

        self.W = None
        self.b = None
        self.bn_weights = layer_conf["bn_weights"]
        self.set_weights(layer_conf["base_weights"], layer_conf["bn_weights"])

        self.buffer = None

    def compute_frame_np(self, x):
        assert (
            len(x.shape) == 2
        ), "Expected frame with 2 dimensions: frequency, channel."
        if self.buffer is None:
            if self.freq_padding == "causal_valid":
                # no frequency padding
                self.buffer = np.zeros((self.kernel_size[0], x.shape[0], x.shape[1]))
                self.buffer[-1, :, :] = x
            elif self.freq_padding == "causal":
                # add frequency padding
                self.buffer = np.zeros(
                    (
                        int(self.kernel_size[0]),
                        self.low_pad + x.shape[0] + self.high_pad,
                        x.shape[1],
                    )
                )
                self.buffer[-1, self.low_pad : self.low_pad + x.shape[0], :] = x

        else:
            # shift buffer before loading new frame
            new_buffer = np.zeros(self.buffer.shape)
            new_buffer[:-1] = self.buffer[1:]
            self.buffer = new_buffer
            if self.freq_padding == "causal_valid":
                self.buffer[-1, :, :] = x
            elif self.freq_padding == "causal":
                self.buffer[-1, self.low_pad : self.low_pad + x.shape[0], :] = x

        assert self.dilation_rate == (1, 1), "Unsupported dilation rate (temporary)"
        assert self.strides == (1, 1), "Unsupported strides (temporary)"
        out = np.zeros((self.buffer.shape[1] - self.kernel_size[1] + 1, self.filters))
        for chan in range(self.filters):
            k = self.W[:, :, :, chan]
            for freq in range(self.buffer.shape[1] - self.kernel_size[1] + 1):
                out[freq, chan] = (
                    np.sum(k * self.buffer[:, freq : freq + self.kernel_size[1], :])
                    + self.b[chan]
                )
        out = self.act(out)
        return out



class VerticalMaxPooling(Layer):

    default_config = {"layer_conf": None}

    def __init__(self, layer_conf=None, config=None):
        # TODO documentation here
        self.assign_values(config=config, layer_conf=layer_conf)
        super().__init__(config)
        # keeping pool size as a integer here e.g. (1,2) -> 2
        self.pool_size = layer_conf["pool_size"][1]
        self.W = None
        self.b = None
        self.buffer = None

    def _apply_batch_norm(self, base_weights, bn_weights):
        pass

    def set_weights(self, base_weights, bn_weigths):
        pass

    def get_weights(self):
        # this is returning None weights
        out = {"w": self.W, "b": self.b}
        return out

    def to_C(self):
        # this is returning None weights
        out_string = []
        out_string.append(str(self.W))
        out_string.append(str(self.b))
        return out_string

    def compute_frame_np(self, x):
        assert len(x.shape) == 2, "Expected 2 dimensional frame: frequency, channel"
        # no need for a buffer
        out = np.zeros((int(np.floor(x.shape[0] / self.pool_size)), x.shape[1]))
        # NB default valid padding drop the right-most elemnts if the input
        # is not multiple of the pool size (also called stride)
        for pool in range(out.shape[0]):
            out[pool, :] = np.max(
                x[pool * self.pool_size : (pool + 1) * self.pool_size, :], axis=0
            )

        return out


#%%
if __name__ == "__main__":
    import tensorflow as tf
    from tensorflow import keras

    x = np.random.random((1, 1, 14, 5)) * 10 - 5
    x_tf = tf.convert_to_tensor(x)
    mp = keras.layers.MaxPooling2D((1, 2))
    out_tf = mp(x_tf)
    mp_np = VerticalMaxPooling(keras_to_np(mp))
    out_np = mp_np.compute_frame_np(x[0, 0])
    print(out_np)
    print(out_tf[0, 0])
    assert np.allclose(out_np, out_tf[0, 0], rtol=1e-5)


# %%
