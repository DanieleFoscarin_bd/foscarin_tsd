import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks._common import Generic_Processor
import numpy as np
from keras_to_np import *
from layers import (
    Layer,
    Dense,
    Conv2D,
    VerticalPointwise,
    DepthwiseConv2D,
    VerticalMaxPooling,
)


def divide_CBConv(base):
    # TODO doc here
    base_pad = base.padding
    if base.padding == "same":
        base_pad = "causal"
    elif base.padding == "causal_valid":
        base_pad = "causal_valid"

    exp = VerticalPointwise(
        keras_to_np(
            base=base.exp_layer, batch_norm=base.batch_norm1, activation=base.act_layer1
        )
    )
    depth = DepthwiseConv2D(
        keras_to_np(
            base=base.depth_conv,
            batch_norm=base.batch_norm2,
            activation=base.act_layer2,
            padding=base_pad,
        )
    )
    proj = VerticalPointwise(
        keras_to_np(base=base.proj_layer, batch_norm=base.batch_norm3, activation=None)
    )
    residual = base.residual_connection
    # return exp, depth, proj, residual
    return {
        "exp": exp,
        "depth": depth,
        "proj": proj,
        "residual": residual,
    }


class CBConv(Layer):
    default_config = {"sub_layers": None}

    def __init__(self, sub_layers=None, config=None):
        self.assign_values(config=config)
        self.exp = sub_layers["exp"]
        self.depth = sub_layers["depth"]
        self.proj = sub_layers["proj"]
        self.residual = sub_layers["residual"]
        super().__init__(config)

    def compute_frame_np(self, x):
        out_exp = self.exp.compute_frame_np(x)
        out_depth = self.depth.compute_frame_np(out_exp)
        out_proj = self.proj.compute_frame_np(out_depth)
        if self.residual:
            # this only work with the right configuration
            # of causal and number of filters
            # not doing any assert here now
            out = out_proj + x
        else:
            out = out_proj
        return out

    def reset(self):
        self.exp.reset()
        self.depth.reset()
        self.proj.reset()


def divide_CSConv(base):
    # TODO assert control right class as input
    base_pad = base.padding
    if base.padding == "same":
        base_pad = "causal"
    elif base.padding == "causal_valid":
        base_pad = "causal_valid"

    depth = DepthwiseConv2D(
        keras_to_np(
            base=base.depthwise,
            batch_norm=base.batch_norm1,
            activation=base.act_layer1,
            padding=base_pad,
        )
    )
    point = VerticalPointwise(
        keras_to_np(
            base=base.pointwise, batch_norm=base.batch_norm2, activation=base.act_layer2
        )
    )
    return {
        "depth": depth,
        "point": point,
    }


class CSConv(Layer):
    default_config = {"sub_layers": None}

    def __init__(self, sub_layers=None, config=None):
        self.assign_values(config=config)
        self.depth = sub_layers["depth"]
        self.point = sub_layers["point"]
        super().__init__(config)    

    def compute_frame_np(self, x):
        out_depth = self.depth.compute_frame_np(x)
        out_point = self.point.compute_frame_np(out_depth)
        return out_point

    def reset(self):
        self.depth.reset()
        self.point.reset()