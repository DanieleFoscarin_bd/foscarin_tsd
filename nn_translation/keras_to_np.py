#%%
import bd_rd as bd
from numpy.core.defchararray import startswith

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10


def keras_to_np(base, batch_norm=None, activation=None, padding="causal"):
    """Generate a dictionary containing configuration and weights of
    a NN layer starting from a base keras layers, batch normalization
    and activation. To be used as argument for a custom numpy layer.

    Parameters
    ----------
    base : keras.layers.Layer
        Layer to reproduce.
    batch_norm : keras.layers.BatchNormalization, optional
        Batch normalization layer, by default None.
    activation : keras.layers.Activation, optional
        Activation layer, by default None.
    padding : str, optional
        Supported "causal" or "causal_valid", by default "causal".

    Returns
    -------
    dict(str, element)
        Dictionary containing configuration and weights.
    """
    out_config = {}

    base_config = base.get_config()
    base_weights = base.get_weights()
    # base_name = base_config['name']
    base_name = base.__class__.__name__.lower()
    out_config["ID"] = base_config["name"]
    out_config["name"] = base_name

    if batch_norm is not None:
        bn_config = batch_norm.get_config()
        bn_weights = batch_norm.get_weights()
        # assert startswith(
        #     bn_config["name"], "batch_normalization"
        # ), "Invalid input layer in batch_norm"
        assert (
            batch_norm.__class__.__name__.lower() == "batchnormalization"
        ), "Invalid input layer in batch_norm"
        out_config["bn_weights"] = {
            "gamma": bn_weights[0],
            "beta": bn_weights[1],
            "mu": bn_weights[2],
            "sigma": bn_weights[3],
        }
    else:
        out_config["bn_weights"] = {
            "gamma": None,
            "beta": None,
            "mu": None,
            "sigma": None,
        }

    assert padding in [
        "causal",
        "causal_valid",
    ], "Unsupported padding, must be 'causal', 'causal_valid'"
    out_config["freq_padding"] = padding

    if activation is not None:
        act_config = activation.get_config()
        act_type = activation.__class__.__name__.lower()
        if act_type == "activation":
            act_type = act_config["activation"]
        out_config["activation"] = act_type
        out_config["relu_max_value"] = act_config.get("max_value")
    else:
        out_config["activation"] = "linear"
        out_config["relu_max_value"] = None

    if base_name == "conv2d":
        out_config["base_weights"] = {"W": base_weights[0], "b": base_weights[1]}
        out_config["filters"] = base_config["filters"]
        out_config["kernel_size"] = base_config["kernel_size"]
        out_config["strides"] = base_config["strides"]
        out_config["dilation_rate"] = base_config["dilation_rate"]
        # out_config["padding"] = base_config["padding"]

    elif base_name == "conv1d":
        out_config["base_weights"] = {"W": base_weights[0], "b": base_weights[1]}
        out_config["filters"] = base_config["filters"]
        out_config["kernel_size"] = base_config["kernel_size"]
        out_config["strides"] = base_config["strides"]
        out_config["dilation_rate"] = base_config["dilation_rate"]
        # out_config["padding"] = base_config["padding"]

    elif base_name == "depthwiseconv2d":
        out_config["base_weights"] = {"W": base_weights[0], "b": base_weights[1]}
        out_config["filters"] = 1
        out_config["kernel_size"] = base_config["kernel_size"]
        out_config["strides"] = base_config["strides"]
        out_config["dilation_rate"] = base_config["dilation_rate"]

    elif base_name == "dense":
        out_config["base_weights"] = {"W": base_weights[0], "b": base_weights[1]}

    elif base_name == "maxpooling2d":
        assert (
            base_config["data_format"] == "channels_last"
        ), "Unsupported data_format, must be 'channels_last'"
        assert (
            base_config["pool_size"][0] == 1
        ), "Unsupported pool_size, must be 1 in the first dimension"
        assert (
            batch_norm == None and activation == None
        ), "Batch norm and activation must be None for a max pooling layer"
        out_config["pool_size"] = base_config["pool_size"]

    return out_config
