# %%
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(rootrepodir)

import bd_rd as bd

bd.print_version(__file__)
from bd_rd.processing.blocks import STFT

from pf_utils import *
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import numpy as np
from matplotlib import pyplot as plt
import keras_flops
from tqdm import tqdm
from sklearn.metrics import (
    confusion_matrix,
    balanced_accuracy_score,
    accuracy_score,
    matthews_corrcoef,
)
import seaborn as sn
import pandas as pd


import tensorflow as tf
from tensorflow.keras import layers

SR = 16000
SAMPLE_LEN_SEC = 20


def santilli_and_target():
    pass


def plot_confusion_matrix(target, pred, title):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = confusion_matrix(target, pred)
    cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm) / 4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(
        cm.T,
        index=[i for i in ["NT", "NO", "ST", "DT"]],
        columns=[i for i in ["NT", "NO", "ST", "DT"]],
    )
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap="rocket_r", vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure


def plot_cm_ready(cm, title):
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(
        cm.T,
        index=[i for i in ["NT", "NO", "ST", "DT"]],
        columns=[i for i in ["NT", "NO", "ST", "DT"]],
    )
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap="rocket_r", vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

# OBSOLETE
def normalize_cm(cm):
    return cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]


def load_target(dataset_folder, mode):
    scenarios_list = [
        name for name in os.listdir(dataset_folder) if name.startswith(mode)
    ]
    scenarios_list.sort()
    target_l = []
    for i, scenario in enumerate(scenarios_list):
        # remember to ignore the first value of talk_state_npy since its the delay value
        curr_target = np.load(os.path.join(dataset_folder, scenario, "talk_state.npy"))[
            1:
        ]
        target_l.append(curr_target)
    return target_l


def load_baseline(dataset_folder, mode):
    scenarios_list = [
        name for name in os.listdir(dataset_folder) if name.startswith(mode)
    ]
    scenarios_list.sort()
    tbmap_ts_l = []
    for i, scenario in enumerate(scenarios_list):
        curr_tbmap_ts = np.load(os.path.join(dataset_folder, scenario, "tbmap_ts.npy"))
        tbmap_ts_l.append(curr_tbmap_ts)
    return tbmap_ts_l

def features_inference(model, feat):
    """Model inference on a single batch of features

    Parameters
    ----------
    model : keras.Model
        Compiled keras model
    feat : tf.tensor
        Features tensor

    Returns
    -------
    dict(str, ndarray)
        Dictionary, key strings: 'ne','ne_prob', 'fe', 'fe_prob', 'ts', 'ts_prob'
    """
    output = model(feat)
    ne_prob = np.squeeze(output[0].numpy())
    fe_prob = np.squeeze(output[1].numpy())
    ts_prob = np.squeeze(output[2].numpy())
    ts = np.zeros((ts_prob.shape[0], ))
    # for i in range(ts.shape[0]):
    #     ts[i] = np.argmax(ts_class[i])
    ts = np.argmax(ts_prob, axis=-1)
    out = {}
    out['ne_prob'] = ne_prob
    out['fe_prob'] = fe_prob
    out['ts_prob'] = ts_prob
    out['ne'] = np.round(ne_prob)
    out['fe'] = np.round(fe_prob)
    out['ts'] = ts
    return out

def dataset_inference(dataset, model):
    """Compute inference in a talk state labeled dataset

    Parameters
    ----------
    dataset : tf.data.Dataset
        Prepared tf Dataset, with batch size 1 corresponding to a TS scenario
    model : keras.Model
        Compiled keras NN model

    Returns
    -------
    dict(str, list)
        Dictionary, key strings: 'ts', 'ne', 'fe', 'vad_ts', 'target'.
    """
    ts_l = []
    ne_l = []
    fe_l = []
    vad_ts_l = []
    target_l = []
    for feat, target in tqdm(dataset):
        output = model(feat)
        ne_vad = np.squeeze(output[0].numpy())
        fe_vad = np.squeeze(output[1].numpy())
        ts_class = np.squeeze(output[2].numpy())
        ts = np.zeros((ts_class.shape[0],))
        ts = np.argmax(ts_class, axis=1)
        target = target["TS"]
        target = np.squeeze(target.numpy())

        ts_l.append(ts)
        ne_l.append(ne_vad)
        fe_l.append(fe_vad)
        vad_ts_l.append(np.round(ne_vad) + 2 * np.round(fe_vad))
        target_l.append(target)

        out_dict = {}
        out_dict["ts"] = ts_l
        out_dict["ne"] = ne_l
        out_dict["fe"] = fe_l
        out_dict["vad_ts"] = vad_ts_l
        out_dict["target"] = target_l
    return out_dict


config = bd.utils.Class_Dict(
    sr=SR,
    framesize=int(0.01 * SR),
    zero_pad_end=False,
    pre_emphasis=True,
    pre_gain=1,
    window="bd",
    ola_ratio=4,
    max_freq=20000,
    stft_gain=1,
    value_type=bd.T_INT16F,
    dc_removal=True,
)
stft_processor = STFT(config=config)
stft_processor.b_wola.out_idx = 1


def apply_ts(aec_out, ts):
    aec_out[ts == 0.0] *= 0.01
    aec_out[ts == 1.0] *= 1
    aec_out[ts == 2.0] *= 0.01
    aec_out[ts == 3.0] *= 1
    return aec_out


def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i, :] *= ts_red[i]
    return aec_out


def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    ne[ts % 2 == 1] = 1
    fe[ts > 1] = 1
    return ne, fe


def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2 * fe
    return ts_out


def todB(x):
    return 20 * np.log10(x)


def tolin(x):
    return 10 ** (x / 20)


def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []

        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)

        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)

    return ts_reduction


# TS space NET
# ----------------------------------------------
# class Misclassification:
#     def __init__(self):
#         self.correct = 0
#         self.leak = 1
#         self.hd_loss = 1
#         self.dt_leak = 0.2
#         self.fd_loss = 0.2
#         self.over = 0.01
#         self.noise = 0.01
#         self.ok = 0.01
#         self.num_classes = 4
#         self.cat_loss = tf.keras.losses.CategoricalCrossentropy()
#         self.w_loss_matrix = np.array(
#             [
#                 [self.correct, self.hd_loss, self.ok, self.fd_loss],
#                 [self.noise, self.correct, self.leak, self.dt_leak],
#                 [self.ok, self.hd_loss, self.correct, self.fd_loss],
#                 [self.over, self.over, self.leak, self.correct],
#             ]
#         )

#         self.w_loss_matrix = tf.convert_to_tensor(
#             self.w_loss_matrix.T, dtype=tf.float32
#         )

#     def weighted_categorical_cross_entropy(self, target, pred):
#         target = tf.squeeze(target, axis=2)
#         target = tf.one_hot(target, self.num_classes, axis=-1)
#         w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

#         # weighted categorical cross entropy
#         loss = -tf.math.reduce_sum((1 - w_loss) * target * tf.math.log(pred + 1e-9)) / (
#             target.shape[1]
#         )
#         return loss

#     def weighted_binary_cross_entropy(self, target, pred):
#         target = tf.squeeze(target, axis=2)
#         target = tf.one_hot(target, self.num_classes, axis=-1)
#         w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

#         # weighted binary cross entropy
#         loss = -tf.math.reduce_sum(
#             (1 - w_loss) * target * tf.math.log(pred + 1e-9)
#             + w_loss * (1 - target) * tf.math.log(1 - pred + 1e-9)
#         ) / (2 * target.shape[1])
#         return loss


def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer="Adam"):

    # misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands / 8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2 ** (-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input

    x = CausalBottleneck(
        int(expansion * in_f),
        int(in_f),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)
    x = CausalBottleneck(
        int(expansion * in_f),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)

    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 4 * wm),
        np.max([int(4 * wm), in_f]),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    x = CausalBottleneck(
        int(expansion * in_f),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(x)
    x = layers.MaxPool2D((1, 2))(x)

    x = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(ne)
    ne = layers.MaxPool2D((1, 2))(ne)

    ne = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(ne)
    ne = CausalConv2D(
        filters=int(32 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(ne)
    ne = tf.squeeze(ne, axis=2)

    ne = layers.Conv1D(int(32 * wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(
        1,
        1,
        activation="sigmoid",
    )(ne)
    ne = layers.Layer(name="NE")(ne)

    # FAR END branch
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(8 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalBottleneck(
        int(expansion * 8 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=False,
    )(fe)
    fe = layers.MaxPool2D((1, 2))(fe)

    fe = CausalBottleneck(
        int(expansion * 16 * wm),
        int(16 * wm),
        kernel_size=(kernel_depth, 3),
        relu_max_value=MAX,
        residual_connection=True,
    )(fe)
    fe = CausalConv2D(
        filters=int(16 * wm),
        kernel_size=(int(np.ceil(kernel_depth / 2)), reduce_last),
        padding="causal_valid",
    )(fe)
    fe = tf.squeeze(fe, axis=2)

    fe = layers.Conv1D(int(16 * wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation="sigmoid")(fe)
    fe = layers.Layer(name="FE")(fe)

    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(
        4,
        1,
    )(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name="TS")(ts)

    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    # if optimizer == "Adam":
    #     model.compile(
    #         optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    #         loss={
    #             "NE": tf.keras.losses.binary_crossentropy,
    #             "FE": tf.keras.losses.binary_crossentropy,
    #             "TS": misclass.weighted_binary_cross_entropy,
    #         },
    #         metrics=["accuracy"],
    #     )
    # elif optimizer == "RMSProp":
    #     model.compile(
    #         optimizer=tf.keras.optimizers.RMSProp(learning_rate=0.001),
    #         loss={
    #             "NE": tf.keras.losses.binary_crossentropy,
    #             "FE": tf.keras.losses.binary_crossentropy,
    #             "TS": misclass.weighted_binary_cross_entropy,
    #         },
    #         metrics=["accuracy"],
    #     )

    return model


# %%
print("load model weigths...")
model_trained = make_tsMNet(
    input_bands=48,
    wm=0.65,
    expansion=2,
    kernel_depth=7,
)
trained_filepath = (
    # "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/lin_20211102-175516"
    "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/lin_20211116-141429"
)
print(
    "Flops per frame: ", "{:e}".format(keras_flops.get_flops(model_trained, 1) / 2000)
)
model_trained.load_weights(os.path.join(trained_filepath, "best_model"))


def prepare_test_set(test_set_folder):
    test_scenarios_list = [
        name for name in os.listdir(test_set_folder) if name.startswith("test")
    ]
    test_scenarios_list.sort()

    auto = tf.data.AUTOTUNE
    lin_bands_freq = np.linspace(150, 8000, 48, endpoint=False)
    batch_size = 1

    test_list = [os.path.join(test_set_folder, name) for name in test_scenarios_list]
    test_dataset = tf.data.Dataset.from_tensor_slices(test_list)
    test_dataset = test_dataset.prefetch(auto)

    test_preprocessor_lin = Pipeline_Preprocessor(
        SR,
        SAMPLE_LEN_SEC,
        custom_bands=lin_bands_freq,
        target_type="dv_ts",
        augmentation=False,
    )
    test_dataset = test_dataset.map(
        test_preprocessor_lin.tf_get_features_from_bd, num_parallel_calls=auto
    )
    test_dataset = test_dataset.batch(batch_size)

    return test_dataset

# %%
# test_folder = "/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR"
test_folder = "/home/disks/ssdata/danielefoscarin/test_sets/devices_test_sets/minispk3/-12dB_speaker/-6_noise"
test_dataset = prepare_test_set(test_folder)
# TODO refactor dataset inference in dictionary
inf_dict = dataset_inference(test_dataset, model_trained)

cm = confusion_matrix(
    np.concatenate(inf_dict["target"]), 
    np.concatenate(inf_dict["ts"]),
    normalize='true',
)
fig = plot_cm_ready(cm, "title")
fig.show()
print("Balanced accuracy:\n    ",balanced_accuracy_score(np.concatenate(inf_dict['target']), np.concatenate(inf_dict["ts"])))

# %%
# val dataset inference cm
auto = tf.data.AUTOTUNE
dataset_folder = "/home/danielefoscarin/LOCAL/SSD/generated/dataset_1029"
val_scenarios_list = [
    name for name in os.listdir(dataset_folder) if name.startswith("val")
]
val_scenarios_list.sort()
val_list = [os.path.join(dataset_folder, name) for name in val_scenarios_list]
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
val_preprocessor = Pipeline_Preprocessor(
    SR,
    SAMPLE_LEN_SEC,
    custom_bands=bands_freq,
    target_type="dv_ts",
    augmentation=False,
)

val_dataset = tf.data.Dataset.from_tensor_slices(val_list)
val_dataset = val_dataset.map(
    val_preprocessor.tf_get_features_from_bd, num_parallel_calls=auto
)

batch_size = 1
val_dataset = val_dataset.batch(batch_size)
val_dataset = val_dataset.prefetch(auto)

print("starting inference...")
val_dict = dataset_inference(val_dataset, model_trained)
cm = confusion_matrix(
    np.concatenate(val_dict["target"]), 
    np.concatenate(val_dict["ts"]),
    normalize='true',
)
fig = plot_cm_ready(cm, "val dataset")
fig.show()

# %% audio inference
bin_folder = '/home/danielefoscarin/LOCAL/SSD/bin'
sox_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_sox'
tbmap_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_tbmap'

sin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/sin.wav'
rin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/rin.wav'
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
inference_preprocessor = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, augmentation=False)
features, aec_out = inference_preprocessor.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder, mul=tolin(20))
print("preprocessing done...")
# %%
inf_dict = features_inference(model_trained, tf.expand_dims(features, axis=0))
print("inference done...")
# %%
post_aec_stft = apply_ts_reduction(aec_out.numpy(), ts_conditional_smoothing(inf_dict['ts'], slow_len=6))

post_aec = stft_processor.invert_np(post_aec_stft)
aec_out_audio = stft_processor.invert_np(aec_out)
bd.io.write_wav("post_aec2.wav", post_aec, SR) 
bd.io.write_wav("aec_out.wav", aec_out_audio, SR) 

# %%
