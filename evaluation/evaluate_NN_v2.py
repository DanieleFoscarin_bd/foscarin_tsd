# %% 
# libraries
import bd_rd as bd
from seaborn.distributions import kdeplot
# bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks import STFT
from bd_rd.processing.common import get_bd_window

from scenario_generator import *  
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import keras.backend as bk


import numpy as np
from matplotlib import pyplot as plt
import os
from icecream import ic
import keras_flops
from tqdm import tqdm
import io
import joblib
import datetime
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, accuracy_score, matthews_corrcoef
import seaborn as sn
import pandas as pd


import tensorflow as tf
from tensorflow.keras import layers


SR = 16000
SAMPLE_LEN_SEC = 20

SAVE_FIG = False



# %% 
# utility functions
def santilli_and_target():
    pass

def plot_confusion_matrix(target, pred, title):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = confusion_matrix(target, pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm)/4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

def plot_cm_ready(cm, title):
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

def normalize_cm(cm):
    return cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]




def load_target(dataset_folder, mode):
    scenarios_list = [name for name in os.listdir(dataset_folder) if name.startswith(mode)]
    scenarios_list.sort()
    target_l = []
    for i, scenario in enumerate(scenarios_list):
        #remember to ignore the first value of talk_state_npy since its the delay value
        curr_target = np.load(os.path.join(dataset_folder, scenario, 'talk_state.npy'))[1:]
        target_l.append(curr_target)
    return target_l

def load_baseline(dataset_folder, mode):
    scenarios_list = [name for name in os.listdir(dataset_folder) if name.startswith(mode)]
    scenarios_list.sort()
    tbmap_ts_l = []
    for i, scenario in enumerate(scenarios_list):
        curr_tbmap_ts = np.load(os.path.join(dataset_folder, scenario, 'tbmap_ts.npy'))
        tbmap_ts_l.append(curr_tbmap_ts)
    return tbmap_ts_l

def dataset_inference(dataset, model):
    ts_l = []
    ne_l = []
    fe_l = []
    vad_ts_l = []
    target_l = []
    for feat, target in tqdm(dataset): 
        output = model(feat)
        ne_vad = np.squeeze(output[0].numpy())
        fe_vad = np.squeeze(output[1].numpy())
        ts_class = np.squeeze(output[2].numpy())
        ts = np.zeros((ts_class.shape[0], ))
        ts = np.argmax(ts_class, axis=1)
        target = target['TS']
        target = np.squeeze(target.numpy())

        ts_l.append(ts)
        ne_l.append(ne_vad)
        fe_l.append(fe_vad)
        vad_ts_l.append(np.round(ne_vad) + 2*np.round(fe_vad))

        target_l.append(target)

    return ne_l, fe_l, ts_l, vad_ts_l, target_l

config = bd.utils.Class_Dict(sr=SR, framesize=int(0.01*SR), zero_pad_end=False,
                    pre_emphasis=True,
                    pre_gain=1, window="bd", ola_ratio=4, max_freq=20000,
                    stft_gain=1, value_type=bd.T_INT16F, dc_removal=True)
stft_processor = STFT(config=config)
stft_processor.b_wola.out_idx = 1

def apply_ts(aec_out, ts):
    aec_out[ts==0.0] *= 0.01
    aec_out[ts==1.0] *= 1
    aec_out[ts==2.0] *= 0.01
    aec_out[ts==3.0] *= 1
    return aec_out

def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i,:] *= ts_red[i]
    return aec_out

def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    ne[ts%2==1] = 1
    fe[ts>1] = 1
    return ne, fe

def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2*fe
    return ts_out

def todB(x):
    return 20*np.log10(x)

def tolin(x):
    return 10**(x/20)

def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []
        
        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)
        
        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)
        
    return ts_reduction

# %%
# PREPARE NN MODELS
# ----------------------------------------------
class Misclassification():

    def __init__(self):
        self.correct = 0
        self.leak = 1
        self.hd_loss = 1
        self.fd_loss = 0.2
        self.over = 0.01
        self.noise = 0.01
        self.ok = 0.01
        self.num_classes = 4
        self.cat_loss = tf.keras.losses.CategoricalCrossentropy()
        self.w_loss_matrix = np.array([[self.correct, self.hd_loss, self.ok, self.fd_loss],
                                    [self.noise, self.correct, self.leak, self.leak],
                                    [self.ok, self.hd_loss, self.correct, self.fd_loss],
                                    [self.over, self.over, self.leak, self.correct]])

        self.w_loss_matrix = tf.convert_to_tensor(self.w_loss_matrix.T, dtype=tf.float32)

    def weighted_categorical_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)        
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted categorical cross entropy
        loss = -tf.math.reduce_sum( (1-w_loss) * target * tf.math.log(pred+1e-9))/(target.shape[1])
        return loss

    def weighted_binary_cross_entropy(self, target, pred):
        target = tf.squeeze(target, axis=2)
        target = tf.one_hot(target, self.num_classes, axis=-1)        
        w_loss = tf.linalg.matmul(target, self.w_loss_matrix)

        # weighted binary cross entropy
        loss = -tf.math.reduce_sum( (1-w_loss) * target * tf.math.log(pred+1e-9) + w_loss * (1-target) * tf.math.log(1-pred+1e-9))/(2*target.shape[1])
        return loss

def make_tsMNet(input_bands, wm=1, expansion=6, kernel_depth=8, optimizer='Adam'):

    misclass = Misclassification()
    input_bands = int(input_bands)
    reduce_last = int(np.floor(input_bands/8))

    # input_shape = (SAMPLE_LEN_SEC*100, input_bands, 3)
    input_shape = (None, input_bands, 3)

    MAX = 8 - 2**(-4)
    input = layers.Input(input_shape)
    in_f = input_shape[-1]
    x = input


    x = CausalBottleneck(int(expansion*in_f), int(in_f), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)
    x = CausalBottleneck(int(expansion*in_f), np.max([int(4*wm), in_f]), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(x)
    
    x = layers.MaxPool2D((1,2))(x)

    x = CausalBottleneck(int(expansion*4*wm), np.max([int(4*wm), in_f]), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)
   
    x = CausalBottleneck(int(expansion*in_f), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(x)
    x = layers.MaxPool2D((1,2))(x)

    x = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(x)

    ne = x
    fe = x

    # NEAR END branch
    ne = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    ne = CausalBottleneck(int(expansion*8*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(ne)
    ne = layers.MaxPool2D((1,2))(ne)


    ne = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(ne)
    ne = CausalConv2D(filters=int(32*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid')(ne)
    ne = tf.squeeze(ne, axis=2)
    
    ne = layers.Conv1D(int(32*wm), 1)(ne)
    ne = layers.BatchNormalization()(ne)
    ne = layers.ReLU(MAX)(ne)
    ne = layers.Conv1D(1, 1, activation='sigmoid',)(ne)
    ne = layers.Layer(name='NE')(ne)

    # FAR END branch
    fe = CausalBottleneck(int(expansion*8*wm), int(8*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(fe)
    fe = CausalBottleneck(int(expansion*8*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=False)(fe)
    fe = layers.MaxPool2D((1,2))(fe)


    fe = CausalBottleneck(int(expansion*16*wm), int(16*wm), kernel_size=(kernel_depth,3), relu_max_value=MAX, residual_connection=True)(fe)
    fe = CausalConv2D(filters=int(16*wm), kernel_size=(int(np.ceil(kernel_depth/2)), reduce_last), padding='causal_valid')(fe)
    fe = tf.squeeze(fe, axis=2)
    
    fe = layers.Conv1D(int(16*wm), 1)(fe)
    fe = layers.BatchNormalization()(fe)
    fe = layers.ReLU(MAX)(fe)
    fe = layers.Conv1D(1, 1, activation='sigmoid')(fe)
    fe = layers.Layer(name='FE')(fe)


    # TALK STATE space
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(4, 1,)(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name='TS')(ts)


    model = tf.keras.Model(inputs=input, outputs=[ne, fe, ts])

    if optimizer == 'Adam':
        model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), 
        loss = {"NE": tf.keras.losses.binary_crossentropy,
                    "FE": tf.keras.losses.binary_crossentropy,
                    "TS": misclass.weighted_binary_cross_entropy
                    }, 
        metrics=['accuracy'])
    elif optimizer == 'RMSProp':
        model.compile(optimizer=tf.keras.optimizers.RMSProp(learning_rate=0.001), 
        loss = {"NE": tf.keras.losses.binary_crossentropy,
                    "FE": tf.keras.losses.binary_crossentropy,
                    "TS": misclass.weighted_binary_cross_entropy
                    }, 
        metrics=['accuracy'])

    return model

# NB need to reinstantiate the model with None length if seq len != 20 seconds
print('load best...')
model_best = make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
best_filepath = '/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/lin_20210908-181650/'
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model_best, 1)/2000))
model_best.load_weights(best_filepath+'best_model')

print('load bark...')
model_bark = make_tsMNet(input_bands= 48, wm=0.65, expansion=2, kernel_depth=7)
bark_filepath = '/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/bark_20210908-181737/'
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model_bark, 1)/2000))
model_bark.load_weights(bark_filepath+'best_model')

print('load big...')
model_big = make_tsMNet(input_bands=96, wm=1.5, expansion=5, kernel_depth=12)
big_filepath = '/home/disks/sdb/danielefoscarin/model_checkpoint/ts_space/big_lin_20210908-181911/'
print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model_big, 1)/2000))
model_big.load_weights(big_filepath+'best_model')


# model.summary()
# print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)/2000))
# print("Flops per frame: ","{:e}".format(keras_flops.get_flops(model, 1)))

# %% 
# COMPARISON WITH TBMAP TALK STATE DETECTOR
#------------------------------------------
test_set_folder = '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR'
#------------------------------------------

def prepare_datasets(test_set_folder):
    test_scenarios_list = [name for name in os.listdir(test_set_folder) if name.startswith('test')]
    test_scenarios_list.sort()

    auto = tf.data.AUTOTUNE
    lin_bands_freq = np.linspace(150, 8000, 48, endpoint=False)
    batch_size = 1

    test_list = [os.path.join(test_set_folder, name) for name in test_scenarios_list]
    test_dataset = tf.data.Dataset.from_tensor_slices(test_list)
    test_dataset = test_dataset.prefetch(auto)

    test_preprocessor_lin = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=lin_bands_freq, target_type='dv_ts', augmentation=False)
    test_dataset_best = test_dataset.map(test_preprocessor_lin.tf_get_features_from_bd,
                                num_parallel_calls=auto)
    test_dataset_best = test_dataset_best.batch(batch_size)

    test_preprocessor_bark = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, target_type='dv_ts', augmentation=False)
    test_dataset_bark = test_dataset.map(test_preprocessor_bark.tf_get_features_from_bd,
                                num_parallel_calls=auto)
    test_dataset_bark = test_dataset_bark.batch(batch_size)

    linear_bands_96 = np.linspace(150, 8000, 96, endpoint=False)
    test_preprocessor_big = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=linear_bands_96, target_type='dv_ts', augmentation=False)
    test_dataset_big = test_dataset.map(test_preprocessor_big.tf_get_features_from_bd,
                                num_parallel_calls=auto)
    test_dataset_big = test_dataset_big.batch(batch_size)

    return test_dataset_best, test_dataset_bark, test_dataset_big

# test_dataset_best, test_dataset_bark, test_dataset_big = prepare_datasets(test_set_folder)
# %%
test_sets_l = [
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_48_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_24_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_6_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_clean',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisy',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisier',
    ]

name_l = ['CA_Opt', 'CA_Bark', 'L_Opt', 'baseline']

write_folder = '/home/disks/ssdata/danielefoscarin/small_data/test_inference/'
# %%
# write model inference into disk npy
SAVE_INF = False
if SAVE_INF:
    for test_set_folder in test_sets_l:
        print(f'Inference {os.path.basename(test_set_folder)}...')
        test_dataset_best, test_dataset_bark, test_dataset_big = prepare_datasets(test_set_folder)
        ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_best, model_best)
        ca_opt_out = np.array([ne_l, fe_l, ts_l, vad_ts_l, target_l], dtype=object)
        np.save(write_folder+os.path.basename(test_set_folder)+'_CA_Opt.npy', ca_opt_out )

        ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_bark, model_bark)
        ca_bark_out = np.array([ne_l, fe_l, ts_l, vad_ts_l, target_l], dtype=object)
        np.save(write_folder+os.path.basename(test_set_folder)+'_CA_Bark.npy', ca_bark_out )

        ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_big, model_big)
        l_opt_out = np.array([ne_l, fe_l, ts_l, vad_ts_l, target_l], dtype=object)
        np.save(write_folder+os.path.basename(test_set_folder)+'_L_Opt.npy', l_opt_out )

    # write baseline inference into disk npy with the same format
    target_48 = np.array(load_target(test_sets_l[0], 'test'))
    target_24 = np.array(load_target(test_sets_l[1], 'test'))
    target_12 = np.array(load_target(test_sets_l[2], 'test'))
    target_6 = np.array(load_target(test_sets_l[3], 'test'))
    target_clean = np.array(load_target(test_sets_l[4], 'test'))
    target_noisy = np.array(load_target(test_sets_l[5], 'test'))
    target_noisier = np.array(load_target(test_sets_l[6], 'test'))
    ts_48 = np.array(load_baseline(test_sets_l[0], 'test'))
    ts_24 = np.array(load_baseline(test_sets_l[1], 'test'))
    ts_12 = np.array(load_baseline(test_sets_l[2], 'test'))
    ts_6 = np.array(load_baseline(test_sets_l[3], 'test'))
    ts_clean = np.array(load_baseline(test_sets_l[4], 'test'))
    ts_noisy = np.array(load_baseline(test_sets_l[5], 'test'))
    ts_noisier = np.array(load_baseline(test_sets_l[6], 'test'))
    print('load ok...')

    # dummy array for compatibility 
    dum = np.zeros(ts_48.shape)
    np.save(write_folder+'synthetic_48_SNR_baseline', np.array([dum,dum,ts_48,dum,target_48], dtype=object))
    np.save(write_folder+'synthetic_24_SNR_baseline', np.array([dum,dum,ts_24,dum,target_24], dtype=object))
    np.save(write_folder+'synthetic_12_SNR_baseline', np.array([dum,dum,ts_12,dum,target_12], dtype=object))
    np.save(write_folder+'synthetic_6_SNR_baseline', np.array([dum,dum,ts_6,dum,target_6], dtype=object))
    np.save(write_folder+'rec_clean_baseline', np.array([dum,dum,ts_clean,dum,target_clean], dtype=object))
    np.save(write_folder+'rec_noisy_baseline', np.array([dum,dum,ts_noisy,dum,target_noisy], dtype=object))
    np.save(write_folder+'rec_noisier_baseline', np.array([dum,dum,ts_noisier,dum,target_noisier], dtype=object))





# %%
# TARGET AND BASELINE LOADING

# target_l = load_target(test_set_folder, 'test')
# tbmap_ts_l = load_baseline(test_set_folder, 'test')

# count_class = np.zeros((4,))
# tt = np.concatenate(target_l)
# for i in range(tt.shape[0]):
#     count_class[int(tt[i])] += 1
# count_class /= tt.shape[0]
# plt.figure()
# plt.title('Target classes distribution')
# # plt.grid(linestyle=':', zorder=-1)
# plt.bar(range(4), count_class)
# plt.ylabel('class frequency')
# plt.xticks(range(0,4), labels=['no talk','near only','single talk','double talk'])
# plt.grid(linestyle=":")
# if SAVE_FIG:
#    plt.savefig('writing/fig_scripts/'+os.path.basename(test_set_folder)+'_classes_dist.pdf') 
# plt.show()


# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(tbmap_ts_l), "baseline")
# if SAVE_FIG:
#    fig.savefig('writing/fig_scripts/'+os.path.basename(test_set_folder)+'_baseline_cm.pdf') 
# fig.show()
# print('accuracy ', accuracy_score(np.concatenate(target_l), np.concatenate(tbmap_ts_l)))
# print('balanced accuracy ', balanced_accuracy_score(np.concatenate(target_l), np.concatenate(tbmap_ts_l)))
# print('matthews corrcoeff ', matthews_corrcoef(np.concatenate(target_l), np.concatenate(tbmap_ts_l)))

# # %%
# # CA_OPT MODEL inference
# ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_best, model_best)
# best_ts_l = ts_l.copy()
# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(vad_ts_l), "2 VAD TSD")
# fig.show()

# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(ts_l), "CA_Opt")
# if SAVE_FIG:
#    fig.savefig('writing/fig_scripts/'+os.path.basename(test_set_folder)+'_CA_Opt_cm.pdf') 
# fig.show()

# print('accuracy ', accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
# print('balanced accuracy ', balanced_accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
# print('matthews corrcoeff ', matthews_corrcoef(np.concatenate(target_l), np.concatenate(ts_l)))

# # %%
# # CA_BARK MODEL inference
# # print('bark model test...')
# ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_bark, model_bark)
# bark_ts_l = ts_l.copy()
# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(vad_ts_l), "2 VAD TSD")
# fig.show()

# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(ts_l), "CA_Bark")
# if SAVE_FIG:
#    fig.savefig('writing/fig_scripts/'+os.path.basename(test_set_folder)+'_CA_Bark_cm.pdf') 
# fig.show()

# print('accuracy ', accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
# print('balanced accuracy ', balanced_accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
# print('matthews corrcoeff ', matthews_corrcoef(np.concatenate(target_l), np.concatenate(ts_l)))

# %% 
# big cm 
test_sets_l = [
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_48_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_24_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_6_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_clean',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisy',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisier',
    ]


def make_total_cm(m_name, write_folder):
    s48 = np.load(write_folder+'synthetic_48_SNR_'+m_name+'.npy', allow_pickle=True)
    s24 = np.load(write_folder+'synthetic_24_SNR_'+m_name+'.npy', allow_pickle=True)
    s12 = np.load(write_folder+'synthetic_12_SNR_'+m_name+'.npy', allow_pickle=True)
    s6 = np.load(write_folder+'synthetic_6_SNR_'+m_name+'.npy', allow_pickle=True)
    r_clean = np.load(write_folder+'rec_clean_'+m_name+'.npy', allow_pickle=True)
    r_noisy = np.load(write_folder+'rec_noisy_'+m_name+'.npy', allow_pickle=True)
    r_noisier = np.load(write_folder+'rec_noisier_'+m_name+'.npy', allow_pickle=True)


    cm48 = normalize_cm(confusion_matrix(np.concatenate(s48[4].tolist()), np.concatenate(s48[2].tolist())))
    cm24 = normalize_cm(confusion_matrix(np.concatenate(s24[4].tolist()), np.concatenate(s24[2].tolist())))
    cm12 = normalize_cm(confusion_matrix(np.concatenate(s12[4].tolist()), np.concatenate(s12[2].tolist())))
    cm6 = normalize_cm(confusion_matrix(np.concatenate(s6[4].tolist()), np.concatenate(s6[2].tolist())))

    cm_s = (cm48+cm24+cm12+cm6) /4

    cm_clean = normalize_cm(confusion_matrix(np.concatenate(r_clean[4].tolist()), np.concatenate(r_clean[2].tolist())))
    cm_noisy = normalize_cm(confusion_matrix(np.concatenate(r_noisy[4].tolist()), np.concatenate(r_noisy[2].tolist())))
    cm_noisier = normalize_cm(confusion_matrix(np.concatenate(r_noisier[4].tolist()), np.concatenate(r_noisier[2].tolist())))

    cm_r = (cm_clean+cm_noisy+cm_noisier) /3

    return cm_s, cm_r

name = 'CA_Opt'
cm_s, cm_r = make_total_cm(name, write_folder)
fig = plot_cm_ready(cm_r, name+' Videocall test sets')
fig.savefig('writing/new_fig/'+name+'_videocall.pdf')
fig.show()
fig = plot_cm_ready(cm_s, name+' Synthetic test sets')
fig.savefig('writing/new_fig/'+name+'_synthetic.pdf')
fig.show()

name = 'CA_Bark'
cm_s, cm_r = make_total_cm(name, write_folder)
fig = plot_cm_ready(cm_r, name+' Videocall test sets')
fig.savefig('writing/new_fig/'+name+'_videocall.pdf')
fig.show()
fig = plot_cm_ready(cm_s, name+' Synthetic test sets')
fig.savefig('writing/new_fig/'+name+'_synthetic.pdf')
fig.show()

name = 'L_Opt'
cm_s, cm_r = make_total_cm(name, write_folder)
fig = plot_cm_ready(cm_r, name+' Videocall test sets')
fig.savefig('writing/new_fig/'+name+'_videocall.pdf')
fig.show()
fig = plot_cm_ready(cm_s, name+' Synthetic test sets')
fig.savefig('writing/new_fig/'+name+'_synthetic.pdf')
fig.show()


target_48 = load_target(test_sets_l[0], 'test')
target_24 = load_target(test_sets_l[1], 'test')
target_12 = load_target(test_sets_l[2], 'test')
target_6 = load_target(test_sets_l[3], 'test')
target_clean = load_target(test_sets_l[4], 'test')
target_noisy = load_target(test_sets_l[5], 'test')
target_noisier = load_target(test_sets_l[6], 'test')
ts_48 = load_baseline(test_sets_l[0], 'test')
ts_24 = load_baseline(test_sets_l[1], 'test')
ts_12 = load_baseline(test_sets_l[2], 'test')
ts_6 = load_baseline(test_sets_l[3], 'test')
ts_clean = load_baseline(test_sets_l[4], 'test')
ts_noisy = load_baseline(test_sets_l[5], 'test')
ts_noisier = load_baseline(test_sets_l[6], 'test')

cm48 = normalize_cm(confusion_matrix(np.concatenate(target_48), np.concatenate(ts_48)))
cm24 = normalize_cm(confusion_matrix(np.concatenate(target_24), np.concatenate(ts_24)))
cm12 = normalize_cm(confusion_matrix(np.concatenate(target_12), np.concatenate(ts_12)))
cm6 = normalize_cm(confusion_matrix(np.concatenate(target_6), np.concatenate(ts_6)))
cm_s = (cm48+cm24+cm12+cm6) /4

cm_clean = normalize_cm(confusion_matrix(np.concatenate(target_clean), np.concatenate(ts_clean)))
cm_noisy = normalize_cm(confusion_matrix(np.concatenate(target_noisy), np.concatenate(ts_noisy)))
cm_noisier = normalize_cm(confusion_matrix(np.concatenate(target_noisier), np.concatenate(ts_noisier)))
cm_r = (cm_clean+cm_noisy+cm_noisier) /3

fig = plot_cm_ready(cm_r, 'Baseline Videocall test sets')
fig.savefig('writing/new_fig/baseline_videocall.pdf')
fig.show()
fig = plot_cm_ready(cm_s, 'Baseline Synthetic test sets')
fig.savefig('writing/new_fig/baseline_synthetic.pdf')
fig.show()


# %%
# L_OPT MODEL inference
# print('big model test...')
ne_l, fe_l, ts_l, vad_ts_l, target_l = dataset_inference(test_dataset_big, model_big)
big_ts_l = ts_l.copy()
# fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(vad_ts_l), "2 VAD TSD")
# fig.show()

fig = plot_confusion_matrix(np.concatenate(target_l), np.concatenate(ts_l), "L_Opt")
if SAVE_FIG:
   fig.savefig('writing/fig_scripts/'+os.path.basename(test_set_folder)+'_L_Opt_cm.pdf') 
fig.show()

print('accuracy ', accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
print('balanced accuracy ', balanced_accuracy_score(np.concatenate(target_l), np.concatenate(ts_l)))
print('matthews corrcoeff ', matthews_corrcoef(np.concatenate(target_l), np.concatenate(ts_l)))

# %%
# balanced accuracy analysis
def populate_ba_l(ba_l, ts_l, target_l, name):
    for i in tqdm(range(len(ts_l))):
        curr_ba = balanced_accuracy_score(target_l[i], ts_l[i])
        ba_l.append([name, curr_ba])
    return ba_l

def populate_ba_l_multiset(ba_l, test_sets_l, folder, name_l):
    # load precomputed inference outputs
    # inf_out = []
    for m_name in name_l:
        for test_set in test_sets_l:
            test_set_name = os.path.basename(test_set)
            curr_inf = np.load(folder+test_set_name+'_'+m_name+'.npy', allow_pickle=True)
            curr_target = curr_inf[4]
            curr_ts = curr_inf[2]
            for scen in range(curr_target.shape[0]):
                curr_ba = balanced_accuracy_score(curr_target[scen].tolist(), curr_ts[scen].tolist())
                ba_l.append([m_name, test_set_name, curr_ba])
    return ba_l

# TODO change to fancy names
fancy_names = ['synthetic_48_SNR', 'synthetic_24_SNR', 'synthetic_12_SNR', 'synthetic_6_SNR', 'videocall_quiet', 'videocall_noisy', 'videocall_harsh']
ba_l = []
ba_l = populate_ba_l_multiset(ba_l, test_sets_l, write_folder, name_l)            

# %%

def populate_er_l_multiset(er_l, test_sets_l, folder, name_l):
    for m_name in name_l:
        for test_set in test_sets_l:
            test_set_name = os.path.basename(test_set)
            curr_inf = np.load(folder+test_set_name+'_'+m_name+'.npy', allow_pickle=True)
            curr_target = curr_inf[4]
            curr_ts = curr_inf[2]
            for scen in range(curr_target.shape[0]):
                cm = confusion_matrix(curr_target[scen].tolist(), curr_ts[scen].tolist()).T
                # cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
                st = np.sum(cm[:,2])
                no = np.sum(cm[:,1])
                dt = np.sum(cm[:,3])
                e_leak = cm[1,2] + cm[3,2] 
                ne_loss = cm[0,1] + cm[2,1]
                fd_loss = cm[0,3] + cm[2,3]
                e_leak /= st
                ne_loss /= no
                fd_loss /= dt
                # er_l.append([m_name, test_set_name, st, no, dt, e_leak, ne_loss, fd_loss])
                er_l.append([m_name, test_set_name, e_leak, ne_loss, fd_loss])
    return er_l

er_l = []
er_l = populate_er_l_multiset(er_l, test_sets_l, write_folder, name_l)

# %%
# https://stackoverflow.com/questions/3899980/how-to-change-the-font-size-on-a-matplotlib-plot
# bd.plots.set_style('seaborn-poster')
bd.plots.set_style('default')
plt.rc('xtick', labelsize=12)    # fontsize of the tick labels
plt.rc('ytick', labelsize=12)
plt.rc('axes', labelsize=18)
plt.rc('legend', fontsize=12)

hue_order=['L_Opt','CA_Opt','CA_Bark','baseline']

ba_synthetic_l = []
ba_synthetic_l = populate_ba_l_multiset(ba_synthetic_l, test_sets_l[0:4], write_folder, name_l)

fig, ax = plt.subplots(1,1, figsize=(5,10))
# ax.set_title('Synthetic test sets')
ba_synthetic = pd.DataFrame(ba_synthetic_l, columns=['predictor', 'test_set', 'balanced_accuracy'])
sn.violinplot(x='balanced_accuracy', y='test_set', data=ba_synthetic, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=ax)
ax.grid(linestyle=':')
ax.set_ylabel('')
ax.set_yticklabels(['48 SNR','24 SNR','12 SNR','6 SNR'])
ax.set_xlim([0,1])
fig.tight_layout()
fig.savefig('writing/new_fig/ba_synthetic.pdf')
fig.show()

ba_videocall_l = []
ba_videocall_l = populate_ba_l_multiset(ba_videocall_l, test_sets_l[4:], write_folder, name_l)

fig, ax = plt.subplots(1,1, figsize=(5,10))
# ax.set_title('Videocall test sets')
ba_videocall = pd.DataFrame(ba_videocall_l, columns=['predictor', 'test_set', 'balanced_accuracy'])
sn.violinplot(x='balanced_accuracy', y='test_set', data=ba_videocall, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=ax)
ax.grid(linestyle=':')
ax.set_ylabel('')
ax.set_yticklabels(['quiet','noisy','harsh'])
ax.set_xlim([0,1])
fig.tight_layout()
fig.savefig('writing/new_fig/ba_videocall.pdf')
fig.show()


# %%
er_synthetic_l = []
er_synthetic_l = populate_er_l_multiset(er_synthetic_l, test_sets_l[0:4], write_folder, name_l)

fig, axes = plt.subplots(1,3, figsize=(15,10))
er_synthetic = pd.DataFrame(er_synthetic_l, columns=['predictor', 'test_set', 'echo_leak recall', 'NE_loss recall', 'FD_loss recall'])

sn.violinplot(x='echo_leak recall', y='test_set', data=er_synthetic, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=axes[0])
axes[0].grid(linestyle=':')
axes[0].set_ylabel('')
axes[0].set_yticklabels(['48 SNR','24 SNR','12 SNR','6 SNR'])
axes[0].set_xlim([0,1])

sn.violinplot(x='NE_loss recall', y='test_set', data=er_synthetic, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=axes[1])
axes[1].grid(linestyle=':')
axes[1].set_ylabel('')
axes[1].set_yticklabels([])
axes[1].set_xlim([0,1])

sn.violinplot(x='FD_loss recall', y='test_set', data=er_synthetic, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=axes[2])
axes[2].grid(linestyle=':')
axes[2].set_ylabel('')
axes[2].set_yticklabels([])
axes[2].set_xlim([0,1])

sn.despine()
fig.tight_layout()
fig.savefig('writing/new_fig/er_synthetic.pdf')
fig.show()


er_videocall_l = []
er_videocall_l= populate_er_l_multiset(er_videocall_l, test_sets_l[4:], write_folder, name_l)

fig, axes = plt.subplots(1,3, figsize=(15,10))
er_videocall = pd.DataFrame(er_videocall_l, columns=['predictor', 'test_set', 'echo_leak recall', 'NE_loss recall', 'FD_loss recall'])
sn.violinplot(x='echo_leak recall', y='test_set', data=er_videocall, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=axes[0])
axes[0].grid(linestyle=':')
axes[0].set_ylabel('')
axes[0].set_yticklabels(['quiet','noisy','harsh'])
axes[0].set_xlim([0,1])

sn.violinplot(x='NE_loss recall', y='test_set', data=er_videocall, hue='predictor', palette='Set3',  hue_order=hue_order, cut=0, ax=axes[1])
axes[1].grid(linestyle=':')
axes[1].set_ylabel('')
axes[1].set_yticklabels([])
axes[1].set_xlim([0,1])

sn.violinplot(x='FD_loss recall', y='test_set', data=er_videocall, hue='predictor', palette='Set3', hue_order=hue_order, cut=0, ax=axes[2])
axes[2].grid(linestyle=':')
axes[2].set_ylabel('')
axes[2].set_yticklabels([])
axes[2].set_xlim([0,1])

sn.despine()
fig.tight_layout()
fig.savefig('writing/new_fig/er_videocall.pdf')
fig.show()



# %%
# -----------------------------------------------------------------------------------
# TS SPACE VISUALIZATION
# -----------------------------------------------------------------------------------

def make_ts_space():
    MAX = 8 - 2**(-4)
    ne = layers.Input(shape=(None, 1))
    fe = layers.Input(shape=(None, 1))
    ts = tf.concat([ne, tf.square(ne), fe, tf.square(fe)], axis=2)
    ts = layers.Conv1D(32, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(16, 1)(ts)
    ts = layers.BatchNormalization()(ts)
    ts = layers.ReLU(MAX)(ts)
    ts = layers.Conv1D(4, 1,)(ts)
    ts = layers.Softmax()(ts)
    ts = layers.Layer(name='TS')(ts)

    model = tf.keras.Model(inputs=[ne, fe], outputs=ts)
    # this model is only needed for inference, basic loss used 
    model.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001), loss=tf.keras.losses.binary_crossentropy, metrics=['accuracy'])
    
    return model

def ts_space_grid(ts_space_model, n_points=40):
    # out_l = []
    class_record = np.zeros((n_points**2, ))
    class_pred = np.zeros((n_points**2, ))
    out_grid = np.zeros((2, n_points**2))
    in_grid = np.zeros((2, n_points**2))
    ne_space = np.linspace(0,1,n_points)
    fe_space = np.linspace(0,1,n_points)

    for i in tqdm(range(ne_space.shape[0])):
        for j in range(fe_space.shape[0]):
            ne_vad = tf.convert_to_tensor([ne_space[i]])
            ne_vad = tf.expand_dims(ne_vad, axis=0)
            ne_vad = tf.expand_dims(ne_vad, axis=0)

            fe_vad = tf.convert_to_tensor([fe_space[j]])
            fe_vad = tf.expand_dims(fe_vad, axis=0)
            fe_vad = tf.expand_dims(fe_vad, axis=0)

            out = ts_space_model([ne_vad, fe_vad])
            out = np.squeeze(out.numpy())
            # out_l.append(out)
            
            ne_class = round(ne_space[i])
            fe_class = round(fe_space[j])
            ts_class = ne_class + 2 * fe_class
            class_record[i*n_points+j] = ts_class
            class_pred[i*n_points+j] = np.argmax(out)
            ne = out[1] + out[3]
            fe = out[2] + out[3]

            in_grid[0, i*n_points+j] = ne_space[i]
            in_grid[1, i*n_points+j]= fe_space[j]
            out_grid[0, i*n_points+j] = ne
            out_grid[1, i*n_points+j] = fe

    return in_grid, out_grid, class_record, class_pred

# %%
# new TS space viz
# using CA_Opt su videocall noisy
bd.plots.set_style('default')

r_noisy = np.load(write_folder+'rec_noisy_CA_Opt.npy', allow_pickle=True)

# near end confusion matrix
ts_target = np.concatenate(r_noisy[4].tolist())
ne_target = np.zeros(ts_target.shape)
ne_target[ts_target%2==1] = 1

ne_cm = confusion_matrix(ne_target, np.round(np.concatenate(r_noisy[0].tolist())), normalize='true')

df_cm = pd.DataFrame(ne_cm.T, index = [i for i in ["0", "1"]],
                columns = [i for i in["0", "1"]])
fig = plt.figure(figsize=(3.4, 3))
sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
plt.xlabel("true")
plt.ylabel("pred")
plt.title("near-end VAD")
plt.tight_layout()
fig.savefig('writing/new_fig/NE_VAD.svg')
fig.show()


# far end confusion matrix
ts_target = np.concatenate(r_noisy[4].tolist())
fe_target = np.zeros(ts_target.shape)
fe_target[ts_target>1] = 1

fe_cm = confusion_matrix(fe_target, np.round(np.concatenate(r_noisy[1].tolist())), normalize='true')

df_cm = pd.DataFrame(fe_cm.T, index = [i for i in ["0", "1"]],
                columns = [i for i in["0", "1"]])
fig = plt.figure(figsize=(3.4, 3))
sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
plt.xlabel("true")
plt.ylabel("pred")
plt.title("far-end VAD")
plt.tight_layout()
fig.savefig('writing/new_fig/FE_VAD.svg')
fig.show()

# double VAD TSD
fig = plot_confusion_matrix(np.concatenate(r_noisy[4].tolist()), np.concatenate(r_noisy[3].tolist()), 'double-VAD TSD')
fig.savefig('writing/new_fig/double_VAD_TSD.svg')
fig.show()

# TS space TSD
fig = plot_confusion_matrix(np.concatenate(r_noisy[4].tolist()), np.concatenate(r_noisy[2].tolist()), 'TS space TSD')
fig.savefig('writing/new_fig/TS_space_TSD.svg')
fig.show()


# TS SPACE VIZ best model
ts_space_model = make_ts_space()
ts_space_model.set_weights(model_best.get_weights()[-14:])
in_grid, out_grid, class_record, class_pred = ts_space_grid(ts_space_model, 40)

# %%
plt.figure()
plt.scatter(in_grid[0, class_pred==0], in_grid[1, class_pred==0], label='NT', marker='s', alpha=0.3)
plt.scatter(in_grid[0, class_pred==1], in_grid[1, class_pred==1], label='NO',marker='s',alpha=0.3)
plt.scatter(in_grid[0, class_pred==2], in_grid[1, class_pred==2], label='ST',marker='s',alpha=0.3)
plt.scatter(in_grid[0, class_pred==3], in_grid[1, class_pred==3], label='DT',marker='s',alpha=0.3)
plt.grid(linestyle=':')
plt.xticks([0, 0.5, 1])
plt.yticks([0, 0.5, 1])
axes=plt.gca()
axes.set_aspect('equal')
plt.title('Final TS space')
plt.legend(bbox_to_anchor=(1.01, 1))
plt.savefig('writing/new_fig/TS_space_viz.svg')
plt.show()

plt.figure()
plt.scatter(in_grid[0, class_record==0], in_grid[1, class_record==0], label='no talk', marker='s', alpha=0.3)
plt.scatter(in_grid[0, class_record==1], in_grid[1, class_record==1], label='near only',marker='s',alpha=0.3)
plt.scatter(in_grid[0, class_record==2], in_grid[1, class_record==2], label='single talk',marker='s',alpha=0.3)
plt.scatter(in_grid[0, class_record==3], in_grid[1, class_record==3], label='double talk',marker='s',alpha=0.3)
plt.grid(linestyle=':')
plt.xticks([0, 0.5, 1])
plt.yticks([0, 0.5, 1])
axes=plt.gca()
axes.set_aspect('equal')
plt.title('Initial TS space')
plt.legend(bbox_to_anchor=(1.05, 1))
plt.savefig('writing/new_fig/TS_space_init.svg')
plt.show()



# %%



# %%
# distortion visual

sig, sr = bd.io.read_wav('/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/rnnoise_data/speech/ML72_08.wav', fmt=bd.T_INT16F)
sig = sig[0:45000]
plt.figure()
plt.plot(sig)
plt.ylim([-8000,8000]) 
plt.show()

def sinful_distortion(in_sig, l, relative_gain_db=0, preserve_level=True, mix=1):  
    """
    Perform signal distortion using a rescaled and clipped sine
    """ 
    gain = 10 ** (relative_gain_db/20)
    sig = in_sig.copy()

    in_std = np.std(sig)
    sig = sig/in_std
    sig = gain * sig

    sig = np.clip(sig, -np.pi/2, np.pi/2)
    sig = np.sin(sig)*(1+l)
    sig = np.clip(sig, -1, 1)
    sig = sig/gain
    if preserve_level:
        sig = sig*in_std
    return mix * sig + (1-mix) * in_sig

# %%

dist_sig = sinful_distortion(sig, l=1, relative_gain_db=-2, mix=0.8 ) 

plt.figure()
plt.plot(sig)
plt.ylim([-8000,8000]) 
plt.axis('off')
plt.savefig('speech_wave.svg')
plt.show()

plt.figure()
plt.plot(dist_sig)
plt.ylim([-8000,8000])
plt.axis('off')
plt.savefig('speech_sinful_wave.svg')
plt.show()


# %%
# %%
# functions

config = bd.utils.Class_Dict(sr=SR, framesize=int(0.01*SR), zero_pad_end=False,
                    pre_emphasis=True,
                    pre_gain=1, window="bd", ola_ratio=4, max_freq=20000,
                    stft_gain=1, value_type=bd.T_INT16F, dc_removal=True)
stft_processor = STFT(config=config)
stft_processor.b_wola.out_idx = 1
stft_processor.reset()

def apply_ts(aec_out, ts):
    aec_out[ts==0.0] *= 0.01
    aec_out[ts==1.0] *= 1
    aec_out[ts==2.0] *= 0.01
    aec_out[ts==3.0] *= 1
    return aec_out

def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i,:] *= ts_red[i]
    return aec_out

def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    # ne[pred_sel%2==1] = 1
    # fe[pred_sel>1] = 1
    ne[ts%2==1] = 1
    fe[ts>1] = 1
    return ne, fe

def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2*fe
    return ts_out

def todB(x):
    return 20*np.log10(x)

def tolin(x):
    return 10**(x/20)

def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []
        
        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)
        
        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)
        
    return ts_reduction

# %%
# audio inference
# preprocessing for inference

sox_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_sox'
slicing_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_slice'
tbmap_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_tbmap'
bin_folder = '/home/danielefoscarin/LOCAL/SSD/bin'

# sin = '/home/disks/ssdata/danielefoscarin/test_sets/rec_clean/test_scenario_63/sIn.wav'
# rin = '/home/disks/ssdata/danielefoscarin/test_sets/rec_clean/test_scenario_63/rIn.wav'

sin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/sin.wav'
rin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/rin.wav'

# sin = '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR/test12dB_10/sIn.wav'
# rin = '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR/test12dB_10/rIn.wav'


sin_sig, _ = bd.io.read_wav(sin, fmt=bd.T_INT16F)
rin_sig, _ = bd.io.read_wav(rin, fmt=bd.T_INT16F)
aec_sig, _ = bd.io.read_wav(os.path.join(os.path.dirname(sin),'temp_sOut.wav'), fmt=bd.T_INT16F)

sn.set_palette('tab10')

# %%
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
inference_preprocessor_lin = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, augmentation=False)
inference_preprocessor_bark = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, augmentation=False)
bands_freq_big = np.linspace(150, 8000, 96, endpoint=False)
inference_preprocessor_big = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq_big, augmentation=False)

print('lin')
feat_lin, aec_out = inference_preprocessor_lin.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_lin = tf.expand_dims(feat_lin, axis=0)
print('bark')
feat_bark, aec_out = inference_preprocessor_bark.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_bark = tf.expand_dims(feat_bark, axis=0)
print('big')
feat_big, aec_out = inference_preprocessor_big.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_big = tf.expand_dims(feat_big, axis=0)

# print(feat.shape)
# %%
def model_inference(model, feat):
    output = model(feat)
    ne_vad = np.squeeze(output[0].numpy())
    fe_vad = np.squeeze(output[1].numpy())
    ts_class = np.squeeze(output[2].numpy())
    ts = np.zeros((ts_class.shape[0], ))
    for i in range(ts.shape[0]):
        ts[i] = np.argmax(ts_class[i])
    return ne_vad, fe_vad, ts
# ic(ne_vad.shape, fe_vad.shape, ts.shape)


# %%
ne_lin, fe_lin, ts_lin = model_inference(model_best, feat_lin)
plt.figure()
plt.plot(np.arange(ts_lin.shape[0])/100, ts_lin)
# plt.xlim(zoom)
plt.grid()
plt.show()

# %%
ne_bark, fe_bark, ts_bark = model_inference(model_bark, feat_bark)

plt.figure()
plt.plot(np.arange(ts_bark.shape[0])/100, ts_bark)
plt.grid()
plt.show()

# %%
ne_big, fe_big, ts_big = model_inference(model_big, feat_big)

plt.figure(figsize=(8,3))
plt.plot(np.arange(ts_big.shape[0])/100, ts_big)
plt.plot(np.arange(sin_sig.shape[0])/16000, sin_sig/10000+1.5, alpha=0.3)
plt.grid()
plt.show()

# %%

ts_baseline = np.load(os.path.join(os.path.dirname(sin), 'tbmap_ts.npy'), allow_pickle=True)
plt.figure()
plt.plot(np.arange(ts_baseline.shape[0])/100, ts_baseline)
plt.grid()
plt.show()

ts_target = np.load(os.path.join(os.path.dirname(sin), 'talk_state.npy'), allow_pickle=True)
plt.figure()
plt.plot(np.arange(ts_target.shape[0]-1)/100, ts_target[1:])
plt.grid()
plt.show()

# %%
stft_processor.reset()
# post_aec_stft = apply_ts(aec_out.numpy(), ts_lin)
spec_fix = 1
s_len = 15

ts_post_lin = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_lin, slow_len=s_len)))
stft_processor.reset()
ts_post_bark = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_bark, slow_len=s_len)))
stft_processor.reset()
ts_post_big = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_big, slow_len=s_len)))
stft_processor.reset()
ts_post_baseline = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_baseline, slow_len=s_len)))
stft_processor.reset()
ts_post_target = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_target[1:], slow_len=s_len)))
stft_processor.reset()

bd.io.write_wav("audio/ts_post_lin.wav", ts_post_lin, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ts_post_bark.wav", ts_post_bark, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ts_post_big.wav", ts_post_big, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ts_post_baseline.wav", ts_post_baseline, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ts_post_target.wav", ts_post_target, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/aec_out.wav", stft_processor.invert_np(aec_out.numpy()), SR, fmt=bd.T_INT16F) 
# bd.io.write_wav("aec_out.wav", aec_out_audio, SR) 

plt.figure(figsize=(8,3))
plt.plot(np.arange(ts_lin.shape[0])/100, ts_lin)
plt.plot(np.arange(ts_post_sig.shape[0])/16000, ts_post_sig/10000+1.5, alpha=0.3)
plt.grid()
plt.show()
# %%



# %%
# audio inference
# preprocessing for inference ugo

sox_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_sox'
slicing_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_slice'
tbmap_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_tbmap'
bin_folder = '/home/danielefoscarin/LOCAL/SSD/bin'


sin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/sin.wav'
rin = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386/rin.wav'



sin_sig, _ = bd.io.read_wav(sin, fmt=bd.T_INT16F)
rin_sig, _ = bd.io.read_wav(rin, fmt=bd.T_INT16F)


sn.set_palette('tab10')

# %%
bands_freq = np.linspace(150, 8000, 48, endpoint=False)
inference_preprocessor_lin = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq, augmentation=False)
inference_preprocessor_bark = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, band_type='bark', n_bands=48, augmentation=False)
bands_freq_big = np.linspace(150, 8000, 96, endpoint=False)
inference_preprocessor_big = Pipeline_Preprocessor(SR, SAMPLE_LEN_SEC, custom_bands=bands_freq_big, augmentation=False)

print('lin')
feat_lin, aec_out = inference_preprocessor_lin.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_lin = tf.expand_dims(feat_lin, axis=0)
print('bark')
feat_bark, aec_out = inference_preprocessor_bark.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_bark = tf.expand_dims(feat_bark, axis=0)
print('big')
feat_big, aec_out = inference_preprocessor_big.inference_preprocessing(sin, rin, bin_folder, tbmap_temp_folder, sox_temp_folder)
feat_big = tf.expand_dims(feat_big, axis=0)

# print(feat.shape)
# %%
def model_inference(model, feat):
    output = model(feat)
    ne_vad = np.squeeze(output[0].numpy())
    fe_vad = np.squeeze(output[1].numpy())
    ts_class = np.squeeze(output[2].numpy())
    ts = np.zeros((ts_class.shape[0], ))
    for i in range(ts.shape[0]):
        ts[i] = np.argmax(ts_class[i])
    return ne_vad, fe_vad, ts
# ic(ne_vad.shape, fe_vad.shape, ts.shape)


# %%
ne_lin, fe_lin, ts_lin = model_inference(model_best, feat_lin)
plt.figure()
plt.plot(np.arange(ts_lin.shape[0])/100, ts_lin)
# plt.xlim(zoom)
plt.grid()
plt.show()

# %%
ne_bark, fe_bark, ts_bark = model_inference(model_bark, feat_bark)

plt.figure()
plt.plot(np.arange(ts_bark.shape[0])/100, ts_bark)
plt.grid()
plt.show()

# %%
ne_big, fe_big, ts_big = model_inference(model_big, feat_big)

plt.figure(figsize=(8,3))
plt.plot(np.arange(ts_big.shape[0])/100, ts_big)
plt.plot(np.arange(sin_sig.shape[0])/16000, sin_sig/10000+1.5, alpha=0.3)
plt.grid()
plt.show()

# %%

# ts_baseline = np.load(os.path.join(os.path.dirname(sin), 'tbmap_ts.npy'), allow_pickle=True)
# plt.figure()
# plt.plot(np.arange(ts_baseline.shape[0])/100, ts_baseline)
# plt.grid()
# plt.show()

# ts_target = np.load(os.path.join(os.path.dirname(sin), 'talk_state.npy'), allow_pickle=True)
# plt.figure()
# plt.plot(np.arange(ts_target.shape[0]-1)/100, ts_target[1:])
# plt.grid()
# plt.show()

# %%
stft_processor.reset()
# post_aec_stft = apply_ts(aec_out.numpy(), ts_lin)
spec_fix = 1
s_len = 15

ts_post_lin = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_lin, slow_len=s_len)))
stft_processor.reset()
ts_post_bark = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_bark, slow_len=s_len)))
stft_processor.reset()
ts_post_big = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_big, slow_len=s_len)))
stft_processor.reset()
# ts_post_baseline = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_baseline, slow_len=s_len)))
stft_processor.reset()
# ts_post_target = stft_processor.invert_np(apply_ts_reduction(aec_out.numpy()*spec_fix, ts_conditional_smoothing(ts_target[1:], slow_len=s_len)))
stft_processor.reset()

bd.io.write_wav("audio/ugo/ts_post_lin.wav", ts_post_lin, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ugo/ts_post_bark.wav", ts_post_bark, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/ugo/ts_post_big.wav", ts_post_big, SR, fmt=bd.T_INT16F) 
# bd.io.write_wav("audio/ugo/ts_post_baseline.wav", ts_post_baseline, SR, fmt=bd.T_INT16F) 
# bd.io.write_wav("audio/ugo/ts_post_target.wav", ts_post_target, SR, fmt=bd.T_INT16F) 
bd.io.write_wav("audio/aec_out.wav", stft_processor.invert_np(aec_out.numpy()), SR, fmt=bd.T_INT16F) 
# bd.io.write_wav("aec_out.wav", aec_out_audio, SR) 

plt.figure(figsize=(8,3))
plt.plot(np.arange(ts_lin.shape[0])/100, ts_lin)
plt.plot(np.arange(ts_post_sig.shape[0])/16000, ts_post_sig/10000+1.5, alpha=0.3)
plt.grid()
plt.show()


# %%
temp_folder = '/home/disks/ssdata/danielefoscarin/TSD_thesis/thesis_foscarin/temp'
data_folder = '/home/danielefoscarin/NAS_RD/GENERAL/DATASET_SOURCE/GOOGLE/DataCollection4MachineLearning/LR/Clean/543_2067386'
tb_map_subprocess(bin_folder, data_folder, os.path.join(bin_folder,'tb_map_21_07'),
                        os.path.join(data_folder, 'sin.wav'),
                        # os.path.join(data_folder, rin_name),
                        os.path.join(data_folder, 'rin.wav'),    
                        os.path.join(temp_folder, 'sOut.wav'),
                        os.path.join(bin_folder, 'ugo.conf'),
                        verbose=False, path_mode='full')
