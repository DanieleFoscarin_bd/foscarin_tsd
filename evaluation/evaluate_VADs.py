# %% 
# libraries
from unicodedata import normalize
import bd_rd as bd
from seaborn.distributions import kdeplot
from tensorflow.python.keras.optimizer_v2.rmsprop import RMSProp
# bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks import STFT
from bd_rd.processing.common import get_bd_window

from scenario_generator import *  
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import keras.backend as bk


import numpy as np
from matplotlib import pyplot as plt
import os
from icecream import ic
import keras_flops
from tqdm import tqdm
import io
import joblib
import datetime
from sklearn.metrics import confusion_matrix, balanced_accuracy_score, accuracy_score, matthews_corrcoef
import seaborn as sn
import pandas as pd


import tensorflow as tf
from tensorflow.keras import layers


SR = 16000
SAMPLE_LEN_SEC = 20

SAVE_FIG = False



# %% 
# utility functions
def santilli_and_target():
    pass

def plot_confusion_matrix(target, pred, title):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = confusion_matrix(target, pred)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm)/4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

def plot_cm_ready(cm, title):
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(cm.T, index = [i for i in ["NT", "NO", "ST", "DT"]],
                    columns = [i for i in["NT", "NO", "ST", "DT"]])
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure

def normalize_cm(cm):
    return cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]




def load_target(dataset_folder, mode):
    scenarios_list = [name for name in os.listdir(dataset_folder) if name.startswith(mode)]
    scenarios_list.sort()
    target_l = []
    for i, scenario in enumerate(scenarios_list):
        #remember to ignore the first value of talk_state_npy since its the delay value
        curr_target = np.load(os.path.join(dataset_folder, scenario, 'talk_state.npy'))[1:]
        target_l.append(curr_target)
    return target_l

def load_baseline(dataset_folder, mode):
    scenarios_list = [name for name in os.listdir(dataset_folder) if name.startswith(mode)]
    scenarios_list.sort()
    tbmap_ts_l = []
    for i, scenario in enumerate(scenarios_list):
        curr_tbmap_ts = np.load(os.path.join(dataset_folder, scenario, 'tbmap_ts.npy'))
        tbmap_ts_l.append(curr_tbmap_ts)
    return tbmap_ts_l

def dataset_inference(dataset, model):
    ts_l = []
    ne_l = []
    fe_l = []
    vad_ts_l = []
    target_l = []
    for feat, target in tqdm(dataset): 
        output = model(feat)
        ne_vad = np.squeeze(output[0].numpy())
        fe_vad = np.squeeze(output[1].numpy())
        ts_class = np.squeeze(output[2].numpy())
        ts = np.zeros((ts_class.shape[0], ))
        ts = np.argmax(ts_class, axis=1)
        target = target['TS']
        target = np.squeeze(target.numpy())

        ts_l.append(ts)
        ne_l.append(ne_vad)
        fe_l.append(fe_vad)
        vad_ts_l.append(np.round(ne_vad) + 2*np.round(fe_vad))

        target_l.append(target)

    return ne_l, fe_l, ts_l, vad_ts_l, target_l

config = bd.utils.Class_Dict(sr=SR, framesize=int(0.01*SR), zero_pad_end=False,
                    pre_emphasis=True,
                    pre_gain=1, window="bd", ola_ratio=4, max_freq=20000,
                    stft_gain=1, value_type=bd.T_INT16F, dc_removal=True)
stft_processor = STFT(config=config)
stft_processor.b_wola.out_idx = 1

def apply_ts(aec_out, ts):
    aec_out[ts==0.0] *= 0.01
    aec_out[ts==1.0] *= 1
    aec_out[ts==2.0] *= 0.01
    aec_out[ts==3.0] *= 1
    return aec_out

def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i,:] *= ts_red[i]
    return aec_out

def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    ne[ts%2==1] = 1
    fe[ts>1] = 1
    return ne, fe

def ts_to_ne(ts):
    ne = np.zeros(ts.shape)
    ne[ts%2==1] = 1
    return ne

def ts_to_fe(ts):
    fe = np.zeros(ts.shape)
    fe[ts>1] = 1
    return fe

def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2*fe
    return ts_out

def todB(x):
    return 20*np.log10(x)

def tolin(x):
    return 10**(x/20)

def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []
        
        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)
        
        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)
        
    return ts_reduction




# %%
# new TS space viz
# using CA_Opt su videocall noisy
write_folder = '/home/disks/ssdata/danielefoscarin/small_data/test_inference'
bd.plots.set_style('default')
# format: ne, fe, ts, vad_ts, target
test_set_name = 'rec_noisier'
test_set = os.path.join(write_folder,test_set_name)
model = 'CA_Opt'
pred = np.load(test_set+'_'+model+'.npy', allow_pickle=True)
# pred = np.load(write_folder+'synthetic_48_SNR_L_Opt.npy', allow_pickle=True)




# near end confusion matrix
ts_target = np.concatenate(pred[4].tolist())
ne_target = np.zeros(ts_target.shape)
ne_target[ts_target%2==1] = 1
tbmap_vad = np.load(test_set+'_tbmap_VAD.npy', allow_pickle=True)

ne_ba = balanced_accuracy_score(ne_target, np.round(np.concatenate(pred[0].tolist())))
ne_tss_ba = balanced_accuracy_score(ne_target, np.round(np.concatenate(ts_to_ne(pred[2]))))
tbmap_ba = balanced_accuracy_score(ne_target, np.round(np.concatenate(tbmap_vad.tolist())))
print('NE VAD balanced_acc: ',ne_ba)
print('NE+ts-space VAD balanced_acc: ',ne_tss_ba)


print('NRAI VAD balanced_acc: ',tbmap_ba)

plt.figure(figsize=(9,3))
plt.subplot(1,3,1,)
tbmap_cm = confusion_matrix(ne_target, np.round(np.concatenate(tbmap_vad.tolist())), normalize='true')
df_cm = pd.DataFrame(tbmap_cm.T, index = [i for i in ["0", "1"]],
                columns = [i for i in["0", "1"]])
sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1,)
plt.xlabel("true")
plt.ylabel("pred")
plt.title("NRAI VAD \n balanced_acc: "+str(np.round(tbmap_ba, 4)))
plt.tight_layout()

plt.subplot(1,3,2)
ne_cm = confusion_matrix(ne_target, np.round(np.concatenate(pred[0].tolist())), normalize='true')
df_cm = pd.DataFrame(ne_cm.T, index = [i for i in ["0", "1"]],
                columns = [i for i in["0", "1"]])
sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1, )
plt.xlabel("true")
plt.ylabel("pred")
plt.title("tsd VAD \n balanced_acc: "+str(np.round(ne_ba, 4)))
plt.tight_layout()

plt.subplot(1,3,3)
ne_tss_cm = confusion_matrix(ne_target, np.round(np.concatenate(ts_to_ne(pred[2]))), normalize='true')
df_cm = pd.DataFrame(ne_tss_cm.T, index = [i for i in ["0", "1"]],
                columns = [i for i in["0", "1"]])

sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1,)
plt.xlabel("true")
plt.ylabel("pred")
plt.title("tsd VAD+TS space \n balanced_acc: "+str(np.round(ne_tss_ba, 4)))
plt.tight_layout()

plt.savefig(model+'_'+test_set_name+'_VADs.png')
fig.show()


# %%
aec, sr = bd.io.read_wav('/home/disks/ssdata/danielefoscarin/test_sets/synthetic_6_SNR/test6dB_0/temp_sOut.wav', fmt=bd.T_INT16F)
pred_r = np.repeat(pred[0,19], 160)
pred_r[pred_r>0.5] = 1
pred_r[pred_r<=0.5] = 0
pred_r = aec*pred_r
bd.io.write_wav('/home/disks/ssdata/danielefoscarin/test_sets/synthetic_6_SNR/test6dB_0/pred_r.wav', pred_r, sr=16000, fmt=bd.T_INT16F)

plt.figure()
plt.plot(tbmap_vad[19], label='tbmap', alpha=0.5)
plt.plot(pred[0,19], label='needle_head', alpha=0.5)
# plt.plot(aec)
plt.legend()
plt.show()


# %%

# # far end confusion matrix
# ts_target = np.concatenate(pred[4].tolist())
# fe_target = np.zeros(ts_target.shape)
# fe_target[ts_target>1] = 1

# fe_cm = confusion_matrix(fe_target, np.round(np.concatenate(pred[1].tolist())), normalize='true')

# df_cm = pd.DataFrame(fe_cm.T, index = [i for i in ["0", "1"]],
#                 columns = [i for i in["0", "1"]])
# fig = plt.figure(figsize=(3.4, 3))
# sn.heatmap(df_cm, annot=True, cmap='rocket_r', vmin=0, vmax=1)
# plt.xlabel("true")
# plt.ylabel("pred")
# plt.title("far-end VAD")
# plt.tight_layout()
# fig.show()
# print('Far End VAD ACCURACY: ',accuracy_score(fe_target, np.round(np.concatenate(pred[1].tolist()))))



# %%
# produce NE VAD with tbmap in test sets
bin_folder = '/home/danielefoscarin/LOCAL/SSD/bin'
sox_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_sox'
slicing_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_slice'
tbmap_temp_folder = '/home/danielefoscarin/LOCAL/SSD/temp_tbmap'
test_sets_l = [
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_48_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_24_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_12_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/synthetic_6_SNR',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_clean',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisy',
    '/home/disks/ssdata/danielefoscarin/test_sets/rec_noisier',
    ]
test_set_folder = test_sets_l[6]

test_scenarios_list = [name for name in os.listdir(test_set_folder) if name.startswith('test')]
test_scenarios_list.sort()

for i, scenario in tqdm(enumerate(test_scenarios_list)):
    scenario = os.path.join(test_set_folder, scenario)
    # curr_target, curr_tbmap_ts, curr_delay_samples = get_oracle_talk_state(os.path.join(out_dataset_folder, scenario), bin_folder, rin_name='rIn_clean.wav', temp_folder=tbmap_temp_folder, far_end_AD=True)
    tb_map_subprocess(bin_folder, '', os.path.join(bin_folder,'tb_map_21_07'),
                        os.path.join(scenario, 'sIn.wav'),
                        os.path.join(scenario, 'rIn.wav'),
                        os.path.join(tbmap_temp_folder, 'temp_sOut.wav'),
                        os.path.join(bin_folder, 'nrai20_with_NR.conf'),
                        verbose=False, path_mode='full')
    # ne_VAD = read_probe_file(os.path.join(temp_folder, 'probe_AI_SPP_vad_prob.dat')).data
    os.rename(os.path.join(tbmap_temp_folder, 'probe_AI_SPP_vad_prob.dat'), os.path.join(scenario, 'probe_AI_SPP_vad_prob.dat'))
    # clean probes from probe folder
    clean_probe_dir(tbmap_temp_folder, verbose=False)


# %%
write_folder = '/home/disks/ssdata/danielefoscarin/small_data/test_inference'
for test_set_folder in tqdm(test_sets_l):
    test_scenarios_list = [name for name in os.listdir(test_set_folder) if name.startswith('test')]
    test_scenarios_list.sort()
    tbmap_vad_l = []
    test_set_name = os.path.basename(test_set_folder)
    for i, scenario in tqdm(enumerate(test_scenarios_list)):
        tbmap_VAD = read_probe_file(os.path.join(test_set_folder, scenario, 'probe_AI_SPP_vad_prob.dat')).data
        tbmap_vad_l.append(tbmap_VAD)
        np.save(os.path.join(write_folder, test_set_name+'_tbmap_VAD'), np.array(tbmap_vad_l, dtype=object))


# 



