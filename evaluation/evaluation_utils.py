# %%
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(rootrepodir)

import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10

from pf_utils import *
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import numpy as np
from matplotlib import pyplot as plt

# import keras_flops
from tqdm import tqdm
from sklearn.metrics import confusion_matrix
import seaborn as sn
import pandas as pd


# import tensorflow as tf
# from tensorflow.keras import layers


def plot_confusion_matrix(target, pred, title, normalize="true"):
    # cm = confusion_matrix(np.concatenate(target_l), np.concatenate(pred_l),)
    cm = np.transpose(confusion_matrix(target, pred, normalize=normalize))
    # cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
    acc = np.trace(cm) / 4
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(
        cm,
        index=[i for i in ["NT", "NO", "ST", "DT"]],
        columns=[i for i in ["NT", "NO", "ST", "DT"]],
    )
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap="rocket_r", vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure


def plot_cm_ready(cm, title):
    # print("Accuracy: ", acc)
    df_cm = pd.DataFrame(
        cm,
        index=[i for i in ["NT", "NO", "ST", "DT"]],
        columns=[i for i in ["NT", "NO", "ST", "DT"]],
    )
    figure = plt.figure(figsize=(5, 4))
    sn.heatmap(df_cm, annot=True, cmap="rocket_r", vmin=0, vmax=1)
    plt.xlabel("true")
    plt.ylabel("pred")
    plt.title(title)
    plt.tight_layout()
    return figure


# OBSOLETE
def normalize_cm(cm):
    return cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]


def load_target(dataset_folder, mode):
    scenarios_list = [
        name for name in os.listdir(dataset_folder) if name.startswith(mode)
    ]
    scenarios_list.sort()
    target_l = []
    for i, scenario in enumerate(scenarios_list):
        curr_target = np.load(os.path.join(dataset_folder, scenario, "talk_state.npy"))
        target_l.append(curr_target)
    return target_l


def load_baseline(dataset_folder, mode):
    """Load baseline numpy saved in the scenario folder

    Parameters
    ----------
    dataset_folder : str
        Path to the dataset.
    mode : str
        COnsider only the scenario with that specified prefix.

    Returns
    -------
    list of ndarray
        List of the baseline classifications in every scenario.
    """
    scenarios_list = [
        name for name in os.listdir(dataset_folder) if name.startswith(mode)
    ]
    scenarios_list.sort()
    tbmap_ts_l = []
    for i, scenario in enumerate(scenarios_list):
        curr_tbmap_ts = np.load(os.path.join(dataset_folder, scenario, "tbmap_ts.npy"))
        tbmap_ts_l.append(curr_tbmap_ts)
    return tbmap_ts_l


def prepare_dataset(
    dataset_folder, type, sr=16000, sample_len_sec=20, bands_type="linear"
):
    # TODO integrate bark bands
    """Prepare a dataset for NeedleHead inference

    Parameters
    ----------
    dataset_folder : str
        Path to the folder containing the scenarios.
    type : 'train', 'val', test'
        Consider only the scenarios with the selected type.

    Returns
    -------
        tf.data.Dataset
        Dataset object already batched and ready to be iterated.
    """
    assert type in ["train", "val", "test"], "Supported type are 'train', 'val', test'."
    scenarios_list = [
        name for name in os.listdir(dataset_folder) if name.startswith(type)
    ]
    scenarios_list.sort()

    auto = tf.data.AUTOTUNE
    auto = 4
    lin_bands_freq = np.linspace(150, 8000, 48, endpoint=False)
    batch_size = 1

    path_list = [os.path.join(dataset_folder, name) for name in scenarios_list]
    dataset = tf.data.Dataset.from_tensor_slices(path_list)
    dataset = dataset.prefetch(auto)

    if bands_type == "linear":
        preprocessor_lin = Pipeline_Preprocessor(
            sr,
            sample_len_sec,
            custom_bands=lin_bands_freq,
            target_type="dv_ts",
            augmentation=False,
        )
    elif bands_type == "bark":
        preprocessor_lin = Pipeline_Preprocessor(
            sr,
            sample_len_sec,
            # custom_bands=lin_bands_freq,
            band_type="bark",
            target_type="dv_ts",
            augmentation=False,
        )
    dataset = dataset.map(
        preprocessor_lin.tf_get_features_from_bd, num_parallel_calls=auto
    )
    dataset = dataset.batch(batch_size)

    return dataset


def features_inference(model, feat):
    """Model inference on a single batch of features

    Parameters
    ----------
    model : keras.Model
        Compiled keras model
    feat : tf.tensor
        Features tensor

    Returns
    -------
    dict(str, ndarray)
        Dictionary, key strings: 'ne','ne_prob', 'fe', 'fe_prob', 'ts', 'ts_prob'
    """
    output = model(feat)
    ne_prob = np.squeeze(output[0].numpy())
    fe_prob = np.squeeze(output[1].numpy())
    ts_prob = np.squeeze(output[2].numpy())
    ts = np.zeros((ts_prob.shape[0],))
    # for i in range(ts.shape[0]):
    #     ts[i] = np.argmax(ts_class[i])
    ts = np.argmax(ts_prob, axis=-1)
    out = {}
    out["ne_prob"] = ne_prob
    out["fe_prob"] = fe_prob
    out["ts_prob"] = ts_prob
    out["ne"] = np.round(ne_prob)
    out["fe"] = np.round(fe_prob)
    out["ts"] = ts
    out["confidence"] = np.max(ts_prob, axis=-1)
    return out


def dataset_inference(dataset, model):
    """Compute inference in a talk state labeled dataset

    Parameters
    ----------
    dataset : tf.data.Dataset
        Prepared tf Dataset, with batch size 1 corresponding to a TS scenario
    model : keras.Model
        Compiled keras NN model

    Returns
    -------
    dict(str, list)
        Dictionary, key strings: 'ts', 'ne', 'fe', 'vad_ts', 'target'.
    """
    ts_l = []
    ne_l = []
    fe_l = []
    vad_ts_l = []
    target_l = []
    for feat, target in tqdm(dataset):
        # print(feat.shape)
        output = model(feat)
        ne_vad = np.squeeze(output[0].numpy())
        fe_vad = np.squeeze(output[1].numpy())
        ts_class = np.squeeze(output[2].numpy())
        ts = np.zeros((ts_class.shape[0],))
        ts = np.argmax(ts_class, axis=1)
        target = target["TS"]
        target = np.squeeze(target.numpy())

        ts_l.append(ts)
        ne_l.append(ne_vad)
        fe_l.append(fe_vad)
        vad_ts_l.append(np.round(ne_vad) + 2 * np.round(fe_vad))
        target_l.append(target)

        out_dict = {}
        out_dict["ts"] = ts_l
        out_dict["ne"] = ne_l
        out_dict["fe"] = fe_l
        out_dict["vad_ts"] = vad_ts_l
        out_dict["target"] = target_l
    return out_dict


def apply_ts(aec_out, ts):
    aec_out[ts == 0.0] *= 0.01
    aec_out[ts == 1.0] *= 1
    aec_out[ts == 2.0] *= 0.01
    aec_out[ts == 3.0] *= 1
    return aec_out


def apply_ts_reduction(aec_out, ts_red):
    for i in range(ts_red.shape[0]):
        aec_out[i, :] *= ts_red[i]
    return aec_out


def ts_to_vad(ts):
    ne = np.zeros(ts.shape)
    fe = np.zeros(ts.shape)
    ne[ts % 2 == 1] = 1
    fe[ts > 1] = 1
    return ne, fe


def ts_smoothing(ts, frames):
    ne, fe = ts_to_vad(ts)
    ne = bd.utils.SMOOTH(ne, bd.utils.SMOOTH_RATE(frames))
    fe = bd.utils.SMOOTH(fe, bd.utils.SMOOTH_RATE(frames))
    ne = np.round(ne)
    fe = np.round(fe)
    ts_out = ne + 2 * fe
    return ts_out


def todB(x):
    return 20 * np.log10(x)


def tolin(x):
    return 10 ** (x / 20)


def ts_conditional_smoothing(ts, slow_len=4, ts0_db=-40, ts2_db=-60):
    past = 0
    curr_red = 0
    slow_ramp = []
    ts_reduction = np.zeros((ts.shape[0],))

    for i in range(ts.shape[0]):
        # conditional build smoothing ramp
        if ts[i] == 0 and (past == 1 or past == 2 or past == 3):
            slow_ramp = [(curr_red + ts0_db) / slow_len] * slow_len
        if ts[i] == 3 and (past == 0 or past == 2):
            slow_ramp = [(0 - curr_red) / slow_len] * slow_len
        # fast transition for near only and single talk
        if ts[i] == 1:
            curr_red = 0
            slow_ramp = []
        if ts[i] == 2:
            curr_red = ts2_db
            slow_ramp = []

        # consume the slow_ramp list
        # if its empty do nothing
        if len(slow_ramp) > 0:
            curr_red += slow_ramp.pop(0)

        # update memory of past frame
        past = ts[i]
        # write in ts array the reduction value for the frame
        ts_reduction[i] = tolin(curr_red)

    return ts_reduction


# %%
