# %%
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(currentdir)
sys.path.append(rootrepodir)

import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
from bd_rd.processing.blocks import STFT

from pf_utils import tb_map_wrap, tb_map_inj_wrap, clean_probe_dir
from evaluation_utils import features_inference

# from evaluation_utils import *
from architectures import make_NeedleHead_4inf
from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import CausalBottleneck, CausalConv2D
import numpy as np
from matplotlib import pyplot as plt
import keras_flops
from tqdm import tqdm

import sklearn.metrics
from sklearn.metrics import confusion_matrix

import seaborn as sns
import pandas as pd
import pickle

import tensorflow as tf
from tensorflow.keras import layers


SR = 16000
# scenario_folder = "/home/disks/ssdata/danielefoscarin/test_sets/devices_test_sets/Aloe/-12dB_speaker/-6dB_noise/test_scenario_40"


class SubjectiveTestInjector:
    def __init__(
        self,
        bin_folder,
        temp_sox_folder,
        model_path,
        tb_map_inj,
        conf_path,
        feature_tb_map_path=None,
        feature_conf_path=None,
    ):

        self.bin_folder = bin_folder
        self.temp_sox_folder = temp_sox_folder
        self.model_path = model_path
        self.tb_map_inj = tb_map_inj
        self.conf_path = conf_path
        self.feature_tb_map_path = feature_tb_map_path
        self.feature_conf_path = feature_conf_path
        self.preprocessor = Pipeline_Preprocessor(
            SR, 20, "bark", 48, target_type="dv_ts", augmentation=False
        )
        self.model_trained = make_NeedleHead_4inf()
        self.model_trained.load_weights(os.path.join(self.model_path, "best_model"))

    def compute_np(
        self, scenario_folder, mode, save_fig=False, ts_makeup=True, clean_probes=True
    ):
        assert mode in ["full_duplex", "leaks_removal", ]

        print(scenario_folder)
        self.scenario_folder = scenario_folder
        self.sin_path = os.path.join(scenario_folder, "sIn.wav")
        self.rin_path = os.path.join(scenario_folder, "rIn.wav")
        self.sOut_legacy_path = os.path.join(scenario_folder, "sOut_legacy.wav")
        self.sout_path = os.path.join(scenario_folder, "sOut_AI_injected.wav")
        self.ts_out_path = os.path.join(scenario_folder, "talk_state_injectable.raw")

        # process files and get features for the NN
        preproc_dict = self.preprocessor.inference_preprocessing(
            sin_path=self.sin_path,
            rin_path=self.rin_path,
            bin_folder=self.bin_folder,
            temp_sox_folder=self.temp_sox_folder,
            temp_tbmap_folder=self.scenario_folder,
            tbmap_path=self.feature_tb_map_path,
            conf_path=self.feature_conf_path,
            clean_probes_after=False,
            verbose=True,
        )
        features = np.expand_dims(preproc_dict["features"], axis=0)

        # NN inference
        inf_dict = features_inference(self.model_trained, features)
        print("\n...Inference ok")

        talk_state = inf_dict["ts"]
        ts_prob = inf_dict["ts_prob"]

        if ts_makeup:
            confidence = inf_dict["confidence"]
            legacy_talk_state = bd.io.probes.read_probe_file(
                os.path.join(self.scenario_folder, "probe_POST_FILTER_talk_state.dat")
            ).data

            # require high confidence for switching ST->DT and sT->NT
            for i, conf_frame in enumerate(confidence):
                if i == 0:
                    past_ts = 0
                else:
                    past_ts = talk_state[i - 1]
                if talk_state[i] == 3 and past_ts == 2 and conf_frame < 0.8:
                    talk_state[i] = 2
                elif talk_state[i] == 0 and past_ts == 2 and conf_frame < 0.75:
                    talk_state[i] = 2
 
            # recovery NN failure
            slow_confidence = bd.utils.SMOOTH(confidence, bd.utils.SMOOTH_RATE(50))
            for i, conf_frame in enumerate(slow_confidence):
                if conf_frame < 0.6:
                    talk_state[i] = legacy_talk_state[i]
        else:
            # quantize talk state
            talk_state = np.zeros((ts_prob.shape[0],))
            talk_state = np.argmax(ts_prob, axis=-1)

        if mode == "full_duplex":
            pass
        if mode == "leaks_removal":
            talk_state = np.where(talk_state == 2, talk_state, legacy_talk_state)      

        if save_fig:
            # talk state figure
            plt.figure(figsize=(12, 6.5))
            plt.plot(talk_state, label="AI_ts")
            plt.plot(legacy_talk_state, alpha=0.5, label="legacy_ts")
            plt.grid()
            plt.yticks([0,1,2,3])
            plt.legend()
            plt.title("talk state")
            plt.savefig(os.path.join(self.scenario_folder, "talk_state_viz.png"))
            plt.show()

            # confidence figure
            plt.figure(figsize=(12, 6.5))
            plt.plot(confidence)
            plt.plot(slow_confidence)
            plt.title("Softmax confidence")
            plt.grid()
            plt.ylim([0, 1])
            plt.savefig(os.path.join(self.scenario_folder, "confidence_viz.png"))
            plt.show()

        # save Talkstate as binary file
        talk_state.astype("int32").tofile(self.ts_out_path)

        tb_map_inj_wrap(
            tb_map=self.tb_map_inj,
            sIn=self.sin_path,
            rIn=self.rin_path,
            sOut=self.sout_path,
            ts=self.ts_out_path,
            conf=self.conf_path,
            verbose=False,
            fix_header=True,
        )
        print("\n...talkstate injection ok")
        if clean_probes:
            clean_probe_dir(self.scenario_folder, verbose=True)

        tb_map_wrap(
            # tb_map=self.tb_map_legacy,
            tb_map=self.tb_map_inj,
            sIn=self.sin_path,
            rIn=self.rin_path,
            sOut=self.sOut_legacy_path,
            conf=self.conf_path,
            verbose=False,
        )

        clean_probe_dir(self.scenario_folder, verbose=True)
        print("\n...legacy processing ok\n\n")




# %%
# Process entire subjective test 23-12

subjective_test_injector = SubjectiveTestInjector(
    bin_folder="/home/disks/ssdata/danielefoscarin/bin",
    temp_sox_folder="/home/disks/ssdata/danielefoscarin/temp_sox",
    model_path="/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20220111-231034",
    tb_map_inj="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf.conf",
    feature_conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf_AEC_only.conf",
)
subjective_test_injector_automotive = SubjectiveTestInjector(
    bin_folder="/home/disks/ssdata/danielefoscarin/bin",
    temp_sox_folder="/home/disks/ssdata/danielefoscarin/temp_sox",
    model_path="/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20220111-231034",
    tb_map_inj="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf_220delay.conf",
    feature_conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf_220delay_AEC_only.conf",
)

# NB Make a copy of the unprocessed test set and call the function on it
test_root_folder = "/home/disks/ssdata/danielefoscarin/test_results/subjective_tests_processed_model_20220111-231034_makeup"

scenario_list = os.listdir(test_root_folder)
print(scenario_list)

for scenario in scenario_list:
    # only ferrary needs the long delay, not every automotive
    # TODO custom conf for every ferrari test
    if "automotive" in scenario:
        subjective_test_injector_automotive.compute_np(
            os.path.join(test_root_folder, scenario), save_fig=True, ts_makeup=True
        )
    else:
        subjective_test_injector.compute_np(
            os.path.join(test_root_folder, scenario), save_fig=True, ts_makeup=True
        )
    # subjective_test_process(os.path.join(test_root_folder, scenario))
# %%
# Single test
test_root_folder = "/home/disks/ssdata/danielefoscarin/test_results/subjective_tests_processed_model_20220111-231034_makeup"
# model_path = "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20220111-231034"
model_path = "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20220126-223905"

subjective_test_injector = SubjectiveTestInjector(
    bin_folder="/home/disks/ssdata/danielefoscarin/bin",
    temp_sox_folder="/home/disks/ssdata/danielefoscarin/temp_sox",
    model_path=model_path,
    tb_map_inj="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf_220delay.conf",
    feature_conf_path="/home/disks/ssdata/danielefoscarin/bin/test_conf_220delay_AEC_only.conf",
)
this_scenario = os.path.join(test_root_folder, "call_automotive5")
subjective_test_injector.compute_np(this_scenario, save_fig=True, ts_makeup=True)

# %%
# new ferrari tests
test_root_folder = (
    "/home/disks/ssdata/danielefoscarin/test_results/ferrari_tests_20220119"
)

subjective_test_injector = SubjectiveTestInjector(
    bin_folder="/home/disks/ssdata/danielefoscarin/bin",
    temp_sox_folder="/home/disks/ssdata/danielefoscarin/temp_sox",
    model_path=model_path,
    tb_map_inj="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    conf_path="placeholder",
    feature_tb_map_path="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    feature_conf_path="placeholder",
)

this_scenario = os.path.join(test_root_folder, "call_ferrari7")
subjective_test_injector.conf_path = os.path.join(
    this_scenario, "ecnrCoreConfiguration.conf"
)
subjective_test_injector.feature_conf_path = os.path.join(
    this_scenario, "ecnrCoreConfiguration.conf"
)
subjective_test_injector.compute_np(this_scenario, save_fig=True, ts_makeup=True)

# %%
# new ferrari tests
model_path = "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20220126-223905"
test_root_folder = (
    "/home/disks/ssdata/danielefoscarin/test_results/ferrari_tests_20220201"
)

subjective_test_injector = SubjectiveTestInjector(
    bin_folder="/home/disks/ssdata/danielefoscarin/bin",
    temp_sox_folder="/home/disks/ssdata/danielefoscarin/temp_sox",
    model_path=model_path,
    tb_map_inj="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    conf_path="placeholder",
    feature_tb_map_path="/home/disks/ssdata/danielefoscarin/bin/tb_map_AI_injected_probe",
    feature_conf_path="placeholder",
)

this_scenario = os.path.join(test_root_folder, "call_ferrari6")
subjective_test_injector.conf_path = os.path.join(
    this_scenario, "ecnrCoreConfiguration.conf"
)
subjective_test_injector.feature_conf_path = os.path.join(
    this_scenario, "ecnrCoreConfiguration.conf"
)
subjective_test_injector.compute_np(this_scenario, mode="leaks_removal", save_fig=True, ts_makeup=True)

# %%
