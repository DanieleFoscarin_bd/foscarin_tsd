# %%
import os
import sys

currentdir = os.path.dirname(os.path.realpath(__file__))
parentdir = os.path.dirname(currentdir)
rootrepodir = os.path.dirname(os.path.dirname(currentdir))
sys.path.append(parentdir)
sys.path.append(currentdir)
sys.path.append(rootrepodir)

import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10

from pf_utils import *
from evaluation_utils import *
from architectures import make_tsMNet
# from preprocessing import Pipeline_Preprocessor
from custom_NN_utils import *
import numpy as np
from matplotlib import pyplot as plt
from tqdm import tqdm

import sklearn.metrics
from sklearn.metrics import confusion_matrix
import seaborn as sns
import pandas as pd
import pickle

# import tensorflow as tf
# from tensorflow.keras import layers

SR = 16000
SAMPLE_LEN_SEC = 20
BANDS_TYPE = "bark"

dataset_root_folder = "/home/disks/ssdata/danielefoscarin/test_sets/devices_test_sets"

def devices_inference(datasets_path, model_path, save_inference=True) -> dict:
    """Compute inference in the devices test set

    Parameters
    ----------
    datasets_path : str
        Path to the devices test set root.
    model_path : str
        Path to the pretrained keras model. If "baseline" it loads the baseline
        predictions saved in every scenario folder.
    save_inference : bool, optional
        Save the inference results with the same name of the used model.

    Returns
    -------
    dict
        Inference tree dictionary
    """
    model_ID = os.path.basename(model_path)
    if model_path == "baseline":
        inference_tree = {}
        c = 0
        devices_list = os.listdir(datasets_path)
        for device in devices_list:
            device_path = os.path.join(datasets_path, device)
            speaker_levels = os.listdir(device_path)
            inference_tree[device] = {}

            for speaker_level in speaker_levels:
                speaker_level_path = os.path.join(device_path, speaker_level)
                noise_levels = os.listdir(os.path.join(speaker_level_path))
                inference_tree[device][speaker_level] = {}

                for noise_level in noise_levels:
                    noise_level_path = os.path.join(speaker_level_path, noise_level)
                    # print(c, "/30 ", noise_level_path)
                    # test_dataset = prepare_dataset(
                    #     noise_level_path, "test", bands_type=BANDS_TYPE
                    # )
                    inf_dict = {}
                    # WARNING for the baseline, the inf_dict contains only hte "ts" key
                    inf_dict["ts"] = load_baseline(
                        os.path.join(
                            dataset_root_folder, device, speaker_level, noise_level
                        ),
                        mode="test",
                    )
                    inf_dict["target"] = load_target(
                        os.path.join(
                            dataset_root_folder, device, speaker_level, noise_level
                        ),
                        mode="test",
                    )
                    inference_tree[device][speaker_level][noise_level] = inf_dict
                    c += 1

    else:
        print("load model weigths...")
        model_trained = make_tsMNet(
            input_bands=48,
            wm=0.65,
            expansion=2,
            kernel_depth=7,
        )
        model_trained.load_weights(os.path.join(model_path, "best_model"))

        # TODO save everythong in a dataframe and not a dictionary
        inference_tree = {}
        c = 0
        devices_list = os.listdir(datasets_path)
        # device_bar = tqdm(devices_list, desc="device")
        for device in devices_list:
            # device_bar.update(1)
            device_path = os.path.join(datasets_path, device)
            speaker_levels = os.listdir(device_path)
            inference_tree[device] = {}
            # print(device_path)

            # speaker_level_bar = tqdm(speaker_levels, desc="speaker_level")
            for speaker_level in speaker_levels:
                # speaker_level_bar.update(1)
                speaker_level_path = os.path.join(device_path, speaker_level)
                noise_levels = os.listdir(os.path.join(speaker_level_path))
                inference_tree[device][speaker_level] = {}
                # print(speaker_level_path)

                # noise_level_bar = tqdm(noise_levels, desc="noise_levels")
                for noise_level in noise_levels:
                    # noise_level_bar.update(1)
                    noise_level_path = os.path.join(speaker_level_path, noise_level)
                    # print(c, "/30 ", noise_level_path)
                    test_dataset = prepare_dataset(
                        noise_level_path, "test", bands_type=BANDS_TYPE
                    )
                    inf_dict = dataset_inference(test_dataset, model_trained)
                    inference_tree[device][speaker_level][noise_level] = inf_dict
                    c += 1
                # noise_level_bar.reset()
            # speaker_level_bar.reset()

    if save_inference:
        # save inference tree
        inference_path = (
            "/home/disks/ssdata/danielefoscarin/small_data/device_test_inference"
        )
        inference_tree_filename = os.path.join(
            inference_path, "inf_tree_" + model_ID + ".pickle"
        )
        with open(inference_tree_filename, "wb") as handle:
            pickle.dump(inference_tree, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return inference_tree

def devices_inference_DF(datasets_path, model_path, save_inference=True):
    model_ID = os.path.basename(model_path)
    df_row_list = []
    if model_path == "baseline":
        c = 0
        devices_list = os.listdir(datasets_path)
        for device in devices_list:
            device_path = os.path.join(datasets_path, device)
            speaker_levels = os.listdir(device_path)


            for speaker_level in speaker_levels:
                speaker_level_path = os.path.join(device_path, speaker_level)
                noise_levels = os.listdir(os.path.join(speaker_level_path))


                for noise_level in noise_levels:
                    noise_level_path = os.path.join(speaker_level_path, noise_level)
                    # print(c, "/30 ", noise_level_path)
                    path = (
                        device
                        + "/"
                        + speaker_level
                        + "/"
                        + noise_level
                        + "/"
                        + str(scenario)
                    )
                    inf_dict = {
                        # "classifier"
                        # "path": 
                        "ne": None, 
                        "fe":None, 
                        "vad_TS":None,
                    }
                    # WARNING for the baseline, the inf_dict contains only hte "ts" key
                    inf_dict["ts"] = load_baseline(
                        os.path.join(
                            dataset_root_folder, device, speaker_level, noise_level
                        ),
                        mode="test",
                    )
                    inf_dict["target"] = load_target(
                        os.path.join(
                            dataset_root_folder, device, speaker_level, noise_level
                        ),
                        mode="test",
                    )
                    df_row_list.append(inf_dict)
                    c += 1
    else:
        print("load model weigths...")
        model_trained = make_tsMNet(
            input_bands=48,
            wm=0.65,
            expansion=2,
            kernel_depth=7,
        )
        model_trained.load_weights(os.path.join(model_path, "best_model"))

        # TODO save everythong in a dataframe and not a dictionary
        inference_tree = {}
        c = 0
        devices_list = os.listdir(datasets_path)

        for device in devices_list:
            device_path = os.path.join(datasets_path, device)
            speaker_levels = os.listdir(device_path)

            for speaker_level in speaker_levels:
                # speaker_level_bar.update(1)
                speaker_level_path = os.path.join(device_path, speaker_level)
                noise_levels = os.listdir(os.path.join(speaker_level_path))

                for noise_level in noise_levels:
                    noise_level_path = os.path.join(speaker_level_path, noise_level)
                    # print(c, "/30 ", noise_level_path)
                    test_dataset = prepare_dataset(
                        noise_level_path, "test", bands_type=BANDS_TYPE
                    )
                    inf_dict = dataset_inference(test_dataset, model_trained)
                    df_row_list.append(inf_dict)
                    c += 1

    inf_df = pd.DataFrame(df_row_list)
    if save_inference:
        # save inference tree
        inference_path = (
            "/home/disks/ssdata/danielefoscarin/small_data/device_test_inference"
        )
        inference_data_filename = os.path.join(
            inference_path, "inf_data_" + model_ID + ".pickle"
        )
        with open(inference_data_filename, "wb") as handle:
            pickle.dump(inf_df, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return inf_df


def metrics_tspf(target, ts, safe_metrics=True, post_process=None):
    # TODO update doc
    """Return talk-state postfilter metrics

    Parameters
    ----------
    target : array
        Target talk state
    ts : array
        Predicted talk state

    Returns
    -------
    dict(str, float)
        dictionary metrics with keys:
            'e_leak', 'ne_loss', 'fd_loss', 'ne_noise',
            'dt_leak', 'vad_accuracy', 'vad_balanced_accuracy'
            'confusion_matrix'.
    """

    if post_process is not None:
        ts = post_process(ts)
    # TODO find a better way to address cases with no complete classes
    if safe_metrics:
        target = np.concatenate((target, np.array([0, 1, 2, 3])))
        ts = np.concatenate((ts, np.array([0, 1, 2, 3])))

    cm = np.transpose(confusion_matrix(target, ts, labels=[0, 1, 2, 3]))
    st = np.sum(cm[:, 2])
    no = np.sum(cm[:, 1])
    dt = np.sum(cm[:, 3])
    nt = np.sum(cm[:, 0])
    e_leak = (cm[1, 2] + cm[3, 2]) / st
    e_leak_dt = (cm[3, 2]) / st
    e_leak_st = (cm[1, 2]) / st
    ne_loss = (cm[0, 1] + cm[2, 1]) / no
    fd_loss = (cm[0, 3] + cm[2, 3]) / dt
    ne_noise = cm[1, 0] / nt
    dt_leak = cm[1, 3] / dt

    cm_norm = np.transpose(
        confusion_matrix(target, ts, labels=[0, 1, 2, 3], normalize="true")
    )

    target_ne, target_fe = ts_to_vad(target)
    ne, fe = ts_to_vad(ts)

    ne_accuracy = sklearn.metrics.accuracy_score(
        target_ne,
        ne,
    )
    ne_balanced_accuracy = sklearn.metrics.balanced_accuracy_score(target_ne, ne)
    fe_accuracy = sklearn.metrics.accuracy_score(target_fe, fe)
    fe_balanced_accuracy = sklearn.metrics.balanced_accuracy_score(target_fe, fe)
    ts_accuracy = sklearn.metrics.accuracy_score(target, ts)
    ts_balanced_accuracy = sklearn.metrics.balanced_accuracy_score(target, ts)

    out = {
        "e_leak": e_leak,
        "e_leak_dt": e_leak_dt,
        "e_leak_st": e_leak_st,
        "ne_loss": ne_loss,
        "fd_loss": fd_loss,
        "ne_noise": ne_noise,
        "dt_leak": dt_leak,
        "ne_accuracy": ne_accuracy,
        "ne_balanced_accuracy": ne_balanced_accuracy,
        "fe_accuracy": fe_accuracy,
        "fe_balanced_accuracy": fe_balanced_accuracy,
        "ts_accuracy": ts_accuracy,
        "ts_balanced_accuracy": ts_balanced_accuracy,
        "confusion_matrix": cm_norm,
    }
    return out


class DevicesMetricsInspector:
    def __init__(self, *args, **kwargs):
        # TODO make a dataframe from the beginning instead of inference_tree
        self.args = args
        self.models_dict = kwargs

        # load all the inference trees
        inf_tree_dict = {}
        for model_name, inf_tree_path in self.models_dict.items():
            # deleting the prefix inf_tree_
            model_ID = (
                os.path.basename(inf_tree_path)
                .replace("inf_tree_", "")
                .replace(".pickle", "")
            )
            with open(inf_tree_path, "rb") as handle:
                inf_tree = pickle.load(handle)
            inf_tree_dict[model_name] = inf_tree

        # make a dataframe with the metrics from every model
        # row_list is a list of dictionaries that populates the dataframe
        row_list = []
        for model_name, curr_inf_tree in inf_tree_dict.items():
            print("... looking at ", model_name)
            # explore the inference tree
            for device, device_dict in curr_inf_tree.items():
                for speaker_level, speaker_level_dict in device_dict.items():
                    for noise_level, curr_dataset in speaker_level_dict.items():
                        # curr_dataset = inference_tree[device][speaker_level][noise_level]
                        for scenario in range(len(curr_dataset["ts"])):
                            path = (
                                device
                                + "/"
                                + speaker_level
                                + "/"
                                + noise_level
                                + "/test_scenario_" + str(scenario)
                            )
                            # these are the first column of the dataframe
                            ID_dict = {
                                "classifier": model_name,
                                "path": path,
                                "device": device,
                                "speaker_level": speaker_level,
                                "noise_level": noise_level,
                                "scenario": scenario,
                            }
                            # inferred metrics
                            metrics_dict = metrics_tspf(
                                curr_dataset["target"][scenario],
                                curr_dataset["ts"][scenario],
                            )
                            # merge dictionaries
                            row_dict_full = {**ID_dict, **metrics_dict}
                            row_list.append(row_dict_full)

        # make a dataframe from the list of dicts
        self.metrics_df = pd.DataFrame(row_list)

    def get_subset(
        self, classifier=None, device=None, speaker_level=None, noise_level=None
    ):
        data = self.df
        out = data.loc[data["classifier"] == classifier]
        if device is not None:
            out = out.loc[data["device"] == device]
        if speaker_level is not None:
            out = out.loc[out["speaker_level"] == speaker_level]
        if noise_level is not None:
            out = out.loc[out["noise_level"] == noise_level]
        assert out.shape[0] > 0, "Empty subset, check your condition"
        return out

    def mean_subset_metrics(self, subset):
        # need this because the confusion matrics cannot be summed with builtin methods
        out = subset.iloc[5:-1].mean()
        # out["confusion_matrix"]
        cm_out = np.zeros((4, 4))
        for i, row in subset.iterrows():
            cm_out += row["confusion_matrix"]
        cm_out /= subset.shape[0]
        out["confusion_matrix"] = cm_out
        return out

    def compare_classifiers_fig(self, x, y, title=""):
        fig, ax = plt.subplots(1, 1, figsize=(8, 5 * len(self.models_dict)))
        sns.violinplot(
            x=x,
            y=y,
            data=self.metrics_df,
            hue="classifier",
            palette="Set3",
            cut=0,
            ax=ax,
        )
        ax.grid()
        ax.set_title(title)
        ax.set_xlim([0, 1])
        return fig

    # TODO a function that produce comparable audio for the models
# %%
if __name__ == "__main__":

    COMPUTE_INFERENCE=True
    if COMPUTE_INFERENCE:
        # compute the inference in the devices test set
        # baseline_inf_tree = devices_inference(
        #     dataset_root_folder,
        #     "baseline",
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/lin_20211212-210326",
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20211214-171737",
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20211215-140926"
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/bark_20211216-100238",
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20211216-100238"
        # )
        # _ = devices_inference(
        #     dataset_root_folder,
        #     "/home/disks/sdb/danielefoscarin/model_checkpoint/candidates/bark_20211216-173249"
        # )
        _ = devices_inference(
            dataset_root_folder,
            "/home/disks/sdb/danielefoscarin/model_checkpoint/tsd/bark_20220104-094648"
        )

    print("... inference done")
# %%
    # load the inspector object
    inspector = DevicesMetricsInspector(
        baseline= "/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_baseline.pickle",
        model_lin="/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_lin_20211212-210326.pickle",
        # model_bark="/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_bark_20211214-171737.pickle",
        # model_bark2="/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_bark_20211215-140926.pickle",
        model_ts_focused="/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_bark_20211216-173249.pickle",
        model_0103="/home/disks/ssdata/danielefoscarin/small_data/device_test_inference/inf_tree_bark_20220104-094648.pickle",
    )
# %%

    fig = inspector.compare_classifiers_fig(x="ts_balanced_accuracy", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="ne_balanced_accuracy", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="e_leak", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="ne_loss", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="fd_loss", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="ne_noise", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="e_leak_st", y="device")
    fig.show()
    fig = inspector.compare_classifiers_fig(x="e_leak_dt", y="device")
    fig.show()



# %%
# %%
