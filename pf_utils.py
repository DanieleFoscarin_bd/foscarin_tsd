import sys
import os
import platform
import subprocess
import bd_rd as bd

bd.print_version(__file__)
# Using bd_rd from branch foscarin_echo and commit c0056d3.
# Python: /home/danielefoscarin/LOCAL/SSD/TSD_thesis/bd_tf2/bin/python; version: 3.8.10
sys.path.append("../")
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt


czeros = lambda shape: np.zeros(shape, dtype=np.complex)

# %%

def tb_map_wrap(tb_map, sIn, rIn, sOut, conf, verbose=False):
    process = subprocess.Popen(
        [
            tb_map,
            sIn,
            rIn,
            sOut,
            conf,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    stdout, stderr = process.communicate()
    out = stdout.decode("utf-8")
    if verbose:
        print(" ".join([
            tb_map,
            sIn,
            rIn,
            sOut,
            conf,
        ]))
        print(out)

def tb_map_inj_wrap(tb_map, sIn, rIn, sOut, conf, ts, verbose=False, fix_header=False):
    if fix_header:
        filename = [sIn, rIn]
        for i, file in enumerate(filename):
            signal, sr = bd.io.read_wav(file)
            bd.io.write_wav(file, signal, sr)
    process = subprocess.Popen(
        [
            tb_map,
            sIn,
            rIn,
            sOut,
            conf,
            ts,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    stdout, stderr = process.communicate()
    out = stdout.decode("utf-8")
    if verbose:
        print(" ".join([
            tb_map,
            sIn,
            rIn,
            sOut,
            conf,
            ts,
        ]))
        print(out)


def tb_map_subprocess(
    bin_folder,
    data_folder,
    tb_map,
    sIn,
    rIn,
    sOut,
    conf,
    verbose=True,
    fix_header=False,
    path_mode="short",
):
    """Launch a subprocess that execute tb_map

    Parameters
    ----------
    bin_folder : str
        Path to the folder containined the bin file.
    data_folder : str
        Path to the folder containing scenario.
    tb_map : str
        Filename of tb_map
    sIn : str
        Filename of the sIn microphone signal.
    rIn : str
        Filename of the rIn reference signal.
    sOut : str
        Filename of the sOut signal to be produced.
    conf : str
        Filename of the .conf file
    verbose : bool, optional
        Display the output of the subprocess, by default True
    fix_header : bool, optional
        Fix the head of files exported by Reaper DAW, needed only one, by default False
    path_mode : "short" or "full", optional
        Use "full" for specifing the entire path for the involved files. 
        With "full" bin_folder and data_folder are ignored , by default "short".

    Returns
    -------
    None
    """    

    def wsl_path(path):
        path = path.replace("\\", "/")
        path = path.replace("C:", "/mnt/c")
        return path

    if fix_header:
        filename = [
            os.path.join(data_folder, rIn),
            os.path.join(data_folder, sIn),
        ]
        for i, file in enumerate(filename):
            signal, sr = bd.io.read_wav(file)
            bd.io.write_wav(file, signal, sr)

    if path_mode == "full":
        process = subprocess.Popen(
            [tb_map, sIn, rIn, sOut, conf],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

    elif path_mode == "short":
        if platform.system() == "Linux":
            process = subprocess.Popen(
                [
                    bin_folder + "/" + tb_map,
                    data_folder + "/" + sIn,
                    data_folder + "/" + rIn,
                    data_folder + "/" + sOut,
                    bin_folder + "/" + conf,
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
        else:
            process = subprocess.Popen(
                ["wsl.exe"]
                + [
                    wsl_path(bin_folder) + "/" + tb_map,
                    wsl_path(data_folder) + "/" + sIn,
                    wsl_path(data_folder) + "/" + rIn,
                    wsl_path(data_folder) + "/" + sOut,
                    wsl_path(bin_folder) + "/" + conf,
                ],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )

    stdout, stderr = process.communicate()
    out = stdout.decode("utf-8")
    if verbose:
        print(out)


def clean_probe_dir(folder, verbose=False):
    """Delete every .dat file in a folder

    Parameters
    ----------
    folder : str
        Path to the folder.
    verbose : bool, optional
        Print information about the number of removed files, by default False
    """
    crowded_folder = os.listdir(folder)
    deleted = 0
    if verbose:
        print("Cleaning probes...\n")
    for item in crowded_folder:
        if item.endswith(".dat"):
            os.remove(os.path.join(folder, item))
            deleted += 1
    if verbose:
        print(deleted, " files removed")

# OBSOLETE
def resample_dir(data_folder, target_sr, verbose=False):
    """
    use sox through subprocess to convert all the wav files in a folder to
    the target sample rate
    """
    files = os.listdir(data_folder)
    wav_files = [name for name in files if name.endswith(".wav")]
    temp_prefix = "soxtemp_"
    for file in wav_files:
        print(file)
        process = subprocess.Popen(
            [
                "sox",
                os.path.join(data_folder, file),
                os.path.join(data_folder, temp_prefix + file),
                "rate",
                str(target_sr),
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        stdout, stderr = process.communicate()
        out = stdout.decode("utf-8")
        if verbose:
            print(out)

        os.remove(os.path.join(data_folder, file))

    files = os.listdir(data_folder)
    for file in files:
        if file.startswith(temp_prefix):
            print(file)
            os.rename(
                os.path.join(data_folder, file),
                os.path.join(data_folder, file[len(temp_prefix) :]),
            )

def resample_on_filename(filename, temp_folder, target_sr, mono=True, verbose=False):
    """Resample an audio file and return its path.
    Sox required.

    Parameters
    ----------
    filename : str
        Path to the audio file
    temp_folder : str
        Path to the temp folder where the resampled file is saved
    target_sr : int
        Target sample rate
    mono : bool, optional
        Force single channel (mono) format, by default True
    verbose : bool, optional
        Print the Sox text output, by default False

    Returns
    -------
    (str, str)
        Path to the resampled file, prefix of the resampled file
    """
    file_base, name = os.path.split(conv_path(filename))
    safe_prefix = "_sox_temp_"
    name = safe_prefix + name
    if mono:
        
        process = subprocess.check_call(
            [
                "sox",
                filename,
                "-c",
                "1",
                "-r",
                str(target_sr),
                os.path.join(temp_folder, name),
            ],
        )
    else:
        process = subprocess.check_call(
            ["sox", filename, "-r", str(target_sr), os.path.join(temp_folder, name)],
        )
    # stdout, _ = process.communicate()
    # out = stdout.decode("utf-8")
    # if verbose:
    #     print(out)
    return os.path.join(temp_folder, name), safe_prefix


def format_spectogram(x):
    x[:, 0] = np.real(x[:, 0]) + np.real(x[:, -1]) * 1j
    x = x[:, :-1]
    return x


def conv_path(path):
    path = path.replace("\\", "/")
    return path


def pf_process(pf, sin_stft, noise_psd, speech_psd, res_echo_psd, talk_state, spp=None):
    """Execute bd postfilter

    Parameters
    ----------
    pf : bd Post_Filter
        Post filter object.
    sin_stft : np.ndarray, (frames, bins)
        Spectrogram of the input signal
    noise_psd : np.ndarray, (frames, bins)
        Noise PSD estimation.
    speech_psd : np.ndarray, (frames, bins)
        Speech PSD estimation.
    res_echo_psd : np.ndarray, (frames, bins)
        Residual echo PSD estimation
    talk_state : np.ndarray, (frames,)
        Talk state estimation
    spp : ndarray, optional
        Speech presence probability, by default None.

    Returns
    -------
    [type]
        [description]
    """
    if spp == None:
        spp = np.ones((speech_psd.shape[0], speech_psd.shape[1]))

    H = np.zeros(sin_stft.shape)

    Inv_noise = np.zeros(sin_stft.shape)
    g_min = np.zeros(sin_stft.shape)
    x_hat_stft = czeros((sin_stft.shape[0], sin_stft.shape[1] + 1))
    for n in range(sin_stft.shape[0]):
        pf.update_psd(
            noise_psd[n, :],
            speech_psd=speech_psd[n, :],
            res_echo_psd=res_echo_psd[n, :],
            talk_state=talk_state[n],
            spp=spp[n, :],
        )
        x_hat_stft[n, ...] = pf.process_frame_np(
            sin_stft[n, :], talk_state=talk_state[n]
        )
        H[n, ...] = pf.g
        g_min[n, ...] = pf.g_min
        Inv_noise[n, ...] = pf.inv_noise
    return x_hat_stft, H, Inv_noise, g_min


def delay_finder(y1, y2, sr, figure=False):
    """
    Find the delay between two signals, using scipy correlation.

    Parameter:
        y1, y2: arrays.
        sr: int, sample rate.
        figure: boolean, generate a figure with the plot of the correlation coefficient.
            (default False)

    Return: float, value of the the delay in seconds
    """
    n = len(y1)
    corr = signal.correlate(y2, y1, mode="same") / np.sqrt(
        signal.correlate(y1, y1, mode="same")[int(n / 2)]
        * signal.correlate(y2, y2, mode="same")[int(n / 2)]
    )
    delay_arr = np.linspace(-0.5 * n / sr, 0.5 * n / sr, n)
    delay = delay_arr[np.argmax(corr)]
    if figure:
        plt.figure()
        plt.plot(delay_arr, corr)
        plt.title("Lag: " + str(np.round(delay, 3)) + " s")
        plt.xlabel("Lag")
        plt.ylabel("Correlation coeff")
        plt.show()
    return delay


def to_onehot(x, n_class=4):
    onehot = np.zeros((n_class, 1))
    onehot[int(x)] = 1
    return onehot


# %%
if __name__ == "__main__":
    print("Utility functions for tb_map and post filter")
